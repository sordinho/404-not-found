# Second Hand Market

Second Hand Market android application developed during Mobile Application Development course at PoliTo

## Development Steps

### Lab 01

- [Text](docs/lab1.pdf)
- Date: 27/03/2020

### Lab 02

- [Text](docs/lab2.pdf)
- Date: 17/04/2020

## Lab 03

- [Text](docs/lab3.pdf)
- Date: 08/05/2020
- (See Branch [LAB-3](https://bitbucket.org/sordinho/404-not-found/src/LAB-3/))

## Lab 04

- [Text](docs/lab4.pdf)
- Date: 22/05/2020
- (See Branch [LAB-4](https://bitbucket.org/sordinho/404-not-found/src/LAB-3/))
- Folder [Lab3](https://bitbucket.org/sordinho/404-not-found/src/LAB-4/Lab3/)

# Public Transport Application

## Lab 05

- [Text](docs/lab5.pdf)
- Date: 05/06/2020

## The Team

- Vittorio Di Leo
- Francesco Riba
- Andrea Settimo
- Davide Sordi
