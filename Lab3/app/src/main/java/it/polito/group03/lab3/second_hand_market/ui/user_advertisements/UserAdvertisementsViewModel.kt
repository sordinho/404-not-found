package it.polito.group03.lab3.second_hand_market.ui.user_advertisements

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import it.polito.group03.lab3.second_hand_market.model.Advertisement
import it.polito.group03.lab3.second_hand_market.model.Repository.AdvertisementRepository


class UserAdvertisementsViewModel : ViewModel() {
    var owner :String = ""
    set(value) {
        field = value
        fetchAdvertisements()
    }
    lateinit var advs: LiveData<MutableMap<String, Advertisement>>
    val repository =
        AdvertisementRepository()

    /**
     * This method retives LiveData<MutableMap<String, Advertisement>> of specific owner and applies an observation to detect new changes.
     */
    fun fetchAdvertisements(): LiveData<MutableMap<String, Advertisement>> {
        var advs = MutableLiveData<MutableMap<String, Advertisement>>()
        repository.getAdvertisements(owner).observeForever { advsList ->
            advs.value = advsList
        }
        this.advs = advs
        return advs
    }
}