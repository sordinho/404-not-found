package it.polito.group03.lab3.second_hand_market.ui.on_sale_list

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.FrameLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.activityViewModels
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import it.polito.group03.lab3.second_hand_market.R
import kotlinx.android.synthetic.main.filter_layout.*
import kotlinx.android.synthetic.main.filter_layout.view.*
import java.util.*


class FilterBottomSheet : BottomSheetDialogFragment(){
    lateinit var buttons: View
    private val onSaleListViewModel: OnSaleListViewModel by activityViewModels()
    private var isAsc = false
    private var isDesc = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.filter_layout, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        buttons = bottomSheetDialog.layoutInflater.inflate(R.layout.buttons, null)
        bottomSheetDialog.setOnShowListener {
            val coordinator = (it as BottomSheetDialog)
                .findViewById<CoordinatorLayout>(com.google.android.material.R.id.coordinator)
            val containerLayout =
                it.findViewById<FrameLayout>(com.google.android.material.R.id.container)

            buttons.layoutParams = FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT
            ).apply {
                gravity = Gravity.BOTTOM
            }
            containerLayout!!.addView(buttons)

            /*
            * Dynamically update bottom sheet containerLayout bottom margin to buttons view height
            * */
            buttons.post {
                (coordinator!!.layoutParams as ViewGroup.MarginLayoutParams).apply {
                    buttons.measure(
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                    )
                    this.bottomMargin = buttons.measuredHeight
                    containerLayout.requestLayout()
                }
            }
        }
        val saveButton = buttons.findViewById<Button>(R.id.save_button)
        val resetButton = buttons.findViewById<Button>(R.id.reset)
        val cancelButton = buttons.findViewById<Button>(R.id.cancel_button)

        saveButton.setOnClickListener {
            onSaleListViewModel.filter = checkFilters()
            dismiss()
        }
        resetButton.setOnClickListener {
            onSaleListViewModel.filter = Filter("", "", "", 0.0, Double.MAX_VALUE, "", false, false)
            reset()
        }
        cancelButton.setOnClickListener {
            dismiss()
        }
        return bottomSheetDialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Setup dropdown menus for categories
        if (savedInstanceState != null) {
            savedInstanceState.getParcelable<Filter>("filter")?.let { setup(it) }
        }else{
            setup(onSaleListViewModel.filter)
        }
        setupDropdownMenusCategories()

        asc.setOnClickListener {
            if (isAsc) {
                it.asc.setChipIconResource(R.drawable.ic_sort_black_24dp)
                isAsc = false
            } else {
                it.asc.setChipIconResource(R.drawable.ic_check_circle_black_24dp)
                des.setChipIconResource(R.drawable.ic_sort_black_24dp)
                isAsc = true
                isDesc = false
            }
        }

        des.setOnClickListener {
            if (isDesc) {
                it.des.setChipIconResource(R.drawable.ic_sort_black_24dp)
                isDesc = false
            } else {
                it.des.setChipIconResource(R.drawable.ic_check_circle_black_24dp)
                asc.setChipIconResource(R.drawable.ic_sort_black_24dp)
                isAsc = false
                isDesc = true
            }
        }

        // Setup Date Picker
        expire_date.editText!!.setOnClickListener {
            setupDatePicker()
        }
    }

    /**
     * This method permittes to set up the view with a specific state.
     */
    private fun setup(filter: Filter) {
        main_category.editText!!.setText(filter.mainCategory)
        sub_category.editText!!.setText(filter.subCategory)
        location.editText!!.setText(filter.location)
        val fromPrice = filter.fromPrice
        if (fromPrice != 0.0)
            from_price.editText!!.setText(fromPrice.toString())
        val toPrice = filter.toPrice
        if (toPrice != Double.MAX_VALUE)
            to_price.editText!!.setText(toPrice.toString())
        expire_date.editText!!.setText(filter.expiryDate)
        if (filter.isAsc) {
            isAsc = true;
            asc.setChipIconResource(R.drawable.ic_check_circle_black_24dp)
        }
        if (filter.isDesc) {
            isDesc = false
            des.setChipIconResource(R.drawable.ic_check_circle_black_24dp)
        }
        setSubcategoryDropdown(filter.mainCategory)
    }

    /**
     * This method allows to reset the state of filter.
     */
    private fun reset() {
        main_category.editText!!.setText("")
        sub_category.editText!!.setText("")
        location.editText!!.setText("")
        from_price.editText!!.setText("")
        to_price.editText!!.setText("")
        expire_date.editText!!.setText("")
        des.setChipIconResource(R.drawable.ic_sort_black_24dp)
        asc.setChipIconResource(R.drawable.ic_sort_black_24dp)
    }

    /**
     * This function setup the dropdown menus for categories and sub cateogries
     * also setup onclick listener for categories
     */
    private fun setupDropdownMenusCategories() {
        var categories = resources.getStringArray(R.array.categories)
        var list: List<String?>
        list = categories.toList()
        val arrayList = ArrayList(list)
        arrayList.add(0, "")
        categories = arrayList.toArray(arrayOfNulls<String>(list.size))
        val adapter = context?.let { ArrayAdapter(it, R.layout.list_item, categories) }
        (main_category.editText as AutoCompleteTextView).setAdapter(adapter)
        (main_category.editText as AutoCompleteTextView).addTextChangedListener(
            object :
                TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    sub_category.isEnabled = true
                    (sub_category.editText as AutoCompleteTextView).setText("")
                    setSubcategoryDropdown(s.toString())
                }
            })
    }

    /**
     * Setup the dropdown menu for sub categories given a category
     */
    private fun setSubcategoryDropdown(selectedCategory: String?) {
        var subCatToUpdate: Array<String> = emptyArray()

        when (selectedCategory) {
            resources.getString(R.string.cat1) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat1)
            resources.getString(R.string.cat2) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat2)
            resources.getString(R.string.cat3) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat3)
            resources.getString(R.string.cat4) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat4)
            resources.getString(R.string.cat5) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat5)
            resources.getString(R.string.cat6) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat6)
            resources.getString(R.string.cat7) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat7)
            resources.getString(R.string.cat8) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat8)
            else -> sub_category.isEnabled = false
        }
        var list: List<String?>
        list = subCatToUpdate.toList()
        val arrayList = ArrayList(list)
        arrayList.add(0, "")
        subCatToUpdate = arrayList.toArray(arrayOfNulls<String>(list.size))
        val subCatAdapter =
            context?.let {
                ArrayAdapter(it, R.layout.list_item, subCatToUpdate)
            }
        (sub_category.editText as AutoCompleteTextView).setAdapter(
            subCatAdapter
        )
    }

    /**
     * Setup the date picker for expiry date
     */
    private fun setupDatePicker() {
        val calendar = Calendar.getInstance()
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)

        // date picker dialog
        val dialog = DatePickerDialog(
            requireContext(),
            DatePickerDialog.OnDateSetListener { view, dYear, dMonth, dayOfMonth ->
                expire_date.editText?.setText("" + dayOfMonth + "/" + (dMonth + 1) + "/" + dYear)
            }, year, month, day
        )
        dialog.datePicker.minDate = System.currentTimeMillis() - 1000;
        dialog.show()
    }

    /**
     * This method prepares the filter object.
     */
    private fun checkFilters(): Filter {
        val mainCategory = (main_category.editText as AutoCompleteTextView).text.toString()
        val subCategory = (sub_category.editText as AutoCompleteTextView).text.toString()
        val expireDate = expire_date.editText!!.text.toString()
        val location = location.editText!!.text.toString()
        var fromPrice = 0.0
        if (!from_price.editText?.text.isNullOrEmpty()) {
            fromPrice = from_price.editText!!.text.toString().toDouble()
        }
        var toPrice = Double.MAX_VALUE
        if (!to_price.editText?.text.isNullOrEmpty()) {
            toPrice = to_price.editText!!.text.toString().toDouble()
        }

        return Filter(mainCategory, subCategory, expireDate, fromPrice, toPrice, location,
            isAsc,
            isDesc
        )
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val filter = checkFilters()
        outState.putParcelable("filter", filter)
    }
}