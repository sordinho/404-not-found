package it.polito.group03.lab3.second_hand_market.ui.user_advertisements

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.ui.item_edit.AdvertisementViewModel
import it.polito.group03.lab3.second_hand_market.ui.user_profile.UserViewModel
import kotlinx.android.synthetic.main.fragment_user_advertisements.*
import kotlinx.android.synthetic.main.fragment_user_advertisements.itemList
import kotlinx.android.synthetic.main.fragment_user_advertisements.shimmer_view_container_interested

class UserAdvertisementsFragment : Fragment() {
    private lateinit var advertisementAdapter: AdvertisementAdapter
    val userAdvertisementsViewModel: UserAdvertisementsViewModel by activityViewModels()
    private val advertisementViewModel: AdvertisementViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        advertisementViewModel.restoreBooleans()
        val root = inflater.inflate(R.layout.fragment_user_advertisements, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        advertisementAdapter = AdvertisementAdapter(findNavController())
        itemList.adapter = advertisementAdapter
        itemList.layoutManager = LinearLayoutManager(context)
        observeData()

        fab.setOnClickListener {
            val bundle = Bundle()
            advertisementViewModel.currentItemMarker = null
            bundle.putString("advertisementID", "")
            findNavController().navigate(R.id.action_nav_slideshow_to_itemEditFragment, bundle)
        }
    }

    /**
     * This method allows you to observe the data of the view model.
     */
    private fun observeData() {
        shimmer_view_container_interested.startShimmer()
        userAdvertisementsViewModel.fetchAdvertisements().observe(viewLifecycleOwner, Observer {
            shimmer_view_container_interested.stopShimmer()
            shimmer_view_container_interested.visibility = View.GONE
            advertisementAdapter.advertisements = it

            messageListEmpty.isVisible = it.isEmpty()
        })

        advertisementViewModel.resultOperation.observe(viewLifecycleOwner, Observer {
            if (advertisementViewModel.doingAnOperation) {
                if (it) {
                    Snackbar.make(
                        constraintLayout,
                        "The operation is complited!",
                        Snackbar.LENGTH_LONG
                    )
                        .setAction("Action", null).show()
                } else {
                    Snackbar.make(
                        constraintLayout,
                        "Error during the operation, retry!",
                        Snackbar.LENGTH_LONG
                    )
                        .setAction("Action", null).show()
                }
                advertisementViewModel.doingAnOperation = false
            }
        })

        userAdvertisementsViewModel.repository.errorOrException.observe(viewLifecycleOwner, Observer {
            if (it) {
                Snackbar.make(
                    constraintLayout,
                    "Error during the loading of advertisements, retry!",
                    Snackbar.LENGTH_LONG
                )
                    .setAction("Action", null).show()
                userAdvertisementsViewModel.repository.errorOrException.value = false
            }
        })
    }
}
