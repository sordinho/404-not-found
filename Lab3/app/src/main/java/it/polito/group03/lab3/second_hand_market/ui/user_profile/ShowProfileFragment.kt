package it.polito.group03.lab3.second_hand_market.ui.user_profile

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayoutMediator
import com.squareup.picasso.Picasso
import it.polito.group03.lab3.second_hand_market.MainActivity
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.model.UserProfile
import it.polito.group03.lab3.second_hand_market.ui.user_profile.comments.AddCommentBottomSheet
import it.polito.group03.lab3.second_hand_market.ui.user_profile.comments.CommentsFragment
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_show_profile.*


class ShowProfileFragment : Fragment() {
    private lateinit var tabCollectionAdapter: TabCollectionAdapter

    private lateinit var myActivityMenuItem: MenuItem
    private lateinit var vm : UserViewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?{
        vm = activity?.let{ ViewModelProvider(it).get(UserViewModel::class.java) }!!

        if(arguments!=null){
            //(activity as MainActivity).supportActionBar?.title = "Other profile"
            //(activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
            Log.d("ARGUMENTS", "Arguments are not null")
            vm.isMyProfile = arguments?.getBoolean("isMyProfile") ?: false
            vm.soldTo = arguments?.getBoolean("soldTo") ?: false
            vm.advertisementID = arguments?.getString("advertisementID") ?: ""
            vm.commented = arguments?.getBoolean("commented") ?: false
            if(!vm.isMyProfile) {
                (activity as MainActivity).supportActionBar?.title = "Other profile"
                vm.coroutineLoadProfile(arguments?.getString("userID") ?: "")
            } else {
                (activity as MainActivity).supportActionBar?.title = "My profile"
            }
        } else {
            (activity as MainActivity).supportActionBar?.title = "My profile"
            Log.d("ARGUMENTS", "Arguments are null")
            vm.isMyProfile = true
        }
        if(vm.isMyProfile)
            setHasOptionsMenu(true)

        return inflater.inflate(R.layout.fragment_show_profile,container,false)
    }

    private fun updateUI(data : UserProfile){
        if(data.avatarPath.isNotEmpty())
            Picasso.get().load(data.avatarPath).into(userAvatar)
        else{
            userAvatar.setImageResource(R.drawable.ic_user)
        }
        nickname.text = data.nickname
        main_rating.rating = data.rating
    }

    private fun loadUser(){
        if(vm.isMyProfile) {
            vm.userLiveData.observe(this, Observer { data ->
                updateUI(data)
            })
        }else{
            vm.otherUserProfile.observe(this, Observer { data ->
                updateUI(data)
            })
        }
    }

    override fun onStart(){
        super.onStart()
        loadUser()
        vm.profilePicHasChanged=false
        vm.profileHasChanged=false
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(!vm.isMyProfile) {
            (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true);
            this.view?.setOnKeyListener { v, keyCode, _ ->
                if (keyCode == KeyEvent.KEYCODE_BACK)
                    vm.unsubscribeToOtherUser()
                true
            }
        }

        configureTab()
        configureFab()
        observeData()
    }

    private fun configureFab() {
        if (!vm.isMyProfile && vm.soldTo && !vm.commented){
            fab.setOnClickListener {
                val addCommentsFragment = AddCommentBottomSheet()
                activity?.supportFragmentManager?.let {
                        it1 -> addCommentsFragment.show(it1, "Add Comment")
                }
            }
        }else{
            fab.visibility = View.GONE
        }

        if (!vm.isLogged())
            fab.visibility = View.GONE
    }

    /**
     * This method configures the tab view.
     */
    private fun configureTab() {
        tabCollectionAdapter = TabCollectionAdapter(this)
        pager.adapter = tabCollectionAdapter
        TabLayoutMediator(htab_tabs, pager) { tab, position ->
            when (position) {
                0 -> tab.text = "Details"
                1 -> tab.text = "Comments"
                2 -> tab.text = "Location"
            }
        }.attach()

        pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            var isInPage2 = false
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (position == 2) {
                    pager.setUserInputEnabled(false)
                    appBarLayout.setExpanded(false)
                    isInPage2 = true
                    fab.visibility = View.GONE
                } else {
                    if (isInPage2) {
                        pager.setUserInputEnabled(true)
                        appBarLayout.setExpanded(true)
                        isInPage2 = false
                    }
                    if (position == 0) {
                        fab.visibility = View.GONE
                    } else {
                        if (vm.isMyProfile || !vm.soldTo || vm.commented){
                            fab.visibility = View.GONE
                        }else {
                            fab.visibility = View.VISIBLE
                        }
                    }
                }
            }
        })
    }

    /**
     * Observe repository data.
     */
    private fun observeData() {
        vm.resultAddComment.observe(viewLifecycleOwner, Observer {
            if (vm.newComment) {
                if (it) {
                    Snackbar.make(
                        this.requireView(),
                        "The comment is added!",
                        Snackbar.LENGTH_LONG
                    ).show()
                    fab.hide()
                } else {
                    Snackbar.make(
                        this.requireView(),
                        "The comment is not added!",
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                vm.newComment = false
            }
        })

        vm.db.addCommentErrorOrException.observe(viewLifecycleOwner, Observer {
            if (it) {
                Snackbar.make(
                    this.requireView(),
                    "The comment is not added!",
                    Snackbar.LENGTH_LONG
                )
                    .setAction("Action", null).show()
                vm.db.addCommentErrorOrException.value = false
            }
        })
    }

    /**
     * Inflate the menu when created
     */
    override fun onCreateOptionsMenu(menu: Menu,inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu,inflater)

        menu.clear()
        if(vm.isMyProfile)
            inflater.inflate(R.menu.edit_profile, menu)
    }

    /**
     * Check which menu item is pressed and start corresponding task
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.nav_user_edit_profile -> {
                if (vm.isMyProfile) {
                    myActivityMenuItem = item
                    item.isEnabled = false

                    vm.isRegistration = false
                    vm.currentUserMarker = null
                    if (NavHostFragment.findNavController(nav_host_fragment).currentDestination?.getAction(R.id.action_nav_user_profile_to_nav_user_edit_profile) != null) {
                        NavHostFragment.findNavController(nav_host_fragment).navigate(R.id.action_nav_user_profile_to_nav_user_edit_profile)
                    } else {
                        NavHostFragment.findNavController(nav_host_fragment).navigate(R.id.action_user_profile_to_nav_user_edit_profile)
                    }
                    true
                } else {
                    Snackbar.make(showProfileLayout, "This is not your profile!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                    false
                }
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}

class TabCollectionAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 3

    /**
     * Return a NEW fragment instance in createFragment(int)
     */
    override fun createFragment(position: Int): Fragment {
    var fragment: Fragment?
    when (position) {
        0 -> fragment =
            ProfileDetailFragment()
        1 -> fragment =
            CommentsFragment()
        2 -> fragment =
            UserMapFragment()
        else -> fragment =
            CommentsFragment()
    }
    return fragment
    }
}