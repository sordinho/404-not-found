package it.polito.group03.lab3.second_hand_market

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.squareup.picasso.Picasso
import it.polito.group03.lab3.second_hand_market.model.Repository.AdvertisementRepository
import it.polito.group03.lab3.second_hand_market.model.UserProfile
import it.polito.group03.lab3.second_hand_market.ui.item_detail.ItemDetailViewModel
import it.polito.group03.lab3.second_hand_market.ui.on_sale_list.OnSaleListViewModel
import it.polito.group03.lab3.second_hand_market.ui.user_advertisements.UserAdvertisementsViewModel
import it.polito.group03.lab3.second_hand_market.ui.user_profile.UserViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {

    //private lateinit var gso : GoogleSignInOptions
    //private lateinit var mGoogleSignInClient : GoogleSignInClient
    //private lateinit var account : GoogleSignInAccount
    private lateinit var appBarConfiguration: AppBarConfiguration

    private lateinit var user_vm: UserViewModel
    private lateinit var userAdvertisementsViewModel: UserAdvertisementsViewModel
    private lateinit var onSaleListViewModel: OnSaleListViewModel
    private lateinit var detail_vm: ItemDetailViewModel
    private lateinit var navEmail: TextView
    private lateinit var navNick: TextView
    private lateinit var navAvatar: ImageView
    private lateinit var navView: NavigationView
    var advRepo =
        AdvertisementRepository()


    private fun updateNavUserData(data: UserProfile) {
        if (data.avatarPath.isNotEmpty()) {
            Picasso.get().load(data.avatarPath).into(navAvatar)
            navAvatar.tag = data.avatarPath
        } else {
            Picasso
                .get()
                .load(R.drawable.ic_user)
                .placeholder(R.drawable.ic_user)
                .into(navAvatar)
            //navAvatar.setImageURI(null)
        }
        navEmail.text = data.email
        navNick.text = data.nickname
        detail_vm.userEmail = data.email
        userAdvertisementsViewModel.owner = user_vm.userProfileId.value.toString()
        onSaleListViewModel.owner = user_vm.userProfileId.value.toString()

    }

    private fun loadUser(): Boolean {
        user_vm.userLiveData.observe(this, Observer { data ->
            updateNavUserData(data)
        })

        user_vm.userProfileId.observe(this, Observer { userID ->
            advRepo.getWatchlistAdvs(userID).observe(this, Observer {
                it.forEach{itemId->
                FirebaseMessaging.getInstance().subscribeToTopic("/topics/"+itemId+"_status")
            }})

            advRepo.getAdvs(userID).observe(this, Observer {
                it.forEach { itemId ->
                    FirebaseMessaging.getInstance()
                        .subscribeToTopic("/topics/" + itemId + "_interest")
                }
            })
        })

        user_vm.loginOk.observe(this, Observer { data ->
            refreshNavView(data)
        })

        if (!user_vm.isLogged()) {
            return false
        }

        user_vm.coroutineLoadProfile()
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userAdvertisementsViewModel = ViewModelProvider(this).get(UserAdvertisementsViewModel::class.java)
        onSaleListViewModel = ViewModelProvider(this).get(OnSaleListViewModel::class.java)
        user_vm = ViewModelProvider(this).get(UserViewModel::class.java)
        detail_vm = ViewModelProvider(this).get(ItemDetailViewModel::class.java)

        user_vm.setupAssetConverter(this,2)
        detail_vm.setAssetConverter(user_vm.drawableConverter)

        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        navEmail = navView
            .getHeaderView(0)
            .findViewById(R.id.header_email)
        navNick = navView
            .getHeaderView(0)
            .findViewById(R.id.header_nickname)
        navAvatar = navView
            .getHeaderView(0)
            .findViewById(R.id.header_avatar)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_on_sale,
                R.id.nav_advertisements,
                R.id.nav_watchlist,
                R.id.nav_user_profile,
                R.id.nav_login,
                R.id.nav_boughtitems
            ),
            drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navView.menu.findItem(R.id.nav_logout).setOnMenuItemClickListener {
            //FirebaseDatabase.getInstance().getReference("topics").
            GlobalScope.launch { FirebaseInstanceId.getInstance().deleteInstanceId() }
            user_vm.logOut()
            onSaleListViewModel.owner = ""
            refreshNavView(false)

            navController.navigate(R.id.nav_on_sale)
            return@setOnMenuItemClickListener true
        }
        loadUser()
    }

    private fun refreshNavView(logged: Boolean) {
        if (logged) {
            navView.menu.findItem(R.id.nav_login).isVisible = false
            navView.menu.findItem(R.id.nav_user_profile).isVisible = true
            navView.menu.findItem(R.id.nav_logout).isVisible = true
            navView.menu.findItem(R.id.nav_advertisements).isVisible = true
            navView.menu.findItem(R.id.nav_watchlist).isVisible = true
            navView.menu.findItem(R.id.nav_boughtitems).isVisible = true
        } else {
            Snackbar.make(drawer_layout, "You are not logged in!", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
            navView.menu.findItem(R.id.nav_login).isVisible = true
            navView.menu.findItem(R.id.nav_user_profile).isVisible = false
            navView.menu.findItem(R.id.nav_logout).isVisible = false
            navView.menu.findItem(R.id.nav_advertisements).isVisible = false
            navView.menu.findItem(R.id.nav_watchlist).isVisible = false
            navView.menu.findItem(R.id.nav_boughtitems).isVisible = false
        }
    }


    fun setActionBarTitle(title: String?) {
        getSupportActionBar()?.setTitle(title)
    }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}
