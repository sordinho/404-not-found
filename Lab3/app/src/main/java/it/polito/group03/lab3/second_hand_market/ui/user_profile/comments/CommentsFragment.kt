package it.polito.group03.lab3.second_hand_market.ui.user_profile.comments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.ui.user_profile.UserViewModel
import kotlinx.android.synthetic.main.fragment_comments.*
import kotlinx.android.synthetic.main.fragment_comments.messageListEmpty

class CommentsFragment : Fragment() {
    private val userProfile: UserViewModel by activityViewModels()
    lateinit var commentsAdapter: CommentsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_comments, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_comments.visibility = View.GONE
        messageListEmpty.visibility = View.GONE

        commentsAdapter = CommentsAdapter(findNavController())
        recycler_comments.adapter = commentsAdapter
        recycler_comments.layoutManager = LinearLayoutManager(context)
        observeData()

        /*if (!userProfile.isMyProfile && userProfile.soldTo){
            fab.setOnClickListener {
                val addCommentsFragment = AddCommentBottomSheet()
                activity?.supportFragmentManager?.let {
                        it1 -> addCommentsFragment.show(it1, "Add Comment")
                }
            }
        }else{
            fab.visibility = View.GONE
        }

        if (!userProfile.isLogged())
            fab.visibility = View.GONE*/
    }

    private fun observeData() {
        if (shimmer_view_container != null) {
            shimmer_view_container.visibility = View.VISIBLE
            shimmer_view_container.startShimmer()
        }

        /* Observe the comments and show them into the screen */
        userProfile.getComments().observe(viewLifecycleOwner, Observer {
            Log.d("XXX", "Fragment" + it.size)
            commentsAdapter.comments = it
            if (shimmer_view_container != null) {
                shimmer_view_container.stopShimmer()
                shimmer_view_container.visibility = View.GONE
            }

            if (it.size == 0) {
                messageListEmpty.visibility = View.VISIBLE
            }else{
                messageListEmpty.visibility = View.GONE
                recycler_comments.visibility = View.VISIBLE
            }
        })

        /* Observe the user profiles and set them in each item of list */
        userProfile.commentUsers.observe(viewLifecycleOwner, Observer {
            commentsAdapter.user = it
        })

        userProfile.db.errorOrException.observe(viewLifecycleOwner, Observer {
            if (it) {
                Snackbar.make(
                    this.requireView(),
                    "Error during the loading of comments, retry!",
                    Snackbar.LENGTH_LONG
                )
                    .setAction("Action", null).show()
                userProfile.db.errorOrException.value = false
            }
        })
    }
}
