package it.polito.group03.lab3.second_hand_market.ui.user_profile

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import it.polito.group03.lab3.second_hand_market.MainActivity
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.model.UserProfile
import kotlinx.android.synthetic.main.fragment_profile_detail.*

/**
 * A simple [Fragment] subclass.
 * Use the [ProfileDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfileDetailFragment : Fragment() {
    private lateinit var vm: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = activity?.let { ViewModelProvider(it).get(UserViewModel::class.java) }!!
        return inflater.inflate(R.layout.fragment_profile_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!vm.isMyProfile) {
            (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true);
            this.view?.setOnKeyListener { v, keyCode, _ ->
                if (keyCode == KeyEvent.KEYCODE_BACK)
                    vm.unsubscribeToOtherUser()
                true
            }
            fullname_label.isVisible = false
            fullname.isVisible = false
        }
    }

    private fun updateUI(data: UserProfile) {
        email.text = data.email
        nickname.text = data.nickname
        location.text = data.location
        if (vm.isMyProfile) {
            fullname.text = data.fullname
        }
    }

    private fun loadUser() {
        if(vm.isMyProfile)
            vm.userLiveData.observe(this, Observer { data ->
                updateUI(data)
            })
        else {
            vm.otherUserProfile.observe(this, Observer { data ->
                updateUI(data)
            })
        }
    }

    override fun onStart() {
        super.onStart()
        loadUser()
        vm.profilePicHasChanged = false
        vm.profileHasChanged = false
    }
}
