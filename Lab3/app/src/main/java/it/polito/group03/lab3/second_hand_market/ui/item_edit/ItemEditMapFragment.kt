package it.polito.group03.lab3.second_hand_market.ui.item_edit

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Resources
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import com.google.android.material.snackbar.Snackbar
import it.polito.group03.lab3.second_hand_market.R
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_item_edit_map.*
import java.io.IOException
import java.util.*

class ItemEditMapFragment : Fragment() {
    private lateinit var mMap: GoogleMap
    private lateinit var vm : AdvertisementViewModel
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private var currentMarker: Marker? = null
    private var currentLat : Double = -200.00
    private var currentLng : Double = -200.00
    //private lateinit var placesClient: PlacesClient
    private val REQUEST_LOCATION_PERMISSION = 1234
    private val AUTOCOMPLETE_REQUEST_CODE = 332
    private val zoom = 15f
    private val TAG = ItemEditMapFragment::class.java.simpleName


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = activity?.let{ ViewModelProvider(it).get(AdvertisementViewModel::class.java) }!!

        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_item_edit_map, container, false)
    }

    override fun onResume() {
        super.onResume()
        itemEditMapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        itemEditMapView.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        itemEditMapView.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putDouble("itemLat",currentMarker?.position?.latitude ?: -200.00)
        outState.putDouble("itemLng",currentMarker?.position?.longitude ?: -200.00)
        super.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        val lat = savedInstanceState?.getDouble("itemLat") ?: -200.00
        var lng: Double? = null
        if(lat != -200.00)
            lng = savedInstanceState?.getDouble("itemLng") ?: -200.00
        if(lng != null && lat!=-200.00 && lng != -200.00){
            currentLat = lat
            currentLng = lng
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        itemEditMapView.onCreate(savedInstanceState)
        itemEditMapView.onResume()
        try{
            MapsInitializer.initialize(requireContext())
        } catch (e:Exception){
            e.printStackTrace()
        }
        itemEditMapView.getMapAsync {
            mMap = it


            val turin = LatLng(45.0578564352, 7.65664237342)
            /* zoom levels
                    1: World
                    5: Landmass/continent
                    10: City
                    15: Streets
                    20: Buildings
            */
            if(currentLat != -200.00 && currentLng != -200.00){
                val pos = LatLng(
                    currentLat,
                    currentLng
                )
                currentMarker = placeMarkerOnMap(pos)
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos,zoom))
            } else if(vm.temporaryAdvData.latLng != null){
                val pos = LatLng(
                    vm.temporaryAdvData.latLng!!.latitude,
                    vm.temporaryAdvData.latLng!!.longitude
                )
                currentMarker = placeMarkerOnMap(pos)
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos,zoom))
            } else {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(turin,10f))
            }

            setMapLongClick(mMap)
            setMarkerListener()
            setPOIClick(mMap)
            setMarkerDragListener()
            //setMapStyle(mMap)
            enableMyLocation()
        }
        editText_itemMap_location.editText?.setOnEditorActionListener { _, actionId, event ->

            if(actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_SEARCH){

                hideKeyboard(requireContext(), editText_itemMap_location.editText!!)
                val coords = addrToCoord(editText_itemMap_location.editText?.text.toString())
                if(coords == null){
                    Snackbar.make(
                        nav_item_edit_map,
                        "Please, enter a valid address.",
                        Snackbar.LENGTH_LONG
                    )
                        .setAction("Action", null).show()
                } else {
                    currentMarker?.remove()
                    currentMarker = placeMarkerOnMap(coords)
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coords,zoom))
                }
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        //val apiKey = getString(R.string.api_key)
        /*Places.initialize(requireContext(),apiKey)
        placesClient = Places.createClient(requireContext())*/
    }

    private fun hideKeyboard(ctx : Context, view :View){
        val imm: InputMethodManager = ctx.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken,0)
    }

    private fun addrToCoord(addr:String) : LatLng?{
        val addresses = Geocoder(requireContext()).getFromLocationName(addr,1)
        var retVal:LatLng? = null
        if(addresses.size>0){
            retVal = LatLng(addresses[0].latitude,addresses[0].longitude)
        }
        return retVal
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == REQUEST_LOCATION_PERMISSION){
            if(grantResults.contains(PackageManager.PERMISSION_GRANTED)){
                enableMyLocation()
            }
        }
    }

    private fun isPermissionGranted() : Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    private fun placeMarkerOnMap(latLng: LatLng): Marker{
        val snippet = String.format(
            Locale.getDefault(),
            "Lat: %1$.5f, Long: %2$.5f",
            latLng.latitude,
            latLng.longitude
        )
        val addr = getAddress(latLng)
        val bm = vm.drawableConverter
            ?.getBitmap(R.drawable.ic_baseline_shopping_basket_24)
        val markerOptions = MarkerOptions()
            .position(latLng)
            .title(addr)
            .snippet(snippet)
            .draggable(true)
        if(bm != null){
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bm))
        } else {
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(
                BitmapDescriptorFactory.HUE_ORANGE
            ))
        }
        editText_itemMap_location.editText?.setText(addr)
        return mMap.addMarker(markerOptions)
    }

    private fun getAddress(latLng: LatLng):String{
        val geocoder = Geocoder(requireContext())
        val addresses: List<Address>
        val address : Address?
        var addressText = ""

        try{
            addresses = geocoder.getFromLocation(latLng.latitude,latLng.longitude,1)
            if(addresses != null && addresses.isNotEmpty()){
                address = addresses[0]

                var i = 0
                while(i <= address.maxAddressLineIndex){
                    addressText += if(i==0) address.getAddressLine(i) else "\n"+address.getAddressLine(i)
                    i++
                }
            }
        } catch (e: IOException){
            Log.e(TAG,"Error getAddress :" + e.localizedMessage)
        }
        return addressText
    }

    private fun setMarkerDragListener() {
        mMap.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener{
            override fun onMarkerDragEnd(p0: Marker?) {
                if(p0 != null){
                    currentMarker?.remove()
                    currentMarker = placeMarkerOnMap(p0.position)
                }
            }

            override fun onMarkerDragStart(p0: Marker?) {
            }

            override fun onMarkerDrag(p0: Marker?) {
                if(p0 != null) {
                    val snippet = String.format(
                        Locale.getDefault(),
                        "Lat: %1$.5f, Long: %2$.5f",
                        p0.position.latitude,
                        p0.position.longitude
                    )
                    val addr = getAddress(p0.position)
                    p0.snippet = snippet
                    p0.title = addr
                    p0.showInfoWindow()
                } else
                    Log.d("DRAGGING", "NULL")
            }

        })
    }

    private fun enableMyLocation() {
        if(isPermissionGranted()){
            mMap.isMyLocationEnabled = true
            mMap.uiSettings.isMyLocationButtonEnabled=true
            fusedLocationClient.lastLocation.addOnSuccessListener { loc ->
                if(loc != null){
                    lastLocation = loc
                    val currentLatLng = LatLng(loc.latitude,loc.longitude)
                    //placeMarkerOnMap(currentLatLng)
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng,zoom))
                }
            }
        } else {
            activity?.let {
                ActivityCompat.requestPermissions(
                    it,
                    arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_LOCATION_PERMISSION
                )
            }
        }
    }

    private fun setMapLongClick(map:GoogleMap){
        map.setOnMapLongClickListener { latlng ->
            currentMarker?.remove()
            currentMarker = placeMarkerOnMap(latlng)
        }
    }

    private fun setPOIClick(map:GoogleMap){
        map.setOnPoiClickListener{
            val poiMarker = map.addMarker(
                MarkerOptions()
                    .position(it.latLng)
                    .title(it.name)
            )
            poiMarker.showInfoWindow()
        }
    }

    private fun setMarkerListener(){
        mMap.setOnMarkerClickListener {
            Log.d(TAG,"-->"+it.title)
            if(currentMarker == null) {
                it.remove()
                true
            } else {
                if(it.position.latitude == currentMarker!!.position.latitude &&
                    it.position.longitude == currentMarker!!.position.longitude){
                    return@setOnMarkerClickListener false
                }
                it.remove()
                true
            }
        }
    }

    private fun setMapStyle(map:GoogleMap){
        try{
            val success = map.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    requireContext(),
                    R.raw.map_style
                )
            )
            if(!success)
                Log.e(TAG,"Style parsing failed")
        } catch (e: Resources.NotFoundException){
            Log.e(TAG,"Can't find style, error : ",e)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.map_options,menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //super.onOptionsItemSelected(item)
        return when(item.itemId){
            R.id.normal_map -> {
                mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
                true
            }
            R.id.hybrid_map -> {
                mMap.mapType = GoogleMap.MAP_TYPE_HYBRID
                true
            }
            R.id.satellite_map -> {
                mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
                true
            }
            R.id.terrain_map -> {
                mMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
                true
            }
            R.id.save_address -> {
                //loadSearchActivity()
                vm.currentItemMarker = currentMarker

                NavHostFragment.findNavController(nav_host_fragment)
                    .popBackStack(R.id.itemEditFragment,false)

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /*private fun loadSearchActivity() {
        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.OVERLAY,
            Arrays.asList(
                Place.Field.NAME,
                Place.Field.ADDRESS,
                Place.Field.ADDRESS_COMPONENTS,
                Place.Field.LAT_LNG,
                Place.Field.ID
            )
        )
            .build(requireContext())
        startActivityForResult(intent,AUTOCOMPLETE_REQUEST_CODE)
    }*/

    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            AUTOCOMPLETE_REQUEST_CODE->{
                if(resultCode == RESULT_OK && data != null){
                    var place = Autocomplete.getPlaceFromIntent(data)
                    Log.i(TAG,"Place: " + place.name + ", " + place.id+","+place.address)
                } else if (resultCode == AutocompleteActivity.RESULT_ERROR){
                    val status = data?.let { Autocomplete.getStatusFromIntent(it) }
                    Log.i(TAG,"status : "+status?.status)
                } else if( resultCode == RESULT_CANCELED){
                    Log.i(TAG,"status : CANCELLATO")
                }
            }
        }
    }*/
}