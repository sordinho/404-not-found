package it.polito.group03.lab3.second_hand_market.utils

class InvalidUserProfileUpdateException(message: String) : Exception(message)