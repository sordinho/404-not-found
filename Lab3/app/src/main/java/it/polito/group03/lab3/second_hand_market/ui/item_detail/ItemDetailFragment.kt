package it.polito.group03.lab3.second_hand_market.ui.item_detail

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.content.ContextCompat
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.firestore.FieldValue
import com.google.firebase.messaging.FirebaseMessaging
import com.squareup.picasso.Picasso
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.model.Advertisement
import it.polito.group03.lab3.second_hand_market.utils.FCM_API
import it.polito.group03.lab3.second_hand_market.utils.contentType
import it.polito.group03.lab3.second_hand_market.utils.serverKey
import it.polito.group03.lab3.second_hand_market.ui.item_edit.AdvertisementViewModel
import it.polito.group03.lab3.second_hand_market.ui.user_profile.UserViewModel
import kotlinx.android.synthetic.main.bought_items_fragment.*
import kotlinx.android.synthetic.main.fragment_item_detail.*
import kotlinx.android.synthetic.main.fragment_item_detail.htab_tabs
import kotlinx.android.synthetic.main.fragment_item_detail.pager
import org.json.JSONException
import org.json.JSONObject
import kotlin.collections.HashMap


class ItemDetailFragment : Fragment() {
    private lateinit var vm: ItemDetailViewModel
    private lateinit var vmUser: UserViewModel
    private val editViewModel: AdvertisementViewModel by activityViewModels()
    private lateinit var tabCollectionAdapter: TabCollectionAdapter
    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(this.context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        vmUser = activity?.let { ViewModelProvider(it).get(UserViewModel::class.java) }!!
        return inflater.inflate(R.layout.fragment_item_detail, container, false)
    }

    @SuppressLint("ResourceAsColor")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm = activity?.let { ViewModelProvider(it).get(ItemDetailViewModel::class.java) }!!
        vm.setADID(arguments?.getString("advertisementID"))
        arguments?.getString("advertisementOwner")?.let { vm.setAdvertisementOwnerCode(it) }
        loadAdvertisement()
        FAB_interested_item_detail.show()

        FAB_interested_item_detail.setOnClickListener {
            var snack: Snackbar =
                Snackbar.make(this.requireView(), "Added to watchlist", Snackbar.LENGTH_LONG)
            snack.show()
            vm.getRepo().db.collection("items").document(vm.getADID()).update(
                "interestedUsers",
                FieldValue.arrayUnion(vmUser.userProfileId.value.toString())
            )
            vm.getAdvertisement().value?.interestedUsers?.forEach {
                Log.d(
                    "xxxx",
                    "InterestedUser: " + it
                )
            }
            //Notification managing

            //Notification subscribe when the owner changes the availability of the advertisement
            FirebaseMessaging.getInstance().subscribeToTopic("/topics/" + vm.getADID() + "_status")

            //Prepare the notification to send to the owner so s/he will be notified
            val topic ="/topics/" + vm.getADID() + "_interest"

            val notification = JSONObject()
            val notifcationBody = JSONObject()

            try {
                notifcationBody.put("title", "A user is interested in your advertisement!")
                notifcationBody.put(
                    "message",
                    vm.advertisement.value?.title
                )
                notification.put("to", topic)
                notification.put("data", notifcationBody)
            } catch (e: JSONException) {
                Log.e("Notification", "onCreate: " + e.message)
            }
            sendNotification(notification)
        }

        configureTab()
    }

    /**
     * This method configures the tab view.
     */
    private fun configureTab() {
        tabCollectionAdapter = TabCollectionAdapter(this)
        pager.adapter = tabCollectionAdapter
        TabLayoutMediator(htab_tabs, pager) { tab, position ->
            when (position) {
                0 -> tab.text = "Details"
                1 -> tab.text = "Location"
            }
        }.attach()

        pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (position == 1) {
                    pager.setUserInputEnabled(false)
                    appBarLayout.setExpanded(false)
                } else {
                    pager.setUserInputEnabled(true)
                    appBarLayout.setExpanded(true)
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.item_detail, menu)
    }

    private fun sendNotification(notification: JSONObject) {
        val jsonObjectRequest = object : JsonObjectRequest(
            FCM_API, notification,
            Response.Listener { response ->
                //test_notification_button.setText("")
            },
            Response.ErrorListener {
                Toast.makeText(this.context, "Request error", Toast.LENGTH_LONG).show()
            }) {

            override fun getHeaders(): Map<String, String> {
                val params = HashMap<String, String>()
                params["Authorization"] =
                    serverKey
                params["Content-Type"] =
                    contentType
                return params
            }
        }
        requestQueue.add(jsonObjectRequest)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_edit -> {
                if (vm.advertisement.value?.status == getString(R.string.item_status_sold)) {
                    Snackbar.make(
                        this.requireView(),
                        getString(R.string.alert_edit_item_sold),
                        Snackbar.LENGTH_LONG
                    ).show()
                    return false
                }
                val bundle = Bundle()
                bundle.putString("advertisementID", vm.getADID())
                editViewModel.restoreBooleans()
                editViewModel.currentItemMarker=null
                findNavController().navigate(
                    R.id.action_itemDetailFragment3_to_itemEditFragment,
                    bundle
                )
                return true
            }
            else -> {
                return false
            }
        }
    }

    private fun loadAdvertisement() {
        vm.getAdvertisement().observe(viewLifecycleOwner, Observer { data ->
            updateUI(data)
            vm.setOwnedExternal(vmUser.userProfileId.value == vm.getAdvertisementOwnerString())

            setStatus()

            if (vm.getOwnedTrue()) {
                FAB_interested_item_detail.hide()
            } else if(vmUser.userProfileId.value == ""){
                setHasOptionsMenu(false)
                FAB_interested_item_detail.hide()
            }else {
                setHasOptionsMenu(false)
            }
        })

        vm.getRepo().errorOrException.observe(viewLifecycleOwner, Observer {
            if (it) {
                Snackbar.make(
                    this.requireView(),
                    "Error during the loading of advertisement, retry!",
                    Snackbar.LENGTH_LONG
                )
                    .setAction("Action", null).show()
                vm.getRepo().errorOrException.value = false
            }
        })

        vm.repositoryUserProfile.errorOrException.observe(viewLifecycleOwner, Observer {
            if (it) {
                Snackbar.make(
                    this.requireView(),
                    "Error during the loading of user profile, retry!",
                    Snackbar.LENGTH_LONG
                )
                    .setAction("Action", null).show()
                vm.getRepo().errorOrException.value = false
            }
        })
    }

    /**
     * This method allows to change the visual status of advertisement.
     */
    private fun setStatus() {
        if (vm.getAdvertisement().value?.status == "Selling") {
            if (vm.getAdvertisement().value!!.interestedUsers.contains(vmUser.userProfileId.value))
                FAB_interested_item_detail.hide()
            else
                FAB_interested_item_detail.show()
            state.text = "Available"
            state.setTextColor(ContextCompat.getColor(state.context, R.color.colorAccent))
        } else if (vm.getAdvertisement().value?.status == "Blocked") {
            FAB_interested_item_detail.hide()
            state.text = vm.getAdvertisement().value?.status
            state.setTextColor(
                ContextCompat.getColor(
                    state.context,
                    R.color.common_google_signin_btn_text_light_disabled
                )
            )
        } else if (vm.getAdvertisement().value?.status == "Sold") {
            FAB_interested_item_detail.hide()
            state.text = vm.getAdvertisement().value?.status
            state.setTextColor(
                ContextCompat.getColor(
                    state.context,
                    R.color.design_default_color_error
                )
            )
        }
    }

    private fun updateUI(data: Advertisement) {
        imageView_item_detail_picture.setImageURI(Uri.parse(data.thumbnailPath))
        textView_item_detail_title.text = data.title
        //textView_item_detail_description.text = data.description
        if (data.price.isNaN())
            textView_item_detail_price.text = ""
        else
            textView_item_detail_price.text = data.price.toString() + "€"
        textView_item_detail_category.text = data.category + "/" + data.subCategory
        Picasso.get().load(data.thumbnailPath).into(imageView_item_detail_picture)
    }
}

class TabCollectionAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 2

    /**
     * Return a NEW fragment instance in createFragment(int)
     */
    override fun createFragment(position: Int): Fragment {
        var fragment: Fragment?
        when (position) {
            0 -> fragment =
                ItemDetail()
            1 -> fragment =
                ItemDetailMapFragment()
            else -> fragment =
                ItemDetail()
        }
        return fragment
    }
}