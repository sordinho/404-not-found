package it.polito.group03.lab3.second_hand_market.ui.item_detail

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.model.Advertisement
import it.polito.group03.lab3.second_hand_market.ui.user_profile.UserViewModel
import kotlinx.android.synthetic.main.item_detail.*
import java.text.SimpleDateFormat
import java.util.*


class ItemDetail : Fragment() {
    private val vm: ItemDetailViewModel by activityViewModels()
    private val vmUser: UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.item_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadAdvertisement()

        container.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("userID", vm.advertisementOwner)
            bundle.putBoolean("isMyProfile", vm.getOwnedTrue())
            bundle.putBoolean("soldTo", vm.advertisement.value?.soldTo == vmUser.userProfileId.value)
            bundle.putString("advertisementID", vm.adID)
            bundle.putBoolean("commented", vm.advertisement.value!!.commented)
            findNavController().navigate(R.id.action_itemDetailFragment_to_user_profile, bundle)
        }
    }

    private fun loadAdvertisement() {
        vm.getAdvertisement().observe(viewLifecycleOwner, Observer { data ->
            updateUI(data)
            vm.setOwnedExternal(vmUser.userProfileId.value == vm.getAdvertisementOwnerString())
            if (vm.owned) {
                configureInterestedButton()
            } else {
                interested_users_button.visibility = View.GONE
            }
        })

        if (shimmer_view_container_interested != null){
            shimmer_view_container_interested.startShimmer()
        }
        vm.user.observe(viewLifecycleOwner, Observer {
            username.text = it.nickname
            email.text = it.email
            ratingBar.rating = it.rating
            if (it.avatarPath == "") {
                avatar.setImageResource(R.drawable.ic_user)
                shimmer_view_container_interested.stopShimmer()
                if (shimmer_view_container_interested != null) {
                    shimmer_view_container_interested.stopShimmer()
                    shimmer_view_container_interested.visibility = View.GONE
                }
            }else if (!it.avatarPath.isNullOrEmpty())
                Picasso.get().load(it.avatarPath).into(avatar, object : Callback {
                    override fun onSuccess() {
                        if (shimmer_view_container_interested != null) {
                            shimmer_view_container_interested.stopShimmer()
                            shimmer_view_container_interested.visibility = View.GONE
                        }
                    }

                    override fun onError(e: Exception?) {
                        if (shimmer_view_container_interested != null) {
                            shimmer_view_container_interested.stopShimmer()
                            shimmer_view_container_interested.visibility = View.GONE
                        }
                    }

                })
        })
    }

    private fun configureInterestedButton() {
        var i = vm.advertisement.value!!.interestedUsers.size
        interested_users_button.text = i.toString() + " Interested Users"
        if (i == 0) {
            interested_users_button.isClickable = false
            interested_users_button.setTextColor(
                ContextCompat.getColor(
                    interested_users_button.context,
                    R.color.secondary_text
                )
            )
        } else {
            interested_users_button.setTextColor(
                ContextCompat.getColor(
                    interested_users_button.context,
                    R.color.colorAccent
                )
            )
            interested_users_button.isClickable = true
            interested_users_button.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("advertisementID", vm.adID)
                bundle.putParcelable("item", vm.advertisement.value)
                findNavController().navigate(
                    R.id.action_itemDetailFragment_to_interestedUsersFragment,
                    bundle
                )
            }
        }
    }

    private fun updateUI(data: Advertisement) {
        textView_item_detail_description.text = data.description
        textView_item_detail_location.text = data.location
        textView_item_detail_expiry_date.text = data.expiryDate
        val newDates = vm.advertisement.value!!.expiryDate.split("/")
        val newDate = newDates[2] + "/" + newDates[1] + "/" + newDates[0]

        if (newDate < SimpleDateFormat("yyyy/MM/dd").format(Date()) && vm.advertisement.value!!.soldTo == "") {
            textView_item_detail_expiry_date.setTextColor(ContextCompat.getColor(requireContext(), R.color.design_default_color_error))
            time.setColorFilter(ContextCompat.getColor(requireContext(), R.color.design_default_color_error), android.graphics.PorterDuff.Mode.SRC_IN)
            error.visibility = View.VISIBLE
            if (vm.owned) {
                error.text = "The date is expired! Please, change the expire date!"
            }
        } else if (newDate < SimpleDateFormat("yyyy/MM/dd").format(Date()) && vm.advertisement.value!!.soldTo != "") {
            textView_item_detail_expiry_date.setTextColor(ContextCompat.getColor(requireContext(), R.color.design_default_color_error))
            time.setColorFilter(ContextCompat.getColor(requireContext(), R.color.design_default_color_error), android.graphics.PorterDuff.Mode.SRC_IN)
            error.visibility = View.INVISIBLE
        } else{
            textView_item_detail_expiry_date.setTextColor(ContextCompat.getColor(requireContext(), R.color.secondary_text))
            time.setColorFilter(ContextCompat.getColor(requireContext(), R.color.primary_text), android.graphics.PorterDuff.Mode.SRC_IN)
            error.visibility = View.INVISIBLE
        }
    }
}
