package it.polito.group03.lab3.second_hand_market.model

import android.os.Parcelable
import android.util.Log
import com.google.firebase.firestore.GeoPoint
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class Advertisement(
    val thumbnailPath: String = "",
    val title: String = "",
    val description: String = "",
    val price: Double = 0.0,
    val category: String = "",
    val subCategory: String = "",
    val location: String = "",
    val expiryDate: String = "",
    val status: String = "",// "Selling" - "Sold" - "Blocked"
    val soldTo: String = "",
    val owner: String = "",
    var titleAsArray: List<String> = arrayListOf(),
    var interestedUsers: List<String> = arrayListOf(),
    var latLng: @RawValue GeoPoint? = null,
    var commented: Boolean = false
) : Parcelable {

    fun generateTitleAsArray() {
        var words = this.title.toLowerCase().split(" ")
        var titleAsArray = arrayListOf<String>()

        for (i in 0..words.size - 1) {
            for (j in 0..words[i].length) {
                titleAsArray.add(words[i].substring(0, j))
                Log.d("XXX", words[i].substring(0, j))
            }
            if (i != words.size - 1)
                titleAsArray.add(words[i] + " ")

            var w = words[i]

            for (k in (i + 1)..words.size - 1) {
                for (j in 0..words[k].length) {
                    titleAsArray.add(w + " " + words[k].substring(0, j))
                    Log.d("XXX", w + " " + words[k].substring(0, j))
                }
                w += " " + words[k]
                titleAsArray.add(w)
            }

        }
        titleAsArray.add(this.title)
        this.titleAsArray = titleAsArray
    }
}