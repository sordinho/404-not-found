package it.polito.group03.lab3.second_hand_market.utils

import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_GET_CONTENT
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.PopupMenu
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.theartofdev.edmodo.cropper.CropImage
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.ui.item_edit.ItemEditFragment
import it.polito.group03.lab3.second_hand_market.ui.user_profile.EditProfileFragment
import java.io.*

class CameraGallery(
    val view: View,
    private val applicationContext: Context,
    private val currentFragment: Fragment,
    showPopup: Boolean = true
) {

    val REQUEST_IMAGE_CAPTURE = 1
    val REQUEST_IMAGE_GALLERY = 2
    var imgUri: Uri? = null

    private var filename = ""

    var photoPath: String = ""
    var thumbnailPath: String = ""

    private var bitmap: Bitmap? = null
    var heightThumbnail = 720
    var widthThumbnail = 1280

    // Initialization block equivalent to showCameraGalleryPopupMenu
    init {
        if(currentFragment is EditProfileFragment) {
            filename = "tmpAvatar.jpg"
        } else if(currentFragment is ItemEditFragment){
            filename = "tmpItem.jpg"
        }
        if (showPopup)
            showCameraGalleryPopupMenu()
    }

    /**
     * This function need to be called for displaying popup menu for selecting between camera and gallery
     * used in itemEditFragment, should be used also in profileEditFragment
     *
     * Includes code for showing icons in popup menu items
     *
     * requires:
     *  - view (button where to show popup)
     *  - fragment (the one who will launch intents
     */
    private fun showCameraGalleryPopupMenu() {
        val popupMenu: PopupMenu = PopupMenu(applicationContext, view)
        popupMenu.menuInflater.inflate(R.menu.popup_menu_camera_gallery_picture, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.action_edit_picture_photo -> {
                    takePictureFromCamera()
                }
                R.id.action_edit_picture_gallery -> {
                    takePictureFromGallery()
                }
            }
            true
        }
        try {
            val fieldMPopup = PopupMenu::class.java.getDeclaredField("mPopup")
            fieldMPopup.isAccessible = true
            val mPopup = fieldMPopup.get(popupMenu)
            mPopup.javaClass
                .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                .invoke(mPopup, true)
        } catch (e: Exception) {
            Log.e("Main", "Error showing menu icons.", e)
        } finally {
            popupMenu.show()
        }
    }

    /**
     * Select a picture from the phone's gallery
     */
    private fun takePictureFromGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = ACTION_GET_CONTENT
        currentFragment.startActivityForResult(
            Intent.createChooser(intent, ""),
            REQUEST_IMAGE_GALLERY
        )
    }

    /**
     * Take a picture using camera Intent
     */
    private fun takePictureFromCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            /* Ensure that there's a camera activity to handle the intent */
            takePictureIntent.resolveActivity(currentFragment.requireActivity().packageManager!!)
                ?.also {
                    /* Create the File where the photo should go */
                    val photoFile: File? = try {
                        createImageFile()
                    } catch (ex: IOException) {
                        /* Error occurred while creating the File */
                        Log.d("Error", "Error occurred while creating the File")
                        null
                    }
                    // Save photo path
                    photoPath = photoFile!!.absolutePath
                    val sharedPref = applicationContext.getSharedPreferences(
                        "it_polito_group03_lab02_preferences",
                        Context.MODE_PRIVATE
                    )
                    sharedPref?.edit()?.putString("persistentCurrentPhotoPath", photoPath)?.apply()
                    /* Continue only if the File was successfully created */
                    photoFile.also {
                        val photoURI: Uri = FileProvider.getUriForFile(
                            applicationContext,
                            applicationContext.packageName
                                .toString() + ".fileprovider",
                            it
                        )
                        takePictureIntent.putExtra(
                            MediaStore.EXTRA_OUTPUT,
                            photoURI
                        )
                        imgUri = photoURI
                        sharedPref?.edit()?.putString("persistentCurrentUri", imgUri.toString())
                            ?.apply()
                        currentFragment.startActivityForResult(
                            takePictureIntent,
                            REQUEST_IMAGE_CAPTURE
                        )
                    }
                }
        }
    }

    /**
     * This method creates a file with a constant name (userImage.jpg) stored in the application directory
     */
    @Throws(IOException::class)
    private fun createImageFile(): File {
        /* Create an image file name */
        if(filename.isEmpty())
            filename = System.currentTimeMillis().toString() + ".jpg"
        val storageDir: File? =
            currentFragment.requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)

        return File(storageDir, filename)
    }

    /**
     * This method creates a thumbnail rappresentation of image, it calls the createImageFile method.
     */
    fun createThumbnail() {
        try {
            val thumbImage = ThumbnailUtils.extractThumbnail(
                BitmapFactory.decodeFile(photoPath),
                widthThumbnail,
                heightThumbnail
            )
            var file: File = createImageFile()
            val stream: OutputStream = FileOutputStream(file)
            thumbImage.compress(Bitmap.CompressFormat.JPEG, 50, stream)
            stream.flush()
            stream.close()
            thumbnailPath = file.absolutePath
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /**
     * This function checks the angle of image's rotation. If the image has a angle != 0, the method calls rotateImage method.
     */
    fun handleRotation() {
        val ei = ExifInterface(photoPath)

        val ins: InputStream? =
            applicationContext.contentResolver?.openInputStream(imgUri!!)/* loading the bitmap */
        bitmap = BitmapFactory.decodeStream(ins)
        ins?.close()

        if (bitmap != null) {
            val orientation: Int = ei.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )
            Log.d("kkk", "Orientation rot handle: $orientation")
            bitmap = when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap!!, 90F)
                ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap!!, 180F)
                ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap!!, 270F)
                ExifInterface.ORIENTATION_NORMAL -> bitmap
                else -> bitmap
            }
        }
    }

    /**
     * This method rotates the image by a preset amount of degrees.
     */
    private fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }

    /**
     * This method allows you to store an image on disk given a bitmap.
     * The name of the image is in accordance with the name given by the createImageFile method.
     */
    fun bitmapToFile(bitmap: Bitmap) {
        try {
            // Initialize a new file instance to save bitmap object
            val file: File = createImageFile()
            val photoURI: Uri = FileProvider.getUriForFile(
                applicationContext,
                applicationContext.packageName
                    .toString() + ".fileprovider",
                file
            )
            imgUri = photoURI
            photoPath = file.absolutePath
            // Compress the bitmap and save in jpg format
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun launchImageCrop(cropRatioX: Int = 1920, cropRatioY: Int = 1080) {
        CropImage
            .activity(imgUri)
            .setAspectRatio(cropRatioX, cropRatioY)
            .setOutputUri(imgUri)
            .setMultiTouchEnabled(true)
            .setCropMenuCropButtonIcon(R.drawable.ic_save)
            .start(applicationContext, currentFragment)
    }
}