package it.polito.group03.lab3.second_hand_market.model.Repository

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import com.google.firebase.storage.ktx.storageMetadata
import it.polito.group03.lab3.second_hand_market.model.Comment
import it.polito.group03.lab3.second_hand_market.model.UserProfile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File

class UserRepository() {
    val db = Firebase.firestore
    private val auth : FirebaseAuth = Firebase.auth
    private val storage : FirebaseStorage = Firebase.storage
    private val storageRef = storage.reference
    val users = db.collection("users")
    var profile : MutableLiveData<UserProfile> = MutableLiveData()
    var otherProfile : MutableLiveData<UserProfile> = MutableLiveData()
    val idOtherProfile: MutableLiveData<String> = MutableLiveData()
    var loggedId : MutableLiveData<String> = MutableLiveData()
    var loginResult : MutableLiveData<Boolean> = MutableLiveData()
    var currentUser : ListenerRegistration? = null
    var otherUser : ListenerRegistration? = null
    var comments = MutableLiveData<MutableList<Comment>>()
    var commentUsers = MutableLiveData<MutableMap<String, UserProfile>>()
    var resultAddComment = MutableLiveData<Boolean>()
    var errorOrException = MutableLiveData<Boolean>()
    var addCommentErrorOrException = MutableLiveData<Boolean>()
    fun unsubscribeToUser(){
        currentUser?.remove()
    }

    suspend fun getLoggedUserProfileCoroutine(firstTime: Boolean = false) {
        withContext(Dispatchers.IO) {

            //Log.w("THREAD","Actual getLoggedUserProfileCoroutine--> "+Thread.currentThread().id)
            val curr = auth.currentUser ?: return@withContext
            if( !loggedId.value.isNullOrEmpty() )  {
                if (firstTime) {
                    if (currentUser != null)
                        unsubscribeToUser()
                    currentUser = users.document(loggedId.value?:"")
                        .addSnapshotListener { snapshot, e ->
                            if (e != null) {
                                //Log.d("DBG", "Listen failed", e)
                                return@addSnapshotListener
                            }
                            if (snapshot != null && snapshot.exists()) {
                                profile.value = snapshot.toObject(UserProfile::class.java)
                                //Log.d("DBG","getLoggedUser --" +profile.value?.latLng.toString())
                                loggedId.value = snapshot.id

                                loginResult.value=true
                            }
                        }
                }
                users.document(loggedId.value!!).get()
                    .addOnSuccessListener { snapshot ->
                        profile.value = snapshot.toObject(UserProfile::class.java) ?: UserProfile()
                        //Log.d("DBG","getLoggedUser1 --" +profile.value?.latLng.toString())
                        loggedId.value = snapshot.id
                        if(firstTime)
                            loginResult.value=true
                    }
                    .addOnFailureListener { exception ->
                        //Log.d("TAG", "Error getting documents: ", exception)
                        profile.value =
                            UserProfile()
                        loggedId.value = ""

                        loginResult.value=false
                        return@addOnFailureListener
                    }
            } else {
                users
                    .whereEqualTo("email",curr.email)
                    .get()
                    .addOnSuccessListener { querySnapshot ->
                        val docs = querySnapshot.documents
                        if(docs.size > 0){
                            profile.value = docs[0].toObject(UserProfile::class.java) ?: UserProfile()
                            //Log.d("DBG","getLoggedUser2 --" +profile.value?.latLng.toString())
                            loggedId.value = docs[0].id
                            if(firstTime)
                                loginResult.value=true
                            if(firstTime) {
                                //currentUserId = docs[0].id
                                if(currentUser != null){
                                    unsubscribeToUser()
                                }
                                currentUser = users.document(loggedId.value?:"")
                                    .addSnapshotListener{ snapshot, e ->
                                        if ( e != null ){
                                            //Log.d("DBG", "Listen failed",e)
                                            return@addSnapshotListener
                                        }
                                        if( snapshot != null && snapshot.exists() ){
                                            profile.value = snapshot.toObject(UserProfile::class.java)
                                            //Log.d("DBG","getLoggedUser3 --" +profile.value?.latLng.toString())

                                            loggedId.value = snapshot.id
                                            if(firstTime)
                                                loginResult.value = true
                                        }
                                    }
                            }
                        } else {
                            //Log.d("TAG", "Error getting user")
                            profile.value =
                                UserProfile()
                            loggedId.value = ""
                            loginResult.value=false
                        }
                    }
                    .addOnFailureListener{ exception ->
                        Log.d("TAG", "Error getting documents: ", exception)
                        profile.value =
                            UserProfile()
                        loggedId.value = ""
                        loginResult.value = false
                        return@addOnFailureListener
                    }
            }
        }
    }

    fun getUserFromEmail(userEmail: String, firstTime: Boolean = false){
        //Log.w("THREAD","Actual getUserFromEmail--> "+Thread.currentThread().id)
        users.whereEqualTo("email",userEmail).get()
            .addOnSuccessListener { querySnapshot ->
                val docs = querySnapshot.documents
                if(docs.size > 0){
                    profile.value = docs[0].toObject(UserProfile::class.java) ?: UserProfile()
                    //Log.d("DBG","getUserFromEmail --" +profile.value?.latLng.toString())

                    loggedId.value = docs[0].id
                    loginResult.value=true
                    if(firstTime) {
                        if(currentUser != null){
                            unsubscribeToUser()
                        }
                        currentUser = users.document(loggedId.value?:"")
                            .addSnapshotListener{ snapshot, e ->
                                if ( e != null ){
                                    Log.d("DBG", "Listen failed",e)
                                    return@addSnapshotListener
                                }
                                if( snapshot != null && snapshot.exists() ){
                                    profile.value = snapshot.toObject(UserProfile::class.java)
                                   // Log.d("DBG","getUserFromEmail --" +profile.value?.latLng.toString())
                                    loggedId.value = snapshot.id
                                }
                            }
                    }
                } else {
                    Log.d("TAG", "Error getting user")
                    profile.value =
                        UserProfile()
                    loggedId.value = ""
                    loginResult.value=false
                }
            }
            .addOnFailureListener{ exception ->
                Log.d("TAG", "Error getting documents: ", exception)
                profile.value =
                    UserProfile()
                loggedId.value = ""
                loginResult.value=false
                return@addOnFailureListener
            }
    }

    suspend fun authWithEmailAndPasswordCoroutine(email : String, pwd : String){
        withContext(Dispatchers.IO) {
            //Log.w("THREAD","Actual authWithEmailAndPasswordCoroutine--> "+Thread.currentThread().id)
            auth.signInWithEmailAndPassword(email, pwd)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.d("TAG", "signInWithEmail : success");
                        val user = auth.currentUser
                        getUserFromEmail(user?.email.toString(), true)

                    } else {
                        Log.d("TAG", "singInWithEmail : failure : " + task.exception)

                        profile.value=
                            UserProfile()
                        loggedId.value = ""
                        loginResult.value=false
                    }
                }
        }
    }

    /*suspend fun coroutineAuthWithGoogle(email:String){
        withContext(Dispatchers.IO){
            val credentials = GoogleAuthProvider.getCredential(email,null)
            auth.signInWithCredential(credentials)
                .addOnCompleteListener {
                    task->
                    if(task.isSuccessful){
                        Log.d("DEBUG","signed in with credentials : success")
                        val user = auth.currentUser
                        getUserFromEmail(user?.email.toString(),true)
                    }else {
                        Log.d("DEBUG","signedInWithCredentials : unsuccess")
                        profile.value= UserProfile()
                        loggedId.value = ""
                        loginResult.value=false
                    }
                }
        }
    }*/

    fun logOut(){
        auth.signOut()
        unsubscribeToUser()
        profile.value =
            UserProfile()
        loggedId.value = ""
        loginResult.value=false
    }

    fun isLogged() : Boolean{
        val currentUser = auth.currentUser
        if(currentUser == null){
            unsubscribeToUser()
            profile.value =
                UserProfile()
            loggedId.value = ""
            loginResult.value=false
            return false
        }
        loginResult.value=true
        return true
    }

    /*fun authWithToken(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken,null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener{
                task ->
                if(task.isSuccessful){
                    Log.d("DEBUG " ,"signedInWithCredentials : success")
                    val user = auth.currentUser
                    getUserFromEmail(user?.email.toString())
                } else {
                    Log.d("DEBUG ", "signedInWithCredentials : unsuccess")
                }
            }
    }*/

    private fun newUser(path: String = "",
                        fullname: String,
                        nickname: String,
                        location: String,
                        email: String,
                        geopoint:GeoPoint?,
                        registerSnapshot: Boolean = true,
                        fromScratch: Boolean = true){
        //Log.w("THREAD","Actual newUser--> "+Thread.currentThread().id)

        var data: HashMap<String, Any>

        if(geopoint != null){
            data = hashMapOf(
                "fullname" to fullname,
                "email" to email,
                "location" to location,
                "nickname" to nickname,
                "latLng" to geopoint
            )
        } else {
            data = hashMapOf(
                "fullname" to fullname,
                "email" to email,
                "location" to location,
                "nickname" to nickname
            )
        }
        if(path.isNotEmpty() && !fromScratch)
            data["avatarPath"] = path
        else if(fromScratch)
            data["avatarPath"] = path

        if(fromScratch) {
            users
                .document()
                .set(data)
                .addOnSuccessListener {
                    //Log.d("TAG", "OK DOCUMENTO CREATO")
                    users.whereEqualTo("email", email).get()
                        .addOnSuccessListener { querySnapshot ->
                            val docs = querySnapshot.documents
                            if (docs.size > 0) {
                                loggedId.value = docs[0].id
                                loginResult.value=true
                                if(path.isNotEmpty())
                                    uploadUserAvatarImage(loggedId.value?:"", path)
                                if (registerSnapshot) {
                                    currentUser = users.document(loggedId.value?:"")
                                        .addSnapshotListener { snapshot, e ->
                                            if (e != null) {
                                                Log.d("DBG", "Listen failed", e)
                                                return@addSnapshotListener
                                            }
                                            if (snapshot != null && snapshot.exists()) {
                                                profile.value =
                                                    snapshot.toObject(UserProfile::class.java)
                                                //Log.d("DBG","newUser --" +profile.value?.latLng.toString())

                                                loggedId.value = snapshot.id
                                                loginResult.value=true
                                            }
                                        }
                                }
                            }
                        }
                        .addOnFailureListener { exception ->
                            Log.d(
                                "TAG",
                                "Error getting documents: ",
                                exception
                            )
                            loginResult.value=false
                        }
                }
                .addOnFailureListener { exception ->
                    Log.d(
                        "TAG",
                        "Error getting documents: ",
                        exception
                    )

                    loginResult.value=false
                }
        } else {
            users
                .document(loggedId.value?:"")
                .update(data as Map<String, Any>)
                .addOnSuccessListener {
                    //Log.d("TAG", "OK DOCUMENTO CAMBIATO")
                    if(path.isNotEmpty())
                        uploadUserAvatarImage(loggedId.value?:"", path)
                }
                .addOnFailureListener {
                    Log.d(
                        "TAG",
                        "Error getting documents: ",
                        it
                    )

                    loginResult.value=false
                }
        }
    }

    suspend fun createNewUserDocumentCoroutine(path: String, fullname: String, nickname: String, location: String, email: String, geopoint: GeoPoint?, password: String){
        withContext(Dispatchers.IO) {
            //Log.w("THREAD","Actual createNewUserDocumentCoroutine--> "+Thread.currentThread().id)
            //Log.w("GEOPOINT",geopoint?.toString() ?: "null")
            users.whereEqualTo("email", email)
                .get()
                .addOnSuccessListener {
                    val usersWithSameNick: List<DocumentSnapshot> = it.toList()
                    if (usersWithSameNick.isNotEmpty())
                        return@addOnSuccessListener

                    auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d("TAG", "createUserWithEmail:success")
                                newUser(path, fullname, nickname, location, email, geopoint, true, true)
                            } else {
                                Log.w("TAG", "createUserWithEmail:failure", task.exception)
                                unsubscribeToUser()
                                profile.value =
                                    UserProfile()
                                loggedId.value = ""
                            }

                        }
                }
                .addOnFailureListener {
                    Log.d("TAG", "Cannot create a new user if this query fails.",it)
                }
        }
    }

    suspend fun getUserFromEmailCoroutine(userEmail: String, firstTime: Boolean) {
        withContext(Dispatchers.IO) {

            //Log.w("THREAD","Actual getUserFromEmailCoroutine--> "+Thread.currentThread().id)

            users.document(userEmail).get()
                .addOnSuccessListener { snapshot ->
                    otherProfile.value = snapshot.toObject(UserProfile::class.java) ?: UserProfile()

                    //Log.d("DBG","getUserFromEmail --" +otherProfile.value?.latLng.toString())

                    otherUser = users.document(snapshot.id)
                        .addSnapshotListener { snapshot2, e ->
                            if (e != null) {
                                Log.d("DBG", "Listen failed", e)
                                return@addSnapshotListener
                            }
                            if (snapshot2 != null && snapshot2.exists()) {
                                otherProfile.value = snapshot2.toObject(UserProfile::class.java)
                                //Log.d("DBG","getUserFromEmail --" +otherProfile.value?.latLng.toString())
                                idOtherProfile.value = snapshot2.id
                            }
                        }
                }
                .addOnFailureListener { exception ->
                    Log.d("TAG", "Error getting documents: ", exception)
                    otherProfile.value =
                        UserProfile()
                    idOtherProfile.value = ""
                    return@addOnFailureListener
                }
        }
    }

    suspend fun updateUserDocumentCoroutine(path: String, fullname: String, nickname: String, location: String, email: String, pwd:String, geopoint:GeoPoint?, emailChanged: Boolean) {
        withContext(Dispatchers.IO) {
            //Log.w("THREAD","Actual updateUserCoroutine--> "+Thread.currentThread().id)
            users
                .whereEqualTo("email", email)
                .get()
                .addOnSuccessListener {
                    val usersWithSameNick: List<DocumentSnapshot> = it.toList()
                    if (usersWithSameNick.isNotEmpty() && emailChanged)
                        return@addOnSuccessListener

                    if (emailChanged) {
                        auth.currentUser
                            ?.reauthenticate(
                                EmailAuthProvider.getCredential(
                                    profile.value?.email ?: "", pwd
                                )
                            )
                            ?.addOnSuccessListener {
                                auth.currentUser
                                    ?.updateEmail(email)
                                    ?.addOnSuccessListener {
                                        newUser(
                                            path,
                                            fullname,
                                            nickname,
                                            location,
                                            email,
                                            geopoint,
                                            emailChanged,
                                            false
                                        )
                                    }
                                    ?.addOnFailureListener { e ->
                                        Log.d(
                                            "TAG",
                                            "Error while updating user account",
                                            e
                                        )
                                    }
                            }
                            ?.addOnFailureListener { e ->
                                Log.d(
                                    "TAG",
                                    "Error while updating user account",
                                    e
                                )
                            }
                    } else
                        newUser(path, fullname, nickname, location, email, geopoint, emailChanged, false)
                }
                .addOnFailureListener {
                    e->
                    Log.d(
                        "TAG",
                        "Error while updating user account",
                        e
                    )
                }
        }
    }

    /**
     * Upload the user image to firebase storage and get a link for download back
     */
    private fun uploadUserAvatarImage(documentId: String, path: String) {
        // Upload image
        // Create a storage reference from our app

        val metadata = storageMetadata {
            contentType = "image/jpg"
        }

        var file = Uri.fromFile(File(path))
        val imageRef = storageRef.child("users/$documentId")
        var downloadUri: Uri?
        val uploadTask = imageRef.putFile(file, metadata)

        uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            imageRef.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                downloadUri = task.result
                Log.d("XXX", "Immagine caricata: $downloadUri")
                setDownloadUrlUsrImage(documentId, downloadUri!!)
            } else {
                Log.d("XXX", "Immagine NON caricata")
            }
        }
    }

    /**
     * Set the download url inside the DB for downloading the user image
     */
    private fun setDownloadUrlUsrImage(docID: String, downloadUri: Uri) {
        users.document(docID).update(
            "avatarPath", downloadUri.toString()
        ).addOnCompleteListener { task ->
            if (task.isSuccessful)
                Log.d("XXX", "modifca foto path OK")
            else
                Log.d("XXX", "ERROR modifica foto path")
        }
    }

    fun unsubscribeToOtherUser() {
        otherUser?.remove()
    }

    fun addCommentAndUpdateUserProfile(comment: Comment, otherUserProfile: UserProfile, advertisementID: String): MutableLiveData<Boolean> {
        db.runTransaction {
            addComment(comment)
            var tot_rating = 0.0F
            comments.value!!.forEach {
                tot_rating += it.numStars
            }
            otherUserProfile.rating = (tot_rating + comment.numStars)/ (comments.value!!.size + 1)
            val data = hashMapOf(
                "fullname" to otherUserProfile.fullname,
                "email" to otherUserProfile.email,
                "location" to otherUserProfile.location,
                "nickname" to otherUserProfile.nickname,
                "rating" to otherUserProfile.rating
            )

            users.document(comment.owner).update(data as Map<String, Any>).addOnSuccessListener {
                Log.d("XXX", "Ok")
            }.addOnFailureListener {
                Log.d("XXX", it.message)
                addCommentErrorOrException.value = true
            }

            db.collection("items").document(advertisementID).update("commented", true).addOnCanceledListener {
                Log.d("XXX", "Ok")
            }.addOnFailureListener {
                Log.d("XXX", it.message)
                addCommentErrorOrException.value = true
            }
        }.addOnSuccessListener { result ->
            Log.d("XXX", "Transaction success: $result")
            resultAddComment.value = true
        }.addOnFailureListener { e ->
            Log.w("XXX", "Transaction failure.", e)
            addCommentErrorOrException.value = true
        }
        return resultAddComment
    }

    fun addComment(comment: Comment) {
        db
            .collection("comments")
            .document()
            .set(comment)
            .addOnCompleteListener {task ->
                if (task.isSuccessful)
                    Log.d("XXX", "Aggiunto commento")
                else {
                    Log.d("XXX", "Commento non aggiunto")
                    addCommentErrorOrException.value = true
                }
            }.addOnCanceledListener {
                addCommentErrorOrException.value = true
            }
    }

    /**
     * This method retrieves a comments for a specific user.
     */
    fun getComments(owner: String): LiveData<MutableList<Comment>> {
        db
            .collection("comments")
            .orderBy("date", Query.Direction.DESCENDING)
            .whereEqualTo("owner", owner)
            .addSnapshotListener { querySnapshot, firebaseFirestoreException ->
                if (firebaseFirestoreException != null) {
                    Log.d("XXX", firebaseFirestoreException.message.toString())
                    errorOrException.value = true
                }else if (querySnapshot != null) {
                    val comments = mutableListOf<Comment>()
                    for (document in querySnapshot.documents) {
                        val comment = document.toObject(Comment::class.java)
                        if (comment != null)
                            comments.add(comment)
                    }
                    this.comments.value = comments
                }
            }
        return this.comments
    }

    /**
     * This method returns a map with documentID and user profile that wrote the comments.
     */
    fun getUsers(id: String): LiveData<MutableMap<String, UserProfile>> {
        commentUsers.value = mutableMapOf()
        db
            .collection("users")
            .document(id)
            .addSnapshotListener { documentSnapshot, firebaseFirestoreException ->
                if (firebaseFirestoreException != null) {
                    errorOrException.value = true
                }else if (documentSnapshot != null) {
                    val u = mutableMapOf<String, UserProfile>()
                    val id = documentSnapshot.id
                    val user = documentSnapshot.toObject(UserProfile::class.java)
                    if (user != null && commentUsers.value != null) {
                        u[id] = user
                        commentUsers.value!!.putAll(u)
                    }
                    commentUsers.value = commentUsers.value
                }
            }
        return commentUsers
    }

    var user = MutableLiveData<UserProfile>()
    /**
     * This method returns a user profile associated to a specific advertisement owner.
     */
    fun getUser(advertisementOwner: String): MutableLiveData<UserProfile> {
        db.collection("users").document(advertisementOwner).addSnapshotListener { documentSnapshot, firebaseFirestoreException ->
            if (firebaseFirestoreException != null) {
                errorOrException.value = true
            }else if (documentSnapshot != null) {
                val user = documentSnapshot.toObject(UserProfile::class.java)
                this.user.value = user
            }
        }
        return user
    }
}