package it.polito.group03.lab3.second_hand_market

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_splash_screen.*

class SplashScreenActivity : AppCompatActivity() {

    lateinit var handler: Handler
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        val from_top = AnimationUtils.loadAnimation(this, R.anim.from_top)
        val from_bottom = AnimationUtils.loadAnimation(this, R.anim.from_bottom)
        icon.animation = from_top
        powered.animation = from_bottom
        group3.animation = from_bottom

        handler = Handler()
        handler.postDelayed({

            // Delay and Start Activity
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        } , 400) // here we're delaying to startActivity after 3seconds
    }
}
