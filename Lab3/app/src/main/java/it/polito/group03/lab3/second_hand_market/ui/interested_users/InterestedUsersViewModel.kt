package it.polito.group03.lab3.second_hand_market.ui.interested_users

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import it.polito.group03.lab3.second_hand_market.model.Advertisement
import it.polito.group03.lab3.second_hand_market.model.Repository.AdvertisementRepository
import it.polito.group03.lab3.second_hand_market.model.UserProfile

class InterestedUsersViewModel : ViewModel() {
    val repository =
        AdvertisementRepository()
    lateinit var advertisement: Advertisement
    lateinit var advertisementID: String
    lateinit var users : LiveData<MutableMap<String, UserProfile>>

    fun fetchUsers(): LiveData<MutableMap<String, UserProfile>> {
        val usrs =  MutableLiveData<MutableMap<String, UserProfile>>()
        usrs.value = mutableMapOf()
        repository.getInterestedUsers(advertisementID).observeForever{ users ->
            usrs.value = users
        }
        this.users = usrs
        return users
    }

    override fun onCleared() {
        super.onCleared()
        repository.unSubscribe()
    }
}