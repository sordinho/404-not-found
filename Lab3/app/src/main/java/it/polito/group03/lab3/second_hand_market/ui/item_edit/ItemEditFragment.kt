package it.polito.group03.lab3.second_hand_market.ui.item_edit

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.GeoPoint
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.model.*
import it.polito.group03.lab3.second_hand_market.ui.user_profile.UserViewModel
import it.polito.group03.lab3.second_hand_market.utils.CameraGallery
import it.polito.group03.lab3.second_hand_market.utils.FCM_API
import it.polito.group03.lab3.second_hand_market.utils.contentType
import it.polito.group03.lab3.second_hand_market.utils.serverKey
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_item_edit.*
import org.json.JSONException
import org.json.JSONObject
import java.io.InputStream
import java.util.*
import java.util.stream.Collectors
import kotlin.collections.HashMap


class ItemEditFragment : Fragment() {
    private var currentThumbnailPath: String = ""
    private lateinit var cameraGallery: CameraGallery
    private val advertisementViewModel: AdvertisementViewModel by activityViewModels()
    private lateinit var userViewModel: UserViewModel
    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(this.context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        userViewModel = activity?.let { ViewModelProvider(it).get(UserViewModel::class.java) }!!
        advertisementViewModel.setAssetConverter(userViewModel.drawableConverter)

        return inflater.inflate(R.layout.fragment_item_edit, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Setup popup menu for camera / gallery
        edit_item_image.setOnClickListener {
            cameraGallery = CameraGallery(edit_item_image, requireContext(), this)
        }

        if (arguments != null && arguments?.getBoolean("fromEditButton") == true) {
            arguments?.putBoolean("fromEditButton", false)
            advertisementViewModel.currentItemMarker = null
        }
        // Fill form
        advertisementViewModel.initAdvertisement(arguments?.getString("advertisementID") ?: "")
        if (!advertisementViewModel.savedInstanceState && !advertisementViewModel.loadFromTemporaryAdv)
            if (advertisementViewModel.advertisementID != "") {
                loadAdvertisement()
            }

        if (advertisementViewModel.advertisementID != "") {
            advertisementViewModel.initInterestedUsersEmails()
                .observe(viewLifecycleOwner, Observer {
                    setupDropDownBuyer(it)
                })
        }

        textField_item_edit_location.editText?.setOnClickListener {
            listenerEditLocation()
        }
        //textField_item_edit_location.setEndIconOnClickListener {
        btn_edit_item_location.setOnClickListener {
            listenerEditLocation()
        }
        // Setup dropdown menus for categories
        setupDropdownMenusCategories()

        // Hide soft keyboard on spinner click
        hideSoftKeyboardOnViewClick(textField_item_edit_category.editText!!)
        hideSoftKeyboardOnViewClick(textField_item_edit_subCategory.editText!!)

        // Setup Date Picker
        textField_item_edit_expiry_date.editText!!.setOnClickListener {
            setupDatePicker()
        }
    }

    private fun listenerEditLocation(){
        var geopoint: GeoPoint? = null
        if (advertisementViewModel.currentItemMarker != null)
            geopoint = GeoPoint(
                advertisementViewModel.currentItemMarker!!.position.latitude,
                advertisementViewModel.currentItemMarker!!.position.longitude
            )
        else if (advertisementViewModel.advertisement.value?.latLng != null)
            geopoint = GeoPoint(
                advertisementViewModel.advertisement.value!!.latLng!!.latitude,
                advertisementViewModel.advertisement.value!!.latLng!!.longitude
            )
        if (geopoint != null)
            Log.d("TEST", "NAV TO MAP WITH : " + geopoint.toString())
        else
            Log.d("TEST", "NAV TO MAP WITH : " + geopoint.toString())
        var price = Double.NaN
        if (textField_item_edit_price.editText?.text.toString() != "")
            price = textField_item_edit_price.editText?.text.toString().toDouble()
        advertisementViewModel.temporaryAdvData = Advertisement(
            item_image.tag?.toString() ?: ""
            , textField_item_edit_title.editText?.text.toString()
            , textField_item_edit_description.editText?.text.toString()
            , price
            , textField_item_edit_category.editText?.text.toString()
            , textField_item_edit_subCategory.editText?.text.toString() ?: ""
            , textField_item_edit_location.editText?.text.toString()
            , textField_item_edit_expiry_date.editText?.text.toString()
            , textField_item_edit_status.editText?.text.toString()
            , textField_item_edit_buyer.editText?.text.toString()
            , userViewModel.userProfileId.value!!,
            latLng = geopoint
        )
        advertisementViewModel.loadFromTemporaryAdv = true
        advertisementViewModel.currentItemMarker = null
        NavHostFragment.findNavController(nav_host_fragment)
            .navigate(R.id.action_itemEditFragment_to_nav_item_edit_map)
    }

    private fun loadAdvertisement() {
        advertisementViewModel.advertisement.observe(
            viewLifecycleOwner,
            Observer { retrievedData -> fillFromViewModel(retrievedData) })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.item_edit_fragment, menu);
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_save -> {
                if (!checkFieldBeforeSave()) {
                    return false
                }
                lateinit var advertisement: Advertisement
                if (advertisementViewModel.advertisementID == "") {
                    advertisement = Advertisement(
                        currentThumbnailPath
                        , textField_item_edit_title.editText?.text.toString()
                        , textField_item_edit_description.editText?.text.toString()
                        , textField_item_edit_price.editText?.text.toString().toDouble()
                        , textField_item_edit_category.editText?.text.toString()
                        , textField_item_edit_subCategory.editText?.text.toString()
                        , textField_item_edit_location.editText?.text.toString()
                        , textField_item_edit_expiry_date.editText?.text.toString()
                        , textField_item_edit_status.editText?.text.toString(),
                        "",
                        userViewModel.userProfileId.value!!
                    )
                } else {
                    toggleBuyerSection(false, false)
                    val buyerDocID = advertisementViewModel
                        .getInterestedDocIdFromMail(textField_item_edit_buyer.editText?.text.toString())
                    advertisement = Advertisement(
                        advertisementViewModel.advertisement.value!!.thumbnailPath
                        , textField_item_edit_title.editText?.text.toString()
                        , textField_item_edit_description.editText?.text.toString()
                        , textField_item_edit_price.editText?.text.toString().toDouble()
                        , textField_item_edit_category.editText?.text.toString()
                        , textField_item_edit_subCategory.editText?.text.toString()
                        , textField_item_edit_location.editText?.text.toString()
                        , textField_item_edit_expiry_date.editText?.text.toString()
                        , textField_item_edit_status.editText?.text.toString(),
                        buyerDocID,
                        userViewModel.userProfileId.value!!
                    )
                    advertisement.interestedUsers =
                        advertisementViewModel.advertisement.value!!.interestedUsers
                    if (advertisement.status != advertisementViewModel.advertisement.value!!.status)
                        triggerNotification(advertisement.status)
                }
                Log.d("KKK", "User ID before save: " + advertisement.owner)
                advertisementViewModel.saveAdvertisement(advertisement, true)
                advertisementViewModel.restoreBooleans()
                val bundle = Bundle()
                bundle.putString("result", "ok")
                NavHostFragment.findNavController(nav_host_fragment).navigate(
                    R.id.action_itemEditFragment_to_nav_advertisements,
                    bundle
                )
                true
            }
            R.id.action_block -> {
                if (advertisementViewModel.advertisementID == "")
                    return false
                if (advertisementViewModel.advertisement.value != null)
                    if (advertisementViewModel.advertisement.value?.status == getString(R.string.item_status_sold))
                        return false
                if (textField_item_edit_status.editText?.text.toString() == getString(R.string.item_status_blocked)) {
                    textField_item_edit_status.editText?.setText(getString(R.string.item_status_selling))
                    Snackbar.make(
                        itemEditLayout!!,
                        getString(R.string.item_status_selling),
                        Snackbar.LENGTH_LONG
                    ).show()
                    return true
                }
                textField_item_edit_status.editText?.setText(getString(R.string.item_status_blocked))
                toggleBuyerSection(false, false, true)
                Snackbar.make(
                    itemEditLayout!!,
                    getString(R.string.advertisement_blocked_alert),
                    Snackbar.LENGTH_LONG
                ).show()
                true
            }
            R.id.action_sold -> {
                Log.d("KKK", "ID: " + advertisementViewModel.advertisementID)
                if (advertisementViewModel.advertisementID == "")
                    return false
                if (advertisementViewModel.advertisement.value != null)
                    if (advertisementViewModel.advertisement.value?.status == getString(R.string.item_status_sold))
                        return false
                textField_item_edit_status.editText?.setText(getString(R.string.item_status_sold))
                toggleBuyerSection(true, true)
                Snackbar.make(
                    itemEditLayout!!,
                    getString(R.string.item_sold_alert),
                    Snackbar.LENGTH_SHORT
                )
                    .setAction(getString(R.string.action_undo), View.OnClickListener {
                        if (textField_item_edit_status != null)
                            textField_item_edit_status.editText?.setText(getString(R.string.item_status_selling))
                        toggleBuyerSection(false, false, true)
                    }).show()
                true
            }
            else -> {
                false
            }
        }
    }

    private fun checkFieldBeforeSave(): Boolean {
        var result = true
        if (advertisementViewModel.advertisementID == "") {
            if (currentThumbnailPath == "") {
                Snackbar.make(
                    itemEditLayout!!,
                    resources.getString(R.string.error_empty_field) + " " + item_image.contentDescription,
                    Snackbar.LENGTH_LONG
                ).show()
                result = false
            }
        }
        if (textField_item_edit_title.editText?.text.isNullOrEmpty()) {
            textField_item_edit_title.error =
                resources.getString(R.string.error_empty_field) + " " + textField_item_edit_title.hint
            result = false
        }
        if (textField_item_edit_description.editText?.text.isNullOrEmpty()) {
            textField_item_edit_description.error =
                resources.getString(R.string.error_empty_field) + " " + textField_item_edit_description.hint
            result = false
        }
        if (textField_item_edit_price.editText?.text.isNullOrEmpty()) {
            textField_item_edit_price.error =
                resources.getString(R.string.error_empty_field) + " " + textField_item_edit_price.hint
            result = false
        }
        if (textField_item_edit_category.editText?.text.isNullOrEmpty()) {
            textField_item_edit_category.error =
                resources.getString(R.string.error_empty_field) + " " + textField_item_edit_category.hint
            result = false
        }
        if (textField_item_edit_subCategory.editText?.text.isNullOrEmpty()) {
            textField_item_edit_subCategory.error =
                resources.getString(R.string.error_empty_field) + " " + textField_item_edit_subCategory.hint
            result = false
        }
        if (textField_item_edit_location.editText?.text.isNullOrEmpty()) {
            textField_item_edit_location.error =
                resources.getString(R.string.error_empty_field) + " " + textField_item_edit_location.hint
            result = false
        }
        if (textField_item_edit_expiry_date.editText?.text.isNullOrEmpty()) {
            textField_item_edit_expiry_date.error =
                resources.getString(R.string.error_empty_field) + " " + textField_item_edit_expiry_date.hint
            result = false
        }
        if (textField_item_edit_status.editText?.text.toString() == getString(R.string.item_status_sold))
            if (textField_item_edit_buyer.editText?.text.isNullOrEmpty()) {
                textField_item_edit_buyer.error =
                    resources.getString(R.string.error_empty_field) + " " + textField_item_edit_buyer.hint
                result = false
            }
        return result
    }

    /**
     * Fill fields from viewModel data
     */
    private fun fillFromViewModel(data: Advertisement?) {
        textField_item_edit_status.editText?.setText(getString(R.string.item_status_selling))
        if (data != null) {
            Picasso.get().load(data.thumbnailPath).into(item_image)
            item_image.tag = data.thumbnailPath
            textField_item_edit_title.editText?.setText(data.title)
            textField_item_edit_description.editText?.setText(data.description)
            if (data.price.isNaN())
                textField_item_edit_price.editText?.setText("")
            else
                textField_item_edit_price.editText?.setText(data.price.toString())
            textField_item_edit_category.editText?.setText(data.category)
            textField_item_edit_subCategory.editText?.setText(data.subCategory)
            setSubcategoryDropdown(data.category)
            textField_item_edit_location.editText?.setText(data.location)
            textField_item_edit_expiry_date.editText?.setText(data.expiryDate)
            if (data.status != "")
                textField_item_edit_status.editText?.setText(data.status)
            if (data.status == getString(R.string.item_status_sold)) {
                toggleBuyerSection(false, false)
            } else
                toggleBuyerSection(false, false, true)
        }
    }

    /**
     * Setup the buyer spinner with interested users options
     */
    private fun setupDropDownBuyer(emailData: Map<String, UserProfile>? = null) {
        val emails: Array<String>
        if (emailData == null) {
            // Get data from viewModel
            emails = arrayOf()
            Log.d("KKK", "null ---" + emails.toString())
        } else {
            // use emailData
            emails = emailData.values.stream().map { it -> it.email }.collect(Collectors.toList())
                .toTypedArray()
            Log.d("KKK", "NOT null --- " + emails.toString())
        }
        // Set emails in spinner
        val emailsAdapter = context?.let { ArrayAdapter(it, R.layout.list_item, emails) }
        (textField_item_edit_buyer.editText as AutoCompleteTextView).setAdapter(emailsAdapter)
    }

    /**
     * Setup the date picker for expiry date
     */
    private fun setupDatePicker() {
        val calendar = Calendar.getInstance()
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)

        // date picker dialog
        val dialog = DatePickerDialog(
            requireContext(),
            DatePickerDialog.OnDateSetListener { view, dYear, dMonth, dayOfMonth ->
                val realMonth = dMonth + 1
                var stringDay: String = dayOfMonth.toString()
                var stringMonth: String = realMonth.toString()
                if (dayOfMonth < 10)
                    stringDay = "0$stringDay"
                if (realMonth < 10)
                    stringMonth = "0$stringMonth"
                textField_item_edit_expiry_date.editText?.setText("" + stringDay + "/" + stringMonth + "/" + dYear)
            }, year, month, day
        )
        dialog.datePicker.minDate = System.currentTimeMillis() - 1000;
        dialog.show()
    }

    /**
     * This function setup the dropdown menus for categories and sub cateogries
     * also setup onclick listener for categories
     */
    private fun setupDropdownMenusCategories() {
        val categories = resources.getStringArray(R.array.categories)
        val adapter = context?.let { ArrayAdapter(it, R.layout.list_item, categories) }
        (textField_item_edit_category.editText as AutoCompleteTextView).setAdapter(adapter)
        (textField_item_edit_category.editText as AutoCompleteTextView).addTextChangedListener(
            object :
                TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    textField_item_edit_subCategory.isEnabled = true
                    (textField_item_edit_subCategory.editText as AutoCompleteTextView).setText("")
                    setSubcategoryDropdown(s.toString())
                }
            })
    }

    /**
     * Setup the dropdown menu for sub categories given a category
     */
    private fun setSubcategoryDropdown(selectedCategory: String?) {
        var subCatToUpdate: Array<String> = emptyArray()

        when (selectedCategory) {
            resources.getString(R.string.cat1) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat1)
            resources.getString(R.string.cat2) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat2)
            resources.getString(R.string.cat3) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat3)
            resources.getString(R.string.cat4) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat4)
            resources.getString(R.string.cat5) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat5)
            resources.getString(R.string.cat6) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat6)
            resources.getString(R.string.cat7) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat7)
            resources.getString(R.string.cat8) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat8)
            else -> textField_item_edit_subCategory.isEnabled = false
        }
        val subCatAdapter =
            context?.let { ArrayAdapter(it, R.layout.list_item, subCatToUpdate) }
        (textField_item_edit_subCategory.editText as AutoCompleteTextView).setAdapter(
            subCatAdapter
        )
    }

    private fun fillFromTemporaryData() {
        if (!advertisementViewModel.photoModified && advertisementViewModel.advertisementID != "") {
            Picasso.get().load(advertisementViewModel.advertisement.value?.thumbnailPath)
                .into(item_image)
            item_image.tag = advertisementViewModel.advertisement.value?.thumbnailPath
        } else {
            val path = advertisementViewModel.temporaryAdvData.thumbnailPath
            if (path == "")
                item_image.setImageResource(R.drawable.ic_gallery)
            else
                item_image.setImageURI(Uri.parse(path))
            item_image.tag = path
            currentThumbnailPath = path ?: ""
        }
        textField_item_edit_title.editText?.setText(advertisementViewModel.temporaryAdvData.title)
        textField_item_edit_description.editText?.setText(advertisementViewModel.temporaryAdvData.description)
        if (advertisementViewModel.temporaryAdvData.price.isNaN())
            textField_item_edit_price.editText?.setText("")
        else
            textField_item_edit_price.editText?.setText(advertisementViewModel.temporaryAdvData.price.toString())
        textField_item_edit_category.editText?.setText(advertisementViewModel.temporaryAdvData.category)
        textField_item_edit_subCategory.editText?.setText(advertisementViewModel.temporaryAdvData.subCategory)
        setupDropdownMenusCategories()
        setSubcategoryDropdown(advertisementViewModel.temporaryAdvData.category)
        if (advertisementViewModel.currentItemMarker != null)
            textField_item_edit_location.editText?.setText(advertisementViewModel.currentItemMarker!!.title)
        else
            textField_item_edit_location.editText?.setText(advertisementViewModel.temporaryAdvData.location)
        textField_item_edit_expiry_date.editText?.setText(advertisementViewModel.temporaryAdvData.expiryDate)
        advertisementViewModel.loadFromTemporaryAdv = false
        advertisementViewModel.temporaryAdvData = Advertisement()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        advertisementViewModel.savedInstanceState = true
        Log.d("KKK", "onSaveInstanceState: " + item_image?.tag?.toString())
        outState.putString("image", item_image?.tag?.toString() ?: "")
        outState.putString("title", textField_item_edit_title?.editText?.text.toString())
        outState.putString(
            "description",
            textField_item_edit_description?.editText?.text.toString()
        )
        outState.putString("price", textField_item_edit_price?.editText?.text.toString())
        outState.putString("category", textField_item_edit_category?.editText?.text.toString())
        outState.putString(
            "subCategory",
            textField_item_edit_subCategory?.editText?.text.toString()
        )
        outState.putString("location", textField_item_edit_location?.editText?.text.toString())
        outState.putString("expiryDate", textField_item_edit_expiry_date?.editText?.text.toString())
        outState.putString("status", textField_item_edit_status?.editText?.text.toString())
        outState.putString("buyer", textField_item_edit_buyer?.editText?.text.toString())
        super.onSaveInstanceState(outState)
    }


    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null && !advertisementViewModel.loadFromTemporaryAdv) {
            if (!advertisementViewModel.photoModified) {
                if (advertisementViewModel.advertisement.value != null)
                    Picasso.get().load(advertisementViewModel.advertisement.value?.thumbnailPath)
                        .into(item_image)
                else
                    item_image.setImageResource(R.drawable.ic_gallery)
                item_image.tag = advertisementViewModel.advertisement.value?.thumbnailPath
            } else {
                val path = savedInstanceState.getString("image")
                if (path == "")
                    item_image.setImageResource(R.drawable.ic_gallery)
                else
                    item_image.setImageURI(Uri.parse(path))
                item_image.tag = path
                currentThumbnailPath = path ?: ""
            }
            textField_item_edit_title.editText?.setText(savedInstanceState.getString("title"))
            textField_item_edit_description.editText?.setText(savedInstanceState.getString("description"))
            textField_item_edit_price.editText?.setText(savedInstanceState.getString("price"))
            textField_item_edit_category.editText?.setText(savedInstanceState.getString("category"))
            textField_item_edit_subCategory.editText?.setText(savedInstanceState.getString("subCategory"))
            setupDropdownMenusCategories()
            setSubcategoryDropdown(savedInstanceState.getString("category"))
            textField_item_edit_location.editText?.setText(savedInstanceState.getString("location"))
            textField_item_edit_expiry_date.editText?.setText(savedInstanceState.getString("expiryDate"))
            textField_item_edit_status.editText?.setText(savedInstanceState.getString("status"))
            textField_item_edit_buyer.editText?.setText(savedInstanceState.getString("buyer"))
            if (textField_item_edit_status.editText?.text.toString() == getString(R.string.item_status_sold)) {
                Log.d("KKK", "VISIBLE")
                toggleBuyerSection(true, true)
            }
        } else if (advertisementViewModel.loadFromTemporaryAdv) {
            fillFromTemporaryData()
        }
    }


    /**
     * Callback function after camera/gallery/crop intents
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //super.onActivityResult(requestCode, resultCode, data)
        if (!this::cameraGallery.isInitialized) {
            Log.d("kkk", "restore cameragallery")
            cameraGallery = CameraGallery(edit_item_image, requireContext(), this, false)
            val sharedP = this.activity?.getSharedPreferences(
                getString(R.string.preference_file_key),
                Context.MODE_PRIVATE
            )
            cameraGallery.photoPath = sharedP?.getString("persistentCurrentPhotoPath", "") ?: ""
            cameraGallery.imgUri = Uri.parse(sharedP?.getString("persistentCurrentUri", "") ?: "")
            Log.d("kkk", "restored cameragallery photopath" + cameraGallery.photoPath)
        }
        when (requestCode) {
            cameraGallery.REQUEST_IMAGE_CAPTURE -> {
                if (resultCode == AppCompatActivity.RESULT_OK) {
                    if (cameraGallery.imgUri == null) {
                        Log.d("", "URI is null")
                        return
                    }
                    cameraGallery.handleRotation()
                    cameraGallery.launchImageCrop()
                } else {
                    Toast.makeText(context, "Error while taking the photo!", Toast.LENGTH_SHORT)
                        .show();
                }
            }
            cameraGallery.REQUEST_IMAGE_GALLERY -> {
                if (resultCode == AppCompatActivity.RESULT_OK && data != null) {
                    val ins: InputStream? =
                        this
                            .activity
                            ?.contentResolver
                            ?.openInputStream(data.data!!)             /* loading the bitmap */
                    var img: Bitmap? = BitmapFactory.decodeStream(ins)
                    cameraGallery.bitmapToFile(img!!)                        /* store the image in application directory */
                    cameraGallery.launchImageCrop()
                } else {
                    Toast.makeText(context, "Error while taking the photo!", Toast.LENGTH_SHORT)
                        .show();
                }
            }
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                if (resultCode == AppCompatActivity.RESULT_OK && data != null) {
                    val result = CropImage.getActivityResult(data)
                    val resUri = result.uri
                    cameraGallery.createThumbnail()
                    item_image.setImageURI(resUri)
                    currentThumbnailPath = cameraGallery.thumbnailPath
                    item_image.tag = currentThumbnailPath
                    advertisementViewModel.photoModified = true
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Toast.makeText(context, "Error while cropping the photo!", Toast.LENGTH_SHORT)
                        .show();
                }
            }
            else -> Toast.makeText(context, "ERROR! YOU SHOULDN'T SEE THIS!!!", Toast.LENGTH_SHORT)
                .show();
        }
    }

    /**
     * Hide soft keyboard when a view is clicked (spinner)
     */
    private fun hideSoftKeyboardOnViewClick(view: View) {
        view.setOnClickListener() {
            val imm: InputMethodManager =
                requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            val viewToken = this.view?.rootView?.windowToken
            imm.hideSoftInputFromWindow(viewToken, 0)
        }
    }

    /**
     * Toggle enable and visibility of the buyer spinner
     */
    private fun toggleBuyerSection(active: Boolean, visible: Boolean, clear: Boolean = false) {
        textField_item_edit_buyer.isEnabled = active
        if (visible)
            textField_item_edit_buyer.visibility = View.VISIBLE
        else {
            textField_item_edit_buyer.visibility = View.GONE
            if (clear)
                textField_item_edit_buyer.editText?.setText("")
        }
    }

    private fun triggerNotification(status: String) {
        val topic =
            "/topics/" + advertisementViewModel.advertisementID + "_status" //topic has to match what the receiver subscribed to

        val notification = JSONObject()
        val notifcationBody = JSONObject()

        try {
            notifcationBody.put(
                "title",
                "Object status changed: " + advertisementViewModel.advertisement.value!!.title
            )
            notifcationBody.put("message", status)   //Enter your notification message
            notification.put("to", topic)
            notification.put("data", notifcationBody)
        } catch (e: JSONException) {
            Log.e("TAG", "notification error: " + e.message)
        }
        sendNotification(notification)
    }

    private fun sendNotification(notification: JSONObject) {
        val jsonObjectRequest = object : JsonObjectRequest(
            FCM_API, notification,
            Response.Listener { response ->
                Log.d("NOTIF", "Notification sent")
            },
            Response.ErrorListener {
                Log.d("NOTIF", "Notification ERROR")
            }) {

            override fun getHeaders(): Map<String, String> {
                val params = HashMap<String, String>()
                params["Authorization"] =
                    serverKey
                params["Content-Type"] =
                    contentType
                return params
            }
        }
        requestQueue.add(jsonObjectRequest)
    }
}