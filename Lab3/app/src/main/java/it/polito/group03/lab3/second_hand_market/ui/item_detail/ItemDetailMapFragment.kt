package it.polito.group03.lab3.second_hand_market.ui.item_detail

import android.Manifest
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.content.res.Resources
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.math.MathUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import com.google.firebase.firestore.GeoPoint
import it.polito.group03.lab3.second_hand_market.MainActivity
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.ui.user_profile.UserViewModel
import kotlinx.android.synthetic.main.fragment_item_location.*
import java.io.IOException
import java.text.DecimalFormat
import java.util.*
import java.util.EnumSet.of
import kotlin.math.*


class ItemDetailMapFragment : Fragment() {
    private lateinit var mMap: GoogleMap
    private lateinit var vm : ItemDetailViewModel
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location

    private var userGeopoint: GeoPoint? = null
    private val REQUEST_LOCATION_PERMISSION = 1
    private val REQUEST_CHECK_SETTINGS = 2
    private var line: Marker? = null
    private var itemMarker: Marker? = null
    private var userMarker : Marker? = null
    private val zoom = 15f
    private val TAG = ItemDetailMapFragment::class.java.simpleName
    private lateinit var locationCallback : LocationCallback
    private lateinit var locationRequest : LocationRequest
    private var locationUpdateState = false
    private var polyline:Polyline? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = activity?.let{ ViewModelProvider(it).get(ItemDetailViewModel::class.java) }!!

        //setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_item_location, container, false)
    }

    override fun onResume() {
        super.onResume()
        itemMapView.onResume()
        //if(!locationUpdateState)
            //startLocationUpdates()
    }

    override fun onPause() {
        super.onPause()
        itemMapView.onPause()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        itemMapView.onLowMemory()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        itemMapView.onCreate(savedInstanceState)
        itemMapView.onResume()
        try{
            MapsInitializer.initialize(requireContext())
        } catch (e:Exception){
            e.printStackTrace()
        }
        userGeopoint = activity?.let{ ViewModelProvider(it).get(UserViewModel::class.java) }!!.userLiveData.value?.latLng

        itemMapView.getMapAsync {
            mMap = it

            val turin = LatLng(45.0578564352, 7.65664237342)
            if(vm.advertisement.value?.latLng != null){

                val pos : LatLng? = vm.advertisement.value!!.latLng?.latitude?.let { it1 ->
                    vm.advertisement.value!!.latLng?.longitude?.let { it2 ->
                        LatLng(
                            it1, it2
                        )
                    }
                }
                if(pos != null){
                    itemMarker = placeMarkerOnMap(pos,true)
                    if(userGeopoint!=null){
                        userMarker = placeMarkerOnMap(LatLng(userGeopoint!!.latitude,userGeopoint!!.longitude),false)
                    }
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos,zoom))
                    item_location_map.text = getAddress(pos)
                } else {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(turin,10f))
                }

            } else {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(turin,10f))
            }
            /* zoom levels
                    1: World
                    5: Landmass/continent
                    10: City
                    15: Streets
                    20: Buildings
            */

            enableMyLocation()
            setMarkerListener()
            setPOIClick(mMap)
            if(userMarker != null && itemMarker != null){
                polyline = drawLine()
            }
            //setMapStyle(mMap)

        }
        fusedLocationClient = LocationServices
            .getFusedLocationProviderClient(requireContext())
        locationCallback = object : LocationCallback(){
            override fun onLocationResult(locationResult: LocationResult?){
                locationResult ?: return

                val latLng = LatLng(locationResult.lastLocation.latitude,
                    locationResult.lastLocation.longitude)
                if(userMarker != null){
                    if(userMarker!!.position.latitude == latLng.latitude &&
                            userMarker!!.position.longitude == latLng.longitude)
                        return
                }

                polyline?.remove()
                polyline = drawLine()
                userMarker?.remove()
                userMarker = placeMarkerOnMap(
                    latLng,
                    false
                )
            }
        }
        createLocationRequest()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == REQUEST_LOCATION_PERMISSION){
            if(grantResults.contains(PackageManager.PERMISSION_GRANTED)){
                enableMyLocation()
            }
        }
    }

    private fun lineInterpolate(p1 :LatLng, p2:LatLng, d:Double) : LatLng{
        val xabs = abs(p1.latitude - p2.latitude)
        val yabs = abs(p1.longitude - p2.longitude)
        val xdiff = p2.latitude - p1.latitude
        val ydiff = p2.longitude - p1.longitude
        val length = sqrt(xabs.pow(2.0) + yabs.pow(2.0))
        val steps = length/d
        val xSteps = xdiff/steps
        val ySteps = ydiff/steps
        return LatLng(p1.latitude+xSteps,p1.longitude+ySteps)
    }

    private fun drawLine() : Polyline? {
        if(userMarker != null && itemMarker != null) {
            val ret = mMap.addPolyline(
                PolylineOptions()
                    .add(userMarker!!.position,
                        itemMarker!!.position)
                    .clickable(true)
                    .color(resources.getColor(R.color.colorPrimaryDark, activity?.theme))
            )

            val distance = sqrt(
                    (itemMarker!!.position.latitude - userMarker!!.position.latitude)*(itemMarker!!.position.latitude - userMarker!!.position.latitude) +
                    (itemMarker!!.position.longitude - userMarker!!.position.longitude)*(itemMarker!!.position.longitude - userMarker!!.position.longitude)
            )
            val midPoint = lineInterpolate(itemMarker!!.position,userMarker!!.position,distance/2)

            var title = ""
            var meters = getDistance()
            if (meters != null) {
                if(meters >= 1000f){
                    meters /= 1000
                    title = "The item is "+meters.format(3)+" km far"
                } else {
                    title = "The item is "+meters.format(3)+" m far"
                }
            }
            val transparent = BitmapDescriptorFactory.fromResource(R.drawable.transparent)
            line = mMap.addMarker(MarkerOptions()
                .position(midPoint)
                .title(title)
                .icon(transparent)
                .anchor(0.5F, 0.5F))
            setOnPolylineClick()
            return ret
        }
        return null
    }

    private fun Float.format(digits:Int) = "%.${digits}f".format(this)

    private fun setOnPolylineClick() {
        mMap.setOnPolylineClickListener {
            line?.showInfoWindow()
        }
    }

    private fun isPermissionGranted() : Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    private fun getDistance(): Float? {
        if(itemMarker== null || userMarker == null)
            return null
        val locI = Location("")
        locI.latitude = itemMarker!!.position.latitude
        locI.longitude = itemMarker!!.position.longitude
        val locU = Location("")
        locU.latitude = userMarker!!.position.latitude
        locU.longitude = userMarker!!.position.longitude
        Log.d(TAG,locI.toString() + "----------" + locU.toString())
        return locU.distanceTo(locI)
    }

    private fun placeMarkerOnMap(latLng: LatLng,isItem: Boolean) : Marker{
        val snippet = String.format(
            Locale.getDefault(),
            "Lat: %1$.5f, Long: %2$.5f",
            latLng.latitude,
            latLng.longitude
        )

        val markerOptions = MarkerOptions()
            .position(latLng)
            .title(getAddress(latLng))
        if(isItem) {
            val bm = vm.drawableConverter
                ?.getBitmap(R.drawable.ic_baseline_shopping_basket_24)
            markerOptions
                    .snippet("ITEM, " + snippet)
            if(bm != null)
                markerOptions
                    .icon(
                        BitmapDescriptorFactory
                            .fromBitmap(bm)
                    )
            else
                markerOptions
                    .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
        }else {
            val bm = vm.drawableConverter
                ?.getBitmap(R.drawable.ic_baseline_person_pin_24)
            markerOptions
                .snippet("YOU, " + snippet)
            if(bm != null)
                markerOptions
                    .icon(
                        BitmapDescriptorFactory
                            .fromBitmap(bm)
                    )
            else
                markerOptions
                    .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
        }
        return mMap.addMarker( markerOptions )
    }

    private fun getAddress(latLng: LatLng):String{
        try {
            val geocoder = Geocoder(requireContext())
            val addresses: List<Address>
            val address: Address?
            var addressText = ""

            try {
                addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
                if (addresses != null && addresses.isNotEmpty()) {
                    address = addresses[0]

                    var i = 0
                    while (i <= address.maxAddressLineIndex) {
                        addressText += if (i == 0) address.getAddressLine(i) else "\n" + address.getAddressLine(
                            i
                        )
                        i++
                    }
                }
            } catch (e: IOException) {
                Log.e(TAG, "Error getAddress :" + e.localizedMessage)
            }
            return addressText
        } catch(e : java.lang.Exception){
            return ""
        }
    }

    private fun startLocationUpdates() {

        if (isPermissionGranted()) {
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                //Looper.getMainLooper()
                null
            )

        }
    }

    private fun createLocationRequest(){
        locationRequest = LocationRequest()
        locationRequest.interval = 2500 // 2.5s
        locationRequest.fastestInterval = 1250 //1.25s
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val client = LocationServices.getSettingsClient(requireContext())
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener {
            locationUpdateState = true
            startLocationUpdates()
        }
        task.addOnFailureListener { e ->
            if(e is ResolvableApiException){
                try{
                    e.startResolutionForResult(this.activity,REQUEST_CHECK_SETTINGS)
                }catch (sendEx : IntentSender.SendIntentException){

                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_CHECK_SETTINGS){
            if(resultCode == Activity.RESULT_OK){
                locationUpdateState = true
                startLocationUpdates()
            }
        }
    }

    private fun enableMyLocation() {
        if(isPermissionGranted()){
            mMap.isMyLocationEnabled = true
            mMap.uiSettings.isMyLocationButtonEnabled=true
            fusedLocationClient.lastLocation.addOnSuccessListener { loc ->
                if(loc != null){
                    lastLocation = loc
                    val currentLatLng = LatLng(loc.latitude,loc.longitude)
                    userMarker?.remove()
                    userMarker = placeMarkerOnMap(currentLatLng,false)

                }
            }

            startLocationUpdates()
        } else {
            activity?.let {
                ActivityCompat.requestPermissions(
                    it,
                    arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_LOCATION_PERMISSION
                )
            }
        }
    }

    private fun setMapLongClick(map:GoogleMap){
        map.setOnMapLongClickListener { latlng ->
            placeMarkerOnMap(latlng,true)
        }
    }

    private fun setPOIClick(map:GoogleMap){
        map.setOnPoiClickListener{
            val poiMarker = map.addMarker(
                MarkerOptions()
                    .position(it.latLng)
                    .title(it.name)
            )
            poiMarker.showInfoWindow()
        }
    }

    private fun setMarkerListener(){
        mMap.setOnMarkerClickListener {
            Log.d(TAG,"-->"+it.title)
            if(itemMarker == null && userMarker == null) {
                it.remove()
                true
            } else {
                var isItem = false
                var isUser = false
                if ( itemMarker != null ) {
                     if( it.position.latitude == itemMarker!!.position.latitude &&
                            it.position.longitude == itemMarker!!.position.longitude)
                         isItem = true
                }
                if(isItem)
                    return@setOnMarkerClickListener false
                if( userMarker != null){
                    if(it.position.latitude == userMarker!!.position.latitude &&
                            it.position.longitude == userMarker!!.position.longitude)
                        isUser = true
                }
                if(isUser)
                    return@setOnMarkerClickListener false
                it.remove()
                true
            }
        }
    }

    private fun setMapStyle(map:GoogleMap){
        try{
            val success = map.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    requireContext(),
                    R.raw.map_style
                )
            )
            if(!success)
                Log.e(TAG,"Style parsing failed")
        } catch (e: Resources.NotFoundException){
            Log.e(TAG,"Can't find style, error : ",e)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //super.onOptionsItemSelected(item)
        return when(item.itemId){
            R.id.normal_map -> {
                mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
                true
            }
            R.id.hybrid_map -> {
                mMap.mapType = GoogleMap.MAP_TYPE_HYBRID
                true
            }
            R.id.satellite_map -> {
                mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
                true
            }
            R.id.terrain_map -> {
                mMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }
}