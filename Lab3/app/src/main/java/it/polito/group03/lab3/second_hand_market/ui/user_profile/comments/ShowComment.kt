package it.polito.group03.lab3.second_hand_market.ui.user_profile.comments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.model.Comment
import it.polito.group03.lab3.second_hand_market.ui.user_profile.UserViewModel
import kotlinx.android.synthetic.main.comment_item.*
import java.lang.Exception


class ShowComment : BottomSheetDialogFragment()  {
    private val userProfile: UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.comment_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var comment = Comment()
        if (arguments != null) {
            comment = requireArguments().getParcelable<Comment>("comment")!!
            title.text = comment.title
            body.text = comment.body
            ratingBar.rating = comment.numStars
            val commentDate = comment.date.split("_")[0]
            date.text = commentDate.split("/")[2] + "/" + commentDate.split("/")[1] + "/" +commentDate.split("/")[0]
            if (comment.title == "")
                title.visibility = View.GONE
            if (comment.body == "")
                body.visibility = View.GONE
            else {
                body.maxLines = 256
                body.isSingleLine = false
            }
        }
        observeData(comment)
    }

    fun observeData(comment: Comment) {
        if (shimmer_view_container != null)
            shimmer_view_container.startShimmer()
        userProfile.getUser(comment.userWC).observe(viewLifecycleOwner, Observer {
            if (it.avatarPath == "") {
                avatar.setImageResource(R.drawable.ic_user)
                shimmer_view_container.stopShimmer()
                if (shimmer_view_container != null) {
                    shimmer_view_container.stopShimmer()
                    shimmer_view_container.visibility = View.GONE
                }
            }else {
                Picasso.get().load(it.avatarPath).into(avatar, object : Callback {
                    override fun onSuccess() {
                        if (shimmer_view_container != null) {
                            shimmer_view_container.stopShimmer()
                            shimmer_view_container.visibility = View.GONE
                        }
                    }

                    override fun onError(e: Exception?) {
                        if (shimmer_view_container != null) {
                            shimmer_view_container.stopShimmer()
                        }
                    }

                })
            }
            username.text = it.nickname
            email.text = it.email
        })

    }
}