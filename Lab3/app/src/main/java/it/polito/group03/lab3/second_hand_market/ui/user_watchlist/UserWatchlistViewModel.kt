package it.polito.group03.lab3.second_hand_market.ui.user_watchlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import it.polito.group03.lab3.second_hand_market.model.Advertisement
import it.polito.group03.lab3.second_hand_market.model.Repository.AdvertisementRepository

class UserWatchlistViewModel : ViewModel() {
    var myUserId = ""
    lateinit var advs: LiveData<MutableMap<String, Advertisement>>
    val repository =
        AdvertisementRepository()

    fun fetchAdvertisements(): LiveData<MutableMap<String, Advertisement>> {
        var advs = MutableLiveData<MutableMap<String, Advertisement>>()
        repository.getWatchlistAdvertisements(myUserId).observeForever { advsList ->
            advs.value = advsList
        }
        this.advs = advs
        return advs
    }

    override fun onCleared() {
        super.onCleared()
        repository.unSubscribe()
    }
}