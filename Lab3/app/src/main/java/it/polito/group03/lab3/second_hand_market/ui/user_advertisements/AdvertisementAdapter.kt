package it.polito.group03.lab3.second_hand_market.ui.user_advertisements

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.model.Advertisement
import java.lang.Exception

class AdvertisementAdapter(
    var navController: NavController
) : RecyclerView.Adapter<AdvertisementAdapter.ViewHolder>() {
    var advertisements: MutableMap<String, Advertisement> = mutableMapOf<String, Advertisement>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    class ViewHolder(v: View, var navController: NavController) : RecyclerView.ViewHolder(v) {
        val cardView: CardView = v as CardView
        val title: TextView = v.findViewById(R.id.title)
        val price: TextView = v.findViewById(R.id.price)
        val description: TextView = v.findViewById(R.id.description)
        val subCategory: TextView = v.findViewById(R.id.subCategory)
        val item_image: ImageView = v.findViewById(R.id.item_image)
        val edit_button: Button = v.findViewById(R.id.edit_button)
        val interested_users_button: Button = v.findViewById(R.id.interestedUsersButton)
        val shimmer_view_container_interested =
            v.findViewById<ShimmerFrameLayout>(R.id.shimmer_view_container_interested)

        @RequiresApi(Build.VERSION_CODES.O)
        @SuppressLint("ResourceAsColor")
        fun bind(adv: Pair<String, Advertisement>) {
            val id = adv.first
            val advertisement = adv.second
            title.text = advertisement.title
            price.text = "" + advertisement.price + "€"
            description.text = advertisement.description
            subCategory.text = advertisement.subCategory
            if (shimmer_view_container_interested != null)
                shimmer_view_container_interested.startShimmer()
            Picasso.get().load(advertisement.thumbnailPath).into(item_image, object : Callback {
                override fun onSuccess() {
                    if (shimmer_view_container_interested != null) {
                        shimmer_view_container_interested.stopShimmer()
                        shimmer_view_container_interested.visibility = View.GONE
                    }
                }

                override fun onError(e: Exception?) {
                    if (shimmer_view_container_interested != null) {
                        shimmer_view_container_interested.stopShimmer()
                    }
                }

            })
            var i = advertisement.interestedUsers.size
            interested_users_button.text = i.toString() + " Interested Users"
            if (i == 0) {
                interested_users_button.isClickable = false
                interested_users_button.setTextColor(
                    ContextCompat.getColor(
                        interested_users_button.context,
                        R.color.secondary_text
                    )
                )
            } else {
                interested_users_button.setTextColor(
                    ContextCompat.getColor(
                        interested_users_button.context,
                        R.color.colorAccent
                    )
                )
                interested_users_button.isClickable = true
                interested_users_button.setOnClickListener {
                    val bundle = Bundle()
                    bundle.putString("advertisementID", id)
                    bundle.putParcelable("item", advertisement)
                    navController.navigate(
                        R.id.action_nav_item_list_to_interestedUsersFragment,
                        bundle
                    )
                }
            }
            if (advertisement.status == "Sold") {
                edit_button.isClickable = false
                edit_button.setTextColor(
                    ContextCompat.getColor(
                        interested_users_button.context,
                        R.color.secondary_text
                    )
                )
            } else {
                edit_button.isClickable=true
                edit_button.setOnClickListener {
                    val bundle = Bundle()
                    bundle.putString("advertisementID", id)
                    bundle.putBoolean("fromEditButton", true)
                    this.navController.navigate(
                        R.id.action_nav_slideshow_to_itemEditFragment,
                        bundle
                    )
                }
            }

            cardView.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("advertisementID", id)
                bundle.putString("advertisementOwner", advertisement.owner)
                navController.navigate(R.id.action_nav_advertisements_to_itemDetailFragment, bundle)
            }

        }

        fun unbind() {
            cardView.setOnClickListener(null)
            edit_button.setOnClickListener(null)
            interested_users_button.setOnClickListener(null)
        }
    }

    /**
     * it inflates the layout and returnes the ViewHolder object.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.advertisement_item, parent, false)
        return ViewHolder(v, this.navController)
    }

    /**
     * Retriving the size of list.
     */
    override fun getItemCount(): Int = advertisements.size

    /**
     * It permittes to bind the ViewHolder object with an specific object in the advertisement list.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(advertisements.toList()[position])
    }

    /**
     * This method is called when an item is recycled, we can perform an unbind method of ViewHolder.
     */
    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        holder.unbind()
    }
}