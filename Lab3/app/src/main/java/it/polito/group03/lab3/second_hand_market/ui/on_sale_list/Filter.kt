package it.polito.group03.lab3.second_hand_market.ui.on_sale_list

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Filter (
    val mainCategory: String,
    val subCategory: String,
    val expiryDate: String,
    val fromPrice: Double,
    val toPrice: Double,
    val location: String,
    val isAsc: Boolean,
    val isDesc: Boolean
): Parcelable {
    fun isEmpty(): Boolean {
        return this.mainCategory == ""
                && this.subCategory == ""
                && this.expiryDate == ""
                && this.fromPrice == 0.0
                && this.toPrice == Double.MAX_VALUE
                && this.location == ""
                && !this.isAsc
                && !this.isDesc
    }
}
