package it.polito.group03.lab3.second_hand_market.ui.user_profile

import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import it.polito.group03.lab3.second_hand_market.model.Repository.UserRepository
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.Marker
import com.google.firebase.firestore.GeoPoint
import it.polito.group03.lab3.second_hand_market.model.Comment
import it.polito.group03.lab3.second_hand_market.model.UserProfile
import it.polito.group03.lab3.second_hand_market.utils.AssetToBitmapConverter
import it.polito.group03.lab3.second_hand_market.utils.InvalidUserProfileUpdateException
import kotlinx.coroutines.launch

class UserViewModel : ViewModel() {
    var commented: Boolean = false
    var advertisementID: String = ""
    var soldTo: Boolean = false
    val db =
        UserRepository()
    private var userEmail = ""
    val userProfileId = db.loggedId
    val loginOk = db.loginResult
    var idOtherProfile = ""
    var drawableConverter : AssetToBitmapConverter? = null
    var currentUserMarker: Marker? = null
    var temporaryUserData = UserProfile()
    var loadFromTemporaryData = false
    lateinit var comments: MutableLiveData<MutableList<Comment>>
    var resultAddComment = MutableLiveData<Boolean>()
    var newComment = false
    var commentUsers = MutableLiveData<MutableMap<String, UserProfile>>()
    var commentUser = MutableLiveData<UserProfile>()
    val otherUserProfile = Transformations.switchMap(db.otherProfile){ value : UserProfile ->
        val toReturn = MutableLiveData<UserProfile>()
        val usr = UserProfile(value.avatarPath,value.fullname,value.nickname,value.email,value.location, value.rating, value.latLng)
        toReturn.value = usr
        userEmail = value.email
        //idOtherProfile = db.idOtherProfile.value.toString()
        return@switchMap toReturn
    }
    var isMyProfile = true
    var isRegistration : Boolean = false
    val userLiveData = Transformations.switchMap(db.profile){ value : UserProfile ->
        val toReturn = MutableLiveData<UserProfile>()
        toReturn.value = value
        userEmail = value.email
        return@switchMap toReturn
    }
    var profilePicHasChanged : Boolean = false
    var profileHasChanged : Boolean = false

    fun setupAssetConverter(context: Context, size: Int){
        //if(drawableConverter == null)
        drawableConverter = AssetToBitmapConverter(context,size)
    }

    fun authWithEmailAndPasswordCoroutine(email:String,pwd:String){

        //Log.w("THREAD","Main --> "+Thread.currentThread().id)
        viewModelScope.launch {
            db.authWithEmailAndPasswordCoroutine(email, pwd)
        }
    }

    fun logOut(){
        temporaryUserData = UserProfile()
        currentUserMarker = null

        db.logOut()
    }

    fun isLogged() : Boolean{
        return db.isLogged()
    }

    fun unsubscribeToOtherUser(){
        db.unsubscribeToOtherUser()
    }

    fun coroutineLoadProfile(userEmail: String = ""){
        //Log.w("THREAD","Main --> "+Thread.currentThread().id)
        if(userEmail.isEmpty()){
            viewModelScope.launch { db.getLoggedUserProfileCoroutine(userEmail.isEmpty()) }
        }else {
            viewModelScope.launch { db.getUserFromEmailCoroutine(userEmail, userEmail.isEmpty()) }
        }
        Log.d("XXX", "id/email: " + userEmail)
        idOtherProfile = userEmail
    }

    override fun onCleared() {
        super.onCleared()
        db.unsubscribeToUser()
        db.unsubscribeToOtherUser()
    }

    fun coroutineRegisterNewProfile(path:String,
                                    fullname:String,
                                    nickname:String,
                                    location:String,
                                    email:String,
                                    password:String){
        //Log.w("THREAD","Main --> "+Thread.currentThread().id)
        viewModelScope.launch {
            var geoPoint: GeoPoint? = null
            if (currentUserMarker != null)
                geoPoint = GeoPoint(
                    currentUserMarker!!.position.latitude,
                    currentUserMarker!!.position.longitude
                )
            db.createNewUserDocumentCoroutine(path,fullname,nickname,location,email,geoPoint,password)
        }
    }

    fun coroutineUpdateProfile(path:String,
                               fullname:String,
                               nickname:String,
                               location:String,
                               email:String,
                               pwd : String=""){
        try {
            //Log.w("THREAD","Main --> "+Thread.currentThread().id)
            viewModelScope.launch {
                var emailChanged = false
                if (email != userLiveData.value?.email)
                    emailChanged = true
                if ((emailChanged && pwd.isNotEmpty() && pwd.length >= 6) ||
                    (!emailChanged)
                ) {
                    var geoPoint: GeoPoint? = null
                    if (currentUserMarker != null)
                        geoPoint = GeoPoint(
                            currentUserMarker!!.position.latitude,
                            currentUserMarker!!.position.longitude
                        )

                    db.updateUserDocumentCoroutine(
                        path,
                        fullname,
                        nickname,
                        location,
                        email,
                        pwd,
                        geoPoint,
                        emailChanged
                    )
                } else
                    throw InvalidUserProfileUpdateException("Conditions for profile update not met")
            }
        } catch (e: InvalidUserProfileUpdateException){
            throw e
        }
    }

    /**
     * Add comment into remote DB.
     */
    fun addComment(comment: Comment) {
        db.addCommentAndUpdateUserProfile(comment, otherUserProfile.value!!, advertisementID).observeForever {
            newComment = true
            resultAddComment.value = it
        }
    }

    /**
     * Get comments from DB and observe them.
     */
    fun getComments(): LiveData<MutableList<Comment>> {
        comments = MutableLiveData()
        commentUsers.value = mutableMapOf()
        var id = ""
        if (!isMyProfile)
            id = idOtherProfile
        else
            id = userProfileId.value.toString()

        db.getComments(id).observeForever {
            comments.value = it
            for (c in it)
                if (!commentUsers.value!!.containsKey(c.userWC))
                    getCommentUser(c.userWC)
        }
        return comments
    }

    /**
     * Get a specific user associted to a comment from DB and put it in a map.
     */
    fun getCommentUser(id: String): LiveData<MutableMap<String, UserProfile>> {
        db.getUsers(id).observeForever {
            Log.d("XXXUser", "" + it.size)
            commentUsers.value!!.putAll(it)
            commentUsers.value = commentUsers.value
        }
        return commentUsers
    }

    /**
     * Get a specific user associted to a comment from DB.
     */
    fun getUser(id: String): MutableLiveData<UserProfile> {
        db.getUser(id).observeForever {
            commentUser.value = it
        }
        return commentUser
    }
}