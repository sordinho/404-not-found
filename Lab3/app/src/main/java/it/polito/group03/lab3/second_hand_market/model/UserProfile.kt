package it.polito.group03.lab3.second_hand_market.model

import android.os.Parcelable
import com.google.firebase.firestore.GeoPoint
import kotlinx.android.parcel.Parcelize

data class UserProfile (
    val avatarPath: String,
    val fullname: String,
    val nickname: String,
    val email: String,
    val location: String,
    var rating: Float,
    val latLng: GeoPoint?
    ) {
    constructor() : this("","","","","", 0.0F, null)
}