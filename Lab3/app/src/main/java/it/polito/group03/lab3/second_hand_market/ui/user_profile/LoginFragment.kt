package it.polito.group03.lab3.second_hand_market.ui.user_profile

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import it.polito.group03.lab3.second_hand_market.R
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_login_user.*


class LoginFragment : Fragment() {
    //private lateinit var gso : GoogleSignInOptions
    //private lateinit var mGoogleSignInClient : GoogleSignInClient
    //private lateinit var account : GoogleSignInAccount
    //private val SIGN_IN_REQUEST_CODE = 123
    private lateinit var vm : UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        vm = activity?.let{ ViewModelProvider(it).get(UserViewModel::class.java) }!!

        /*gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("210357382675-1dvfqn9k6t1o146al3uvg8cm64lef12q.apps.googleusercontent.com")
                .requestEmail()
                .build()
        mGoogleSignInClient = activity?.let { GoogleSignIn.getClient(it,gso) }!!*/

        return inflater.inflate(R.layout.fragment_login_user, container, false)
    }

    override fun onStart() {
        super.onStart()
        //val account = GoogleSignIn.getLastSignedInAccount(requireContext())
        //if(account != null ){
            //doSomething() //logged
        //} else {
            //notLogged
        //}
    }

    private fun validateFields() : Boolean{
        val inputEmail : String= et_emailLayout_login.editText?.text.toString().trim()
        val inputPwd : String= et_passwordLayout_login.editText?.text.toString().trim()
        var retVal = true
        if(inputEmail.isEmpty()){
            et_emailLayout_login.error = "This field can't be empty"
            retVal = false
        } else if(!Patterns.EMAIL_ADDRESS.matcher(inputEmail).matches()){
            et_emailLayout_login.error = "This should be a valid email address"
            retVal = false
        } else {
            et_emailLayout_login.error = null
        }
        if(inputPwd.isEmpty()){
            et_passwordLayout_login.error = "This field can't be empty"
            retVal = false
        } else if(inputPwd.length < 6){
            et_passwordLayout_login.error = "Password must be at least 6 characters long"
            retVal = false
        } else {
            et_passwordLayout_login.error = null
        }
        return retVal
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*val account = GoogleSignIn.getLastSignedInAccount(requireContext())
        if(account != null){
            Log.d("boh" , account.displayName + "---"+account.email)
            mGoogleSignInClient.signOut()
        }*/

        btn_login.setOnClickListener{ _ ->
            val imm = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(this.view?.rootView?.windowToken, 0)

                if(validateFields()) {

                    vm.authWithEmailAndPasswordCoroutine(
                        et_emailLayout_login.editText?.text.toString(),
                        et_passwordLayout_login.editText?.text.toString()
                    )

                    NavHostFragment.findNavController(nav_host_fragment)
                        .navigate(R.id.action_nav_login_to_nav_on_sale)
                }
        }

        /*sign_in_button.setOnClickListener{view ->
            val signInIntent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, SIGN_IN_REQUEST_CODE)
        }*/
        btn_register.setOnClickListener { view ->
            val imm = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(this.view?.rootView?.windowToken, 0)
            vm.isRegistration = true
            vm.profilePicHasChanged = false
            vm.profileHasChanged=false
            NavHostFragment.findNavController(nav_host_fragment)
                .navigate(R.id.action_nav_login_to_nav_user_edit_profile)
        }

        val from_top = AnimationUtils.loadAnimation(context, R.anim.from_top)
        val from_bottom = AnimationUtils.loadAnimation(context, R.anim.from_bottom)

        logo.animation = from_top
        tv_login.animation = from_top
        et_emailLayout_login.animation = from_bottom
        et_passwordLayout_login.animation = from_bottom
        btn_login.animation = from_bottom
        btn_register.animation = from_bottom
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        /*if(requestCode == SIGN_IN_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }*/
    }

   /* private fun handleSignInResult(task : Task<GoogleSignInAccount>){
        try {
            val account = task.getResult(ApiException::class.java)

            Log.d("DEBUG -------","INFO :" +account?.email ?: "nothing")
            Log.d("DEBUG -------", "others: "+ account?.displayName)

            Log.d("DEBUG -------", "others: "+ account?.idToken)
            //account?.email?.let { vm.coroutineAuthWithGoogle(it) }
            account?.idToken?.let{
                vm.coroutineAuthWithGoogle(it)
            }
            NavHostFragment.findNavController(nav_host_fragment)
                .navigate(R.id.action_nav_login_to_nav_on_sale)
        } catch (ex : ApiException){
            Log.d("WRONG -------", "signInResult : failed "+ ex.statusCode)
        }
    }*/
}