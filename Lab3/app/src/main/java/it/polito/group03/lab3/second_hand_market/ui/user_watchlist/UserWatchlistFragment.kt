package it.polito.group03.lab3.second_hand_market.ui.user_watchlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.ui.user_profile.UserViewModel
import kotlinx.android.synthetic.main.watchlist_fragment.*

class UserWatchlistFragment : Fragment() {
    private lateinit var userWatchlistAdapter: UserWatchlistAdapter
    private lateinit var userWatchlistViewModel: UserWatchlistViewModel
    private lateinit var userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        userWatchlistViewModel =
            ViewModelProviders.of(this).get(UserWatchlistViewModel::class.java)
        userViewModel = activity?.let { ViewModelProvider(it).get(UserViewModel::class.java) }!!
        val root = inflater.inflate(R.layout.watchlist_fragment, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userWatchlistAdapter = UserWatchlistAdapter(findNavController())
        itemList.adapter = userWatchlistAdapter
        itemList.layoutManager = LinearLayoutManager(context)
        userWatchlistViewModel.myUserId = userViewModel.userProfileId.value!!
        observeData()
    }

    /**
     * This method allows you to observe the data of the view model.
     */
    private fun observeData() {
        shimmer_view_container_watchlist.startShimmer()
        userWatchlistViewModel.fetchAdvertisements().observe(viewLifecycleOwner, Observer {
            shimmer_view_container_watchlist.stopShimmer()
            shimmer_view_container_watchlist.visibility = View.GONE
            userWatchlistAdapter.watchlist = it

            messageListEmpty.isVisible = it.isEmpty()
        })

        userWatchlistViewModel.repository.errorOrException.observe(viewLifecycleOwner, Observer {
            if (it) {
                Snackbar.make(
                    constraintLayout,
                    "Error during the loading of advertisements, retry!",
                    Snackbar.LENGTH_LONG
                )
                    .setAction("Action", null).show()
                userWatchlistViewModel.repository.errorOrException.value = false
            }
        })
    }


}