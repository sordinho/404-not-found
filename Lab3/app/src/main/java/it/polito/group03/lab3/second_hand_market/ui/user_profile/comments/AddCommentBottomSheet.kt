package it.polito.group03.lab3.second_hand_market.ui.user_profile.comments

import android.app.Dialog
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.activityViewModels
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.model.Comment
import it.polito.group03.lab3.second_hand_market.ui.user_profile.UserViewModel
import kotlinx.android.synthetic.main.add_comment.*
import kotlinx.android.synthetic.main.add_comment_buttons.*
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.*

class AddCommentBottomSheet : BottomSheetDialogFragment() {
    lateinit var buttons: View
    private val userProfile: UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.add_comment, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        buttons = bottomSheetDialog.layoutInflater.inflate(R.layout.add_comment_buttons, null)
        bottomSheetDialog.setOnShowListener {
            val coordinator = (it as BottomSheetDialog)
                .findViewById<CoordinatorLayout>(com.google.android.material.R.id.coordinator)
            val containerLayout =
                it.findViewById<FrameLayout>(com.google.android.material.R.id.container)

            buttons.layoutParams = FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT
            ).apply {
                gravity = Gravity.BOTTOM
            }
            containerLayout!!.addView(buttons)

            /*
            * Dynamically update bottom sheet containerLayout bottom margin to buttons view height
            * */
            buttons.post {
                (coordinator!!.layoutParams as ViewGroup.MarginLayoutParams).apply {
                    buttons.measure(
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                    )
                    this.bottomMargin = buttons.measuredHeight
                    containerLayout.requestLayout()
                }
            }
        }
        return bottomSheetDialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val postButton = buttons.findViewById<Button>(R.id.post_button)
        val resetButton = buttons.findViewById<Button>(R.id.reset)
        val cancelButton = buttons.findViewById<Button>(R.id.cancel_button)

        postButton.setOnClickListener {
            checkComment()
        }

        resetButton.setOnClickListener {
            resetComment()
        }

        cancelButton.setOnClickListener {
            this.dismiss()
        }
    }

    /**
     * Reset the GUI
     */
    private fun resetComment() {
        title.setText("")
        body.setText("")
        ratingBar2.rating = 0.0F
    }

    /**
     * Check if the user entered the rate or he/she also added a textual comment.
     */
    private fun checkComment() {
        if (ratingBar2.rating == 0.0F && title.text.toString() == "" && body.text.toString() == "") {
            et_title.error = "Rate the seller or also add a comment (with title and body)."
            et_body.error = "Rate the seller or also add a comment (with title and body)."
        } else if (title.text.toString() == "" && body.text.toString() != "") {
            et_title.error = "Add title of comment!"
            et_body.error = ""
        } else if (title.text.toString() != "" && body.text.toString() == "") {
            et_title.error = ""
            et_body.error = "Add body of comment!"
        } else if (ratingBar2.rating == 0.0F && title.text.toString() != "" && body.text.toString() != "") {
            et_title.error = "Plese, rate the seller!"
            et_body.error = "Plese, the seller!"
        } else {
            addComment()
            this.dismiss()
        }
    }

    /**
     * Save the comment.
     */
    private fun addComment() {
        val comment = Comment(
            title.text.toString(),
            body.text.toString(),
            ratingBar2.rating,
            userProfile.idOtherProfile,
            userProfile.userProfileId.value.toString(), //da rimuovere
            SimpleDateFormat("yyyy/MM/dd_HHmmss").format(Date())
        )
        userProfile.addComment(comment)
    }
}