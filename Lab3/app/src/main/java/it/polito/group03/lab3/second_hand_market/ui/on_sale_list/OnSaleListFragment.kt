package it.polito.group03.lab3.second_hand_market.ui.on_sale_list

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.ui.user_profile.UserViewModel
import kotlinx.android.synthetic.main.on_sale_list_fragment.*
import kotlinx.android.synthetic.main.on_sale_list_fragment.itemList
import kotlinx.android.synthetic.main.on_sale_list_fragment.messageListEmpty
import kotlinx.android.synthetic.main.on_sale_list_fragment.shimmer_view_container_interested

class OnSaleListFragment : Fragment() {
    private var isInSearch: Boolean = false
    private var bottomSheet = FilterBottomSheet()
    lateinit var onSaleAdvertisementAdapter: OnSaleAdvertisementAdapter
    val onSaleListViewModel: OnSaleListViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.on_sale_list_fragment, container, false)
        setHasOptionsMenu(true)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onSaleAdvertisementAdapter = OnSaleAdvertisementAdapter(findNavController())
        itemList.adapter = onSaleAdvertisementAdapter
        itemList.layoutManager = LinearLayoutManager(context)
        itemList.setItemViewCacheSize(20)
        observeData()
    }

    /**
     * Inflate the menu when created.
     */
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.search_advertisements, menu)
        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem?.actionView as SearchView

        if (onSaleListViewModel.searchTitle != "") {
            searchItem.expandActionView()
            searchView.setQuery(onSaleListViewModel.searchTitle, true)
            searchView.clearFocus()
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                onSaleListViewModel.searchAdvertisements(query!!)
                shimmer_view_container_interested.startShimmer()
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                isInSearch = newText != ""
                onSaleListViewModel.searchAdvertisements(newText)
                if (shimmer_view_container_interested != null)
                    shimmer_view_container_interested.startShimmer()
                return false
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.action_filter -> {
                activity?.supportFragmentManager?.let {
                    bottomSheet.show(it, "add_bottom_sheet")
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * This method allows you to observe the data of the view model.
     */
    private fun observeData() {
        shimmer_view_container_interested.startShimmer()
        onSaleListViewModel.fetchAdvertisements().observe(viewLifecycleOwner, Observer {
            shimmer_view_container_interested.stopShimmer()
            shimmer_view_container_interested.visibility = View.GONE
            onSaleAdvertisementAdapter.onSaleAdvertisements = it

            if (it.isEmpty()) {
                if (isInSearch || !onSaleListViewModel.filter.isEmpty()) {
                    messageListEmpty.text = "No results found, try again."
                    messageListEmpty.isVisible = true
                } else {
                    messageListEmpty.text = "At the moment there is no advertisement."
                    messageListEmpty.isVisible = true
                }
            } else {
                messageListEmpty.isVisible = false
            }
        })

        onSaleListViewModel.repository.errorOrException.observe(viewLifecycleOwner, Observer {
            if (it) {
                Snackbar.make(
                    constraintLayout,
                    "Error during the loading of advertisements, retry!",
                    Snackbar.LENGTH_LONG
                )
                    .setAction("Action", null).show()
                onSaleListViewModel.repository.errorOrException.value = false
            }
        })
    }
}