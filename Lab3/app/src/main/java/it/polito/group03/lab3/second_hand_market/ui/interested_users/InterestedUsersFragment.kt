package it.polito.group03.lab3.second_hand_market.ui.interested_users

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.model.Advertisement
import kotlinx.android.synthetic.main.interested_users.*

class InterestedUsersFragment : BottomSheetDialogFragment() {
    private lateinit var usersAdapter: InterestedUserAdapter
    private lateinit var interestedUsersViewModel: InterestedUsersViewModel
    var advertisementID: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        interestedUsersViewModel =
            ViewModelProviders.of(this).get(InterestedUsersViewModel::class.java)
        return inflater.inflate(R.layout.interested_users, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fillFieldsFromBundle(arguments)
        usersAdapter = InterestedUserAdapter(findNavController(), interestedUsersViewModel.advertisement.soldTo)
        itemListInterested.adapter = usersAdapter
        itemListInterested.layoutManager = LinearLayoutManager(context)
        observeData()
    }

    /**
     * Loads data about interested users and keeps it updated
     */
    private fun observeData() {
        interestedUsersViewModel.fetchUsers().observe(viewLifecycleOwner, Observer {
            usersAdapter.users = it
        })

        interestedUsersViewModel.repository.errorOrException.observe(viewLifecycleOwner, Observer {
            if (it) {
                Snackbar.make(
                    rootView,
                    "Error during the loading of interrested user, retry!",
                    Snackbar.LENGTH_LONG
                )
                    .setAction("Action", null).show()
                interestedUsersViewModel.repository.errorOrException.value = false
            }
        })
    }

    /**
     * Fill fields from received bundle
     */
    private fun fillFieldsFromBundle(arguments: Bundle?) {
        interestedUsersViewModel.advertisementID = arguments?.getString("advertisementID").toString()
        interestedUsersViewModel.advertisement = arguments?.getParcelable<Advertisement>("item")!!
    }
}