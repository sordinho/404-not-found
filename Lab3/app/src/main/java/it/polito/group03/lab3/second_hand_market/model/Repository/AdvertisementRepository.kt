package it.polito.group03.lab3.second_hand_market.model.Repository

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.google.firebase.storage.ktx.storageMetadata
import it.polito.group03.lab3.second_hand_market.model.Advertisement
import it.polito.group03.lab3.second_hand_market.model.UserProfile
import it.polito.group03.lab3.second_hand_market.ui.on_sale_list.Filter
import java.io.File


class AdvertisementRepository {
    val db = Firebase.firestore
    val items = db.collection("items")
    val mut = MutableLiveData<MutableMap<String, Advertisement>>()
    val mutInterestedUsers = MutableLiveData<MutableMap<String, UserProfile>>()
    var snapShotUsers: ListenerRegistration? = null
    var snapShotAdvertisements: ListenerRegistration? = null
    var snapShotAdvertisement: ListenerRegistration? = null
    var errorOrException = MutableLiveData<Boolean>()

    /**
     * Save an advertisement to firebase.
     * docID == "" => new advertisement to insert in DB
     */
    fun saveAdvertisement(docID: String, advToSave: Advertisement): MutableLiveData<String> {
        Log.d("XXX", "Saving to firebase this: $docID")
//        var result = MutableLiveData<Boolean>()
        var result = MutableLiveData<String>()
        // Save adv data
        var generatedDocId = docID
        if (docID == "") {
            // Save a new Document
            items.add(advToSave)
                .addOnSuccessListener { documentReference ->
                    Log.d("XXX", "DocumentSnapshot written with ID: ${documentReference.id}")
                    generatedDocId = documentReference.id
                    uploadAdvertisementImage(generatedDocId, advToSave)
//                    result.value = true
                    result.value = generatedDocId
                }
                .addOnFailureListener { e ->
                    Log.w("XXX", "Error adding document", e)
//                    result.value = false
                    result.value = "ERROR"
                }
        } else {
            items.document(docID).set(advToSave)
                .addOnSuccessListener {
                    Log.d("XXX", "DocumentSnapshot successfully written!")
                    if (!advToSave.thumbnailPath.startsWith("http")) // if starts with http is the old image already on firebase
                        uploadAdvertisementImage(generatedDocId, advToSave)
//                    result.value = true
                    result.value = generatedDocId
                }
                .addOnFailureListener { e ->
                    Log.w("XXX", "Error writing document", e)
//                    result.value = false
                    result.value = "ERROR"
                }
        }
        return result
    }

    /**
     * Upload the advertisement image to firebase storage and get a link for download back
     */
    private fun uploadAdvertisementImage(documentId: String, adv: Advertisement) {
        // Upload image
        // Create a storage reference from our app
        val storage = Firebase.storage
        val storageRef = storage.reference

        val metadata = storageMetadata {
            contentType = "image/jpg"
        }

        var file = Uri.fromFile(File(adv.thumbnailPath))
        val imageRef = storageRef.child("advertisements/$documentId")
        var downloadUri: Uri?
        val uploadTask = imageRef.putFile(file, metadata)

        uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            imageRef.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                downloadUri = task.result
                Log.d("XXX", "Immagine caricata: $downloadUri")
                setDownloadUrlAdvImage(documentId, downloadUri!!)
            } else {
                Log.d("XXX", "Immagine NON caricata")
            }
        }
    }

    /**
     * Set the download url inside the DB for downloading the advertisement image
     */
    private fun setDownloadUrlAdvImage(docID: String, downloadUri: Uri) {
        items.document(docID).update(
            "thumbnailPath", downloadUri.toString(),
            "photoPath", downloadUri.toString()
        ).addOnCompleteListener { task ->
            if (task.isSuccessful)
                Log.d("XXX", "modifca foto path OK")
            else
                Log.d("XXX", "ERROR modifica foto path")
        }
    }

    /**
     * This method creates a MutableMap.
     * Key: is a document id.
     * Value: is an advertisement object.
     */
    private fun createMapAdvertisements(querySnapshot: QuerySnapshot): MutableMap<String, Advertisement> {
        val listAdv = mutableMapOf<String, Advertisement>()
        for (document in querySnapshot.documents) {
            val id = document.id
            val adv = document.toObject(Advertisement::class.java)
            listAdv[id] = adv!!
        }
        return listAdv
    }

    /**
     * This method creates a MutableMap.
     * Key: is a document id.
     * Value: is an advertisement object of owner.
     */
    private fun createMapAdvertisements(querySnapshot: QuerySnapshot, owner: String): MutableMap<String, Advertisement> {
        val listAdv = mutableMapOf<String, Advertisement>()
        for (document in querySnapshot.documents) {
            val id = document.id
            val adv = document.toObject(Advertisement::class.java)
            if (adv!!.owner != owner && (adv.status == "Selling" || adv.status == "Blocked")) {
                listAdv[id] = adv
            }
        }
        return listAdv
    }

    /**
     * This method allows you to query and apply listeners.
     */
    private fun advertisementsQuery(query: Query) {
        if (snapShotAdvertisements != null)
            snapShotAdvertisements!!.remove()

        snapShotAdvertisements =
            query.addSnapshotListener { querySnapshot, firebaseFirestoreException ->
                if (firebaseFirestoreException != null) {
                    errorOrException.value = true
                } else if (querySnapshot != null) {
                    mut.value = createMapAdvertisements(querySnapshot)
                }
            }
    }

    private fun advertisementsQuery(query: Query, owner: String) {
        if (snapShotAdvertisements != null)
            snapShotAdvertisements!!.remove()

        snapShotAdvertisements =
            query.addSnapshotListener { querySnapshot, firebaseFirestoreException ->
                if (firebaseFirestoreException != null) {
                    errorOrException.value = true
                } else if (querySnapshot != null) {
                    mut.value = createMapAdvertisements(querySnapshot, owner)
                }
            }
    }

    /**
     * This method handles a filter and generetes a specific filter.
     */
    private fun handleFilter(filter: Filter): Query {
        var query: Query = items
        if (filter.isAsc)
            query = query.orderBy("price", Query.Direction.ASCENDING)
        else if (filter.isDesc)
            query = query.orderBy("price", Query.Direction.DESCENDING)

        if (filter.mainCategory != "")
            query = query.whereEqualTo("category", filter.mainCategory)
        if (filter.subCategory != "")
            query = query.whereEqualTo("subCategory", filter.subCategory)
        if (filter.fromPrice != 0.0)
            query = query.whereGreaterThanOrEqualTo("price", filter.fromPrice)
        if (filter.toPrice != Double.MAX_VALUE)
            query = query.whereLessThanOrEqualTo("price", filter.toPrice)
        if (filter.expiryDate != "")
            query = query.whereEqualTo("expiryDate", filter.expiryDate)
        if (filter.location != "")
            query = query.whereEqualTo("location", filter.location)
        return query
    }

    /**
     * This method retives a LiveData<MutableMap<String, Advertisement>>.
     */
    fun getAdvertisements(filter: Filter, owner: String): LiveData<MutableMap<String, Advertisement>> {
        val query: Query = handleFilter(filter)

        advertisementsQuery(query, owner)
        return mut
    }

    /**
     * This method allows you to search with a specific input string.
     */
    fun searchForTitle(title: String, owner: String, filter: Filter): LiveData<MutableMap<String, Advertisement>> {
        val query: Query = handleFilter(filter).whereArrayContains("titleAsArray", title)

        advertisementsQuery(query, owner)
        return mut
    }

    /**
     * This method retives a LiveData<MutableMap<String, Advertisement>> for a specific owner.
     */
    fun getAdvertisements(owner: String): LiveData<MutableMap<String, Advertisement>> {
        val query: Query = items.whereEqualTo("owner", owner)

        advertisementsQuery(query)
        return mut
    }

    /**
     * Get the watchlist of a given user
     */
    fun getWatchlistAdvertisements(userID: String): LiveData<MutableMap<String, Advertisement>> {
        val query: Query = items.whereArrayContains("interestedUsers", userID)
        advertisementsQuery(query)
        return mut
    }

    /**
     * Get the bought list of a given user
     */
    fun getBoughtItemsAdvertisements(userID: String): LiveData<MutableMap<String, Advertisement>> {
        val query: Query = items.whereEqualTo("soldTo", userID)
        advertisementsQuery(query)
        return mut
    }

    /**
     * This method allows you to remove listeners to snapshots
     */
    fun unSubscribe() {
        snapShotAdvertisements?.remove()
        snapShotUsers?.remove()
        snapShotAdvertisement?.remove()
    }

    /**
     * This method retrieves a map that contains the document ID and the correspondent user profile object.
     */
    fun getInterestedUsers(advertisementID: String): MutableLiveData<MutableMap<String, UserProfile>> {
        mutInterestedUsers.value = mutableMapOf()
        db.collection("items").document(advertisementID).addSnapshotListener { snapshot, e ->
            if (e != null){
                errorOrException.value = true
            } else if (snapshot != null) {
                val adv = snapshot.toObject(Advertisement::class.java)!!
                adv.interestedUsers.forEach {
                    Log.d("XXX", it)
                    val advs: MutableMap<String, UserProfile> = mutableMapOf()
                    db.collection("users").document(it).addSnapshotListener { snapshotUser, e1 ->
                        if (e1 != null){
                            errorOrException.value = true
                        } else if (snapshotUser != null) {
                            snapshotUser.toObject(UserProfile::class.java)?.let { it1 ->
                                Log.d("XXX", it1.nickname)
                                advs.set(snapshotUser.id, it1)
                                mutInterestedUsers.value?.putAll(advs)
                                mutInterestedUsers.value = mutInterestedUsers.value
                            }
                        }
                    }
                }
            }
        }
        return mutInterestedUsers
    }

    /**
     * This method returns an advertisement object corresponding to the document ID.
     */
    fun getAdvertisement(advertisementID: String): MutableLiveData<Advertisement> {
        var advertisement = MutableLiveData<Advertisement>()
        snapShotAdvertisement = items.document(advertisementID).addSnapshotListener { snapshot, e ->
            if (e != null) {
                errorOrException.value = true
            }
            if (snapshot != null) {
                advertisement.value = snapshot.toObject(Advertisement::class.java)
            }
        }
        return advertisement
    }

    /**
     * wrapper function to load the id of the advertisements that he owns
     */
    fun getAdvs(owner: String): LiveData<ArrayList<String>> {
        val query: Query = items.whereEqualTo("owner", owner)

        return advertisementLoginQuery(query)
    }

    /**
     * function that executes queries after the user logs in
     */
    fun advertisementLoginQuery(query:Query):LiveData<ArrayList<String>>{
        val adv_list = MutableLiveData<ArrayList<String>>()
        query.get().addOnSuccessListener {
            val listAdv = arrayListOf<String>()
            for (document in it.documents) {
                val id = document.id
                listAdv.add(id)
            }
            adv_list.value = listAdv
        }.addOnCanceledListener {
            errorOrException.value = true
        }
        return adv_list
    }

    /**
     * wrapper function that loads the id of the advertisement the logged user is interested in
     */
    fun getWatchlistAdvs(userID: String): LiveData<ArrayList<String>> {
        val query: Query = items.whereArrayContains("interestedUsers", userID)
        return advertisementLoginQuery(query)
    }
}