package it.polito.group03.lab3.second_hand_market.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Comment(
    val title: String = "",
    val body: String = "",
    val numStars: Float = 0.0F,
    val owner: String = "",
    val userWC: String = "",
    val date: String = ""
) : Parcelable {}