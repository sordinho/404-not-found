package it.polito.group03.lab3.second_hand_market.ui.user_profile.comments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.facebook.shimmer.ShimmerFrameLayout
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.model.Advertisement
import it.polito.group03.lab3.second_hand_market.model.Comment
import it.polito.group03.lab3.second_hand_market.model.UserProfile
import kotlinx.android.synthetic.main.comment_item.*
import java.lang.Exception

class CommentsAdapter(var navController: NavController): RecyclerView.Adapter<CommentsAdapter.ViewHolder>() {
    var user = mutableMapOf<String, UserProfile>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var comments: MutableList<Comment> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    class ViewHolder(var v: View, var navController: NavController) : RecyclerView.ViewHolder(v) {
        val title = v.findViewById<TextView>(R.id.title)
        val body = v.findViewById<TextView>(R.id.body)
        val avatar = v.findViewById<ImageView>(R.id.avatar)
        val username = v.findViewById<TextView>(R.id.username)
        val email = v.findViewById<TextView>(R.id.email)
        val date = v.findViewById<TextView>(R.id.date)
        val ratingBar = v.findViewById<RatingBar>(R.id.ratingBar)
        val shimmer_view_container = v.findViewById<ShimmerFrameLayout>(R.id.shimmer_view_container)

        fun bind(comment: Comment, user: UserProfile?) {
            if (comment.title == "")
                title.visibility = View.GONE
            else{
                title.text = comment.title
                title.visibility = View.VISIBLE
            }

            if (comment.body == "")
                body.visibility = View.GONE
            else {
                body.text = comment.body
                body.visibility = View.VISIBLE
            }

            if (shimmer_view_container != null)
                shimmer_view_container.startShimmer()
            if (user != null) {
                username.text = user.nickname
                email.text = user.email
                if (user.avatarPath != "") {
                    Picasso.get().load(user.avatarPath).into(avatar, object : Callback {
                        override fun onSuccess() {
                            if (shimmer_view_container != null) {
                                shimmer_view_container.stopShimmer()
                                shimmer_view_container.visibility = View.GONE
                            }
                        }

                        override fun onError(e: Exception?) {
                            if (shimmer_view_container != null) {
                                shimmer_view_container.stopShimmer()
                            }
                        }

                    })
                } else {
                    avatar.setImageResource(R.drawable.ic_user)
                    shimmer_view_container.stopShimmer()
                    if (shimmer_view_container != null) {
                        shimmer_view_container.stopShimmer()
                        shimmer_view_container.visibility = View.GONE
                    }
                }
            }
            val commentDate = comment.date.split("_")[0]
            date.text = commentDate.split("/")[2] +
                    "/" + commentDate.split("/")[1] +
                    "/" +commentDate.split("/")[0]
            ratingBar.rating = comment.numStars
            v.setOnClickListener {
                val bundle = Bundle()
                bundle.putParcelable("comment", comment)
                if (navController.currentDestination?.getAction(R.id.action_user_profile_to_showcomment) != null) {
                    navController.navigate(R.id.action_user_profile_to_showcomment, bundle)
                } else {
                    navController.navigate(R.id.action_nav_user_profile_to_showcomment, bundle)
                }
            }
        }

        fun unbind() {
            v.setOnClickListener(null)
        }
    }

    /**
     * it inflates the layout and returnes the ViewHolder object.
     */
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.comment_item, parent, false)
        return ViewHolder(v, navController)
    }

    /**
     * Retriving the size of list.
     */
    override fun getItemCount(): Int {
        return comments.size
    }

    /**
     * It permittes to bind the ViewHolder object with an specific object in the advertisement list.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(comments[position], user[comments[position].userWC])
    }

    /**
     * This method is called when an item is recycled, we can perform an unbind method of ViewHolder.
     */
    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        holder.unbind()
    }
}