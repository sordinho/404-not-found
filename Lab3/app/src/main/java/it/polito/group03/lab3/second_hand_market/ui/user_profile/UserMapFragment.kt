package it.polito.group03.lab3.second_hand_market.ui.user_profile

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.content.res.Resources
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import it.polito.group03.lab3.second_hand_market.MainActivity
import it.polito.group03.lab3.second_hand_market.R
import kotlinx.android.synthetic.main.fragment_location.*
import java.io.IOException
import java.lang.Exception
import java.util.*

class UserMapFragment : Fragment() {
    private lateinit var mMap: GoogleMap
    private lateinit var vm : UserViewModel
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private val REQUEST_LOCATION_PERMISSION = 1

    private val zoom = 15f
    private val TAG = UserMapFragment::class.java.simpleName
    private var userMarker : Marker? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        vm = activity?.let{ ViewModelProvider(it).get(UserViewModel::class.java) }!!

        //setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_location, container, false)
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        try{
            MapsInitializer.initialize(requireContext())
        } catch (e:Exception){
            e.printStackTrace()
        }
        mapView.getMapAsync {
            mMap = it

            // Add a marker in Sydney and move the camera
            val turin = LatLng(45.0578564352, 7.65664237342)
            if(vm.isMyProfile) {
                if (vm.userLiveData.value?.latLng != null) {
                    val pos: LatLng? = vm.userLiveData.value!!.latLng?.latitude?.let { it1 ->
                        vm.userLiveData.value!!.latLng?.longitude?.let { it2 ->
                            LatLng(
                                it1, it2
                            )
                        }
                    }
                    if (pos != null) {
                        userMarker = placeMarkerOnMap(pos)
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, zoom))
                        location_map.text = getAddress(pos)
                    } else {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(turin, 10f))
                    }

                } else {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(turin, 10f))
                }
            } else {
                if (vm.otherUserProfile.value?.latLng != null) {
                    val pos: LatLng? = vm.otherUserProfile.value!!.latLng?.latitude?.let { it1 ->
                        vm.otherUserProfile.value!!.latLng?.longitude?.let { it2 ->
                            LatLng(
                                it1, it2
                            )
                        }
                    }
                    if (pos != null) {
                        userMarker = placeMarkerOnMap(pos)
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, zoom))
                        location_map.text = getAddress(pos)
                    } else {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(turin, 10f))
                    }

                } else {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(turin, 10f))
                }
            }
            /* zoom levels
                    1: World
                    5: Landmass/continent
                    10: City
                    15: Streets
                    20: Buildings
            */

            setMarkerListener()
            setPOIClick(mMap)
            //setMapStyle(mMap)
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())

        if(!vm.isMyProfile) {
            (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true);

        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == REQUEST_LOCATION_PERMISSION){
            if(grantResults.contains(PackageManager.PERMISSION_GRANTED)){
                enableMyLocation()
            }
        }
    }

    private fun isPermissionGranted() : Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    private fun placeMarkerOnMap(latLng: LatLng) : Marker{
        val snippet = String.format(
            Locale.getDefault(),
            "Lat: %1$.5f, Long: %2$.5f",
            latLng.latitude,
            latLng.longitude
        )
        val bm = vm.drawableConverter
            ?.getBitmap(R.drawable.ic_baseline_person_pin_24)
        val markerOptions = MarkerOptions()
            .position(latLng)
            .title(getAddress(latLng))
            .snippet(snippet)
        if(bm == null)
            markerOptions.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
        else
            markerOptions.icon(
                BitmapDescriptorFactory
                    .fromBitmap(bm)
            )

        return mMap.addMarker(markerOptions)
    }

    private fun getAddress(latLng: LatLng):String{
        val geocoder = Geocoder(requireContext())
        val addresses: List<Address>
        val address : Address?
        var addressText = ""
        //Log.d(TAG,"ADDRESS --> "+latLng.toString())

        try{
            addresses = geocoder.getFromLocation(latLng.latitude,latLng.longitude,1)
            if(addresses != null && addresses.isNotEmpty()){
                address = addresses[0]
                //Log.d(TAG,"ADDRESS --> "+address.countryName+", -"+address.getAddressLine(0))

                var i = 0
                while(i <= address.maxAddressLineIndex){
                    addressText += if(i==0) address.getAddressLine(i) else "\n"+address.getAddressLine(i)
                    i++
                }
            }
        } catch (e:IOException){
            Log.e(TAG,"Error getAddress :" + e.localizedMessage)
        }
        //Log.d(TAG,"ADDRESS --> "+addressText)
        return addressText
    }

    private fun enableMyLocation() {
        if(isPermissionGranted()){
            mMap.isMyLocationEnabled = true
            mMap.uiSettings.isMyLocationButtonEnabled=true
            fusedLocationClient.lastLocation.addOnSuccessListener { loc ->
                if(loc != null){
                    lastLocation = loc
                    val currentLatLng = LatLng(loc.latitude,loc.longitude)
                    placeMarkerOnMap(currentLatLng)
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng,zoom))
                }
            }
        } else {
            activity?.let {
                ActivityCompat.requestPermissions(
                    it,
                    arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_LOCATION_PERMISSION
                )
            }
        }
    }

    private fun setMapLongClick(map:GoogleMap){
        map.setOnMapLongClickListener { latlng ->
            placeMarkerOnMap(latlng)
        }
    }

    private fun setPOIClick(map:GoogleMap){
        map.setOnPoiClickListener{
            val poiMarker = map.addMarker(
                MarkerOptions()
                    .position(it.latLng)
                    .title(it.name)
            )
            poiMarker.showInfoWindow()
        }
    }

    private fun setMarkerListener(){
        mMap.setOnMarkerClickListener {
            Log.d(TAG,"-->"+it.title)
            if( userMarker == null) {
                it.remove()
                true
            } else {
                if(it.position.latitude == userMarker!!.position.latitude &&
                    it.position.longitude == userMarker!!.position.longitude)
                    return@setOnMarkerClickListener false
                it.remove()
                true
            }
        }
    }

    private fun setMapStyle(map:GoogleMap){
        try{
            val success = map.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    requireContext(),
                    R.raw.map_style
                )
            )
            if(!success)
                Log.e(TAG,"Style parsing failed")
        } catch (e: Resources.NotFoundException){
            Log.e(TAG,"Can't find style, error : ",e)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //super.onOptionsItemSelected(item)
        return when(item.itemId){
            R.id.normal_map -> {
                mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
                true
            }
            R.id.hybrid_map -> {
                mMap.mapType = GoogleMap.MAP_TYPE_HYBRID
                true
            }
            R.id.satellite_map -> {
                mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
                true
            }
            R.id.terrain_map -> {
                mMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }
}
