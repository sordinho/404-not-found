package it.polito.group03.lab3.second_hand_market.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.util.Log
import android.util.LruCache
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat

class AssetToBitmapConverter(val context:Context,val size: Int) {
    private val cache = LruCache<Int,Bitmap>(size)
    private val N = 31
    private val TAG = AssetToBitmapConverter::class.java.simpleName
    private fun hash(drawable :Int, color: Int? = null):Int{
        var hash = 17*N+drawable
        hash = N*hash + (color ?: 0)
        return hash
    }

    fun getBitmap(
        @DrawableRes drawable: Int,
        @ColorRes color: Int?=null
    ): Bitmap? {
        val currentHash = hash(drawable, color ?: 0)

        if (cache[currentHash] == null) {
            // if it's not in the cache, create it
            val bm = resToBitmap(drawable,color)
            bm?.let {
                // then add it to the cache
                cache.put(currentHash, it)
            }
        }
        // Log performance
        Log.d(TAG,"Hits:${cache.hitCount()} Misses:${cache.missCount()}")
        return cache[currentHash]
    }

    private fun resToBitmap( @DrawableRes resource: Int, @ColorRes tintColor: Int? = null): Bitmap? {
        val drawable = ContextCompat.getDrawable(context, resource) ?: return null
        drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        val bm = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        // draw it onto the bitmap
        val canvas = Canvas(bm)
        drawable.draw(canvas)
        return bm
    }
}