package it.polito.group03.lab3.second_hand_market.ui.on_sale_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import it.polito.group03.lab3.second_hand_market.model.Advertisement
import it.polito.group03.lab3.second_hand_market.model.Repository.AdvertisementRepository

class OnSaleListViewModel : ViewModel() {
    var owner = ""
    set(value) {
        field = value
        fetchAdvertisements()
    }
    val repository =
        AdvertisementRepository()
    lateinit var advs : LiveData<MutableMap<String, Advertisement>>
    var searchTitle: String = ""
    var filter = Filter("", "", "", 0.0, Double.MAX_VALUE, "", false, false)
    set(value) {
        field = value
        fetchAdvertisementsWithFilter()
    }

    /**
     * This method retives LiveData<MutableMap<String, Advertisement>> and applies an observation to detect new changes.
     */
    fun fetchAdvertisements(): LiveData<MutableMap<String, Advertisement>> {
        val advs =  MutableLiveData<MutableMap<String, Advertisement>>()
        if (searchTitle != "") {
            repository.searchForTitle(searchTitle, owner, filter).observeForever { advsMap ->
                advs.value = advsMap
            }
        }else{
            repository.getAdvertisements(filter, owner).observeForever { advsMap ->
                advs.value = advsMap
            }
        }
        this.advs = advs
        return advs
    }

    /**
     * This method allows to apply a filter.
     */
    fun fetchAdvertisementsWithFilter() {
        if (searchTitle != "")
            repository.searchForTitle(searchTitle, owner, filter)
        else
            repository.getAdvertisements(filter, owner)
    }

    /**
     * This method allows to apply a search key.
     */
    fun searchAdvertisements(searchTitle: String) {
        this.searchTitle = searchTitle.toLowerCase()
        repository.searchForTitle(searchTitle, owner, filter)
    }

    override fun onCleared() {
        super.onCleared()
        repository.unSubscribe()
    }
}