package it.polito.group03.lab3.second_hand_market.ui.item_edit

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.Marker
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.messaging.FirebaseMessaging
import it.polito.group03.lab3.second_hand_market.model.Advertisement
import it.polito.group03.lab3.second_hand_market.model.Repository.AdvertisementRepository
import it.polito.group03.lab3.second_hand_market.model.UserProfile
import it.polito.group03.lab3.second_hand_market.model.Repository.UserRepository
import it.polito.group03.lab3.second_hand_market.utils.AssetToBitmapConverter

class AdvertisementViewModel : ViewModel() {
    private val advertisementRepository =
        AdvertisementRepository()
    private val userRepository =
        UserRepository()
    var advertisementID = ""
    val advertisement = MutableLiveData<Advertisement>()
    val items = advertisementRepository.items
    val users = userRepository.users
    var savedInstanceState = false // To track if save instance state has some new value
    var photoModified = false
    var doingAnOperation = false
    var resultOperation = MutableLiveData<Boolean>()
    lateinit var interestedUsersEmail: LiveData<MutableMap<String, UserProfile>>

    var currentItemMarker: Marker? = null
    var temporaryAdvData = Advertisement()
    var loadFromTemporaryAdv = false
    var drawableConverter: AssetToBitmapConverter? = null

    fun setAssetConverter(drawableConverter: AssetToBitmapConverter?) {
        //if(drawableConverter == null)
        this.drawableConverter = drawableConverter
    }

    /**
     * Call before saving adv. So if you re-modify it will be back as the original
     */
    fun restoreBooleans() {
        savedInstanceState = false;
        photoModified = false;
    }

    /**
     * ID = Document ID on firestore
     * Get advertisement from firestore and set mutable Live Data
     */
    fun initAdvertisement(ID: String) {
        advertisementID = ID
        if (ID == "") {
            advertisement.value = null
        } else {
            // Devo cercare nel DB quello gia presente
            getAdvertisementByDocumentID(ID)
        }
    }

    /**
     * Save the advertisement in viewModel
     * Save the advertisement on firestore
     */
    fun saveAdvertisement(adv: Advertisement, persistent: Boolean) {
        advertisement.value = adv
        if (persistent) {
            doingAnOperation = true
            adv.generateTitleAsArray()
            var geopoint: GeoPoint? = null
            if (currentItemMarker != null)
                geopoint = GeoPoint(
                    currentItemMarker!!.position.latitude,
                    currentItemMarker!!.position.longitude
                )
            adv.latLng = geopoint
            advertisementRepository.saveAdvertisement(advertisementID, adv).observeForever {
                if (advertisementID != "")
                    interestedUsersEmail.value?.clear()
                FirebaseMessaging.getInstance().subscribeToTopic("/topics/" + it + "_interest")
            }
        }
    }

    fun getInterestedDocIdFromMail(email: String): String {
        for (userPair in interestedUsersEmail.value!!.toList()) {
            if (userPair.second.email == email)
                return userPair.first
        }
        return ""
    }


    fun initInterestedUsersEmails(): MutableLiveData<MutableMap<String, UserProfile>> {
        var interested = MutableLiveData<MutableMap<String, UserProfile>>()
        interested.value = mutableMapOf()
        advertisementRepository.getInterestedUsers(advertisementID).observeForever { users ->
            interested.value = users
        }

        Log.d("KKK", "users: " + interested)
        this.interestedUsersEmail = interested
        return interested
    }


    private fun getAdvertisementByDocumentID(docID: String) {
        items.document(docID).get().addOnSuccessListener { document ->
            if (document != null) {
                Log.d("XXX", "DocumentSnapshot data: ${document.data}")
                advertisement.value = document.toObject<Advertisement>()!!
            } else {
                Log.d("XXX", "No such document")
                advertisement.value = null
            }
        }
            .addOnFailureListener { exception ->
                Log.d("XXX", "get failed with ", exception)
            }
    }
}