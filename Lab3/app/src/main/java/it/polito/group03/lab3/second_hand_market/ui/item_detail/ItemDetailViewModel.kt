package it.polito.group03.lab3.second_hand_market.ui.item_detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import it.polito.group03.lab3.second_hand_market.model.Advertisement
import it.polito.group03.lab3.second_hand_market.model.Repository.AdvertisementRepository
import it.polito.group03.lab3.second_hand_market.model.UserProfile
import it.polito.group03.lab3.second_hand_market.model.Repository.UserRepository
import it.polito.group03.lab3.second_hand_market.utils.AssetToBitmapConverter

class ItemDetailViewModel : ViewModel() {

    private val repo =
        AdvertisementRepository()
    val repositoryUserProfile =
        UserRepository()
    lateinit var userEmail:String
    var advertisementOwner:String=""
    val advertisement : MutableLiveData<Advertisement> = MutableLiveData()
    var user = MutableLiveData<UserProfile>()
    lateinit var adID:String
    var owned : Boolean = false
    var first = true

    var drawableConverter : AssetToBitmapConverter? = null

    fun getAdvertisement(): LiveData<Advertisement> {
        return advertisement
    }

    override fun onCleared() {
        super.onCleared()
        repo.unSubscribe()
    }

    fun setADID(adID:String?){
        if (adID != null) {
            this.adID = adID
        } else {
            //TODO failure
            return
        }
        if (!first) {
            repo.unSubscribe()
        }
        repo.getAdvertisement(this.adID).observeForever {
            advertisement.value = it
            advertisementOwner = advertisement.value?.owner.toString()
            getUserProfile()
        }
        first = false
    }

    fun getAdvertisementOwnerString():String{
        return advertisementOwner
    }

    fun getADID():String{
        return this.adID
    }

    fun getOwnedTrue():Boolean{
        return this.owned
    }

    fun setOwnedExternal(value:Boolean){
        this.owned=value
    }

    fun getRepo(): AdvertisementRepository {
        return this.repo
    }

    fun setAdvertisementOwnerCode(advertisementOwner:String){
        this.advertisementOwner=advertisementOwner
    }

    fun getUserProfile(): LiveData<UserProfile> {
        repositoryUserProfile.getUser(advertisement.value!!.owner).observeForever {
            user.value = it
        }
        return user
    }

    fun setAssetConverter(drawableConverter: AssetToBitmapConverter?){
        //if(drawableConverter == null)
        this.drawableConverter = drawableConverter
    }
}