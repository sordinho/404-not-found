package it.polito.group03.lab3.second_hand_market.ui.interested_users

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.facebook.shimmer.ShimmerFrameLayout
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.model.UserProfile
import kotlinx.android.synthetic.main.item_detail.*
import java.lang.Exception

class InterestedUserAdapter(
    var navController: NavController,
    var whoItWasSold: String
) : RecyclerView.Adapter<InterestedUserAdapter.ViewHolder>() {
    var users: MutableMap<String, UserProfile> = mutableMapOf<String, UserProfile>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    class ViewHolder(var v: View, var navController: NavController, var whoItWasSold: String) : RecyclerView.ViewHolder(v) {
        val username: TextView = v.findViewById(R.id.username)
        val user_image: ImageView = v.findViewById(R.id.interested_user_image)
        val email: TextView = v.findViewById(R.id.email)
        val sold: TextView = v.findViewById(R.id.sold)
        val shimmer_view_container_interested = v.findViewById<ShimmerFrameLayout>(R.id.shimmer_view_container_interested)

        fun bind(adv: Pair<String, UserProfile>) {
            val id = adv.first
            val user = adv.second
            username.text = user.nickname
            email.text = user.email

            if (id == whoItWasSold)
                sold.visibility = View.VISIBLE

            if (shimmer_view_container_interested != null)
                shimmer_view_container_interested.startShimmer()
            if (user.avatarPath != "") {
                Picasso.get().load(user.avatarPath).into(user_image, object : Callback {
                    override fun onSuccess() {
                        if (shimmer_view_container_interested != null) {
                            shimmer_view_container_interested.stopShimmer()
                            shimmer_view_container_interested.visibility = View.GONE
                        }
                    }

                    override fun onError(e: Exception?) {
                        if (shimmer_view_container_interested != null) {
                            shimmer_view_container_interested.stopShimmer()
                        }
                    }

                })
            } else {
                user_image.setImageResource(R.drawable.ic_user)
                shimmer_view_container_interested.stopShimmer()
                if (shimmer_view_container_interested != null) {
                    shimmer_view_container_interested.stopShimmer()
                    shimmer_view_container_interested.visibility = View.GONE
                }
            }
            v.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("userID", id)
                bundle.putBoolean("isMyProfile", false)
                navController.navigate(R.id.action_interestedUsersFragment_to_user_profile, bundle)
            }
        }

        fun unbind() {
            v.setOnClickListener(null)
        }
    }

    /**
     * it inflates the layout and returnes the ViewHolder object.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.interested_user, parent, false)
        return ViewHolder(v, this.navController, whoItWasSold)
    }

    /**
     * Retriving the size of list.
     */
    override fun getItemCount(): Int = users.size

    /**
     * It permittes to bind the ViewHolder object with an specific object in the InterestedUsers list.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(users.toList()[position])
    }

    /**
     * This method is called when an item is recycled, we can perform an unbind method of ViewHolder.
     */
    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        holder.unbind()
    }
}
