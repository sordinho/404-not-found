package it.polito.group03.lab3.second_hand_market.ui.user_profile

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.*
import androidx.appcompat.app.AlertDialog.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.snackbar.Snackbar
import com.theartofdev.edmodo.cropper.CropImage
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.model.UserProfile
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import java.io.*
import it.polito.group03.lab3.second_hand_market.utils.*
import kotlinx.android.synthetic.main.content_main.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.navigation.NavigationView
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.firestore.GeoPoint
import com.squareup.picasso.Picasso

class EditProfileFragment : Fragment() {
    private lateinit var myActivityMenuItem: MenuItem
    private lateinit var vm : UserViewModel
    var currentPhotoPath: String = ""
    lateinit var cameraGallery : CameraGallery

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        vm = activity?.let{ ViewModelProvider(it).get(UserViewModel::class.java) }!!

        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_edit_profile, container, false)
    }

    private fun updateUI(data : UserProfile) {
        if(data.avatarPath.isNotEmpty() && !vm.profileHasChanged) {
            Picasso.get().load(data.avatarPath).into(editImage_user_avatar)
            editImage_user_avatar.tag = data.avatarPath
        }
        editText_edit_fullname.editText?.setText(data.fullname)
        editText_edit_email.editText?.setText(data.email)
        editText_edit_nickname.editText?.setText(data.nickname)
        editText_edit_location.editText?.setText(data.location)
    }

    private fun loadUser(){
        vm.userLiveData.observe(viewLifecycleOwner, Observer {
                data -> updateUI(data)
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        edit_profile_picture.setOnClickListener {
            cameraGallery = CameraGallery(
                edit_profile_picture,
                requireContext(),
                this
            )
        }

        editText_edit_location.editText?.setOnClickListener {
            listenerEditLocation()
        }

        btn_edit_user_location.setOnClickListener {
            listenerEditLocation()
        }

        editText_edit_location.editText?.addTextChangedListener {
            vm.profileHasChanged = true
        }
        editText_edit_nickname.editText?.addTextChangedListener {
            vm.profileHasChanged = true
        }
        editText_edit_email.editText?.addTextChangedListener {
            vm.profileHasChanged = true
        }
        editText_edit_fullname.editText?.addTextChangedListener {
            vm.profileHasChanged = true
        }

        if(!vm.isRegistration) {
            editText_edit_password.isVisible = false
            if(!vm.profileHasChanged)
                loadUser()
        } else {
            editText_edit_password.editText?.addTextChangedListener {
                vm.profileHasChanged= true
            }
        }
    }

    private fun listenerEditLocation(){
        vm.profileHasChanged = true
        var geopoint : GeoPoint? = null
        if(vm.currentUserMarker!= null)
            geopoint = GeoPoint(vm.currentUserMarker!!.position.latitude, vm.currentUserMarker!!.position.longitude)
        else if(vm.userLiveData.value?.latLng != null)
            geopoint = GeoPoint(vm.userLiveData.value!!.latLng!!.latitude,vm.userLiveData.value!!.latLng!!.longitude)

        vm.temporaryUserData = UserProfile(editImage_user_avatar.tag?.toString() ?: "",
            editText_edit_fullname.editText?.text?.toString() ?: "",
            editText_edit_nickname.editText?.text?.toString() ?: "",
            editText_edit_email.editText?.text?.toString() ?: "",
            editText_edit_location.editText?.text?.toString() ?: "",
            0f,
            geopoint
        )
        vm.loadFromTemporaryData = true
        vm.currentUserMarker = null
        NavHostFragment.findNavController(nav_host_fragment)
            .navigate(R.id.action_nav_user_edit_profile_to_nav_user_edit_map)
        return
    }

    private fun fillFromTemporaryData(){
        val path = vm.temporaryUserData.avatarPath
        if(!path.isEmpty()) {
            if(vm.profilePicHasChanged) {
                editImage_user_avatar.setImageURI(Uri.parse(path))
                editImage_user_avatar.tag = path
            }else {
                Picasso.get().load(path).into(editImage_user_avatar)
                editImage_user_avatar.tag = path
            }
        } else {
            editImage_user_avatar.tag = ""
        }
        currentPhotoPath=path ?: ""
        if(vm.currentUserMarker != null ){
            editText_edit_location.editText?.setText(vm.currentUserMarker!!.title)
        } else
            editText_edit_location.editText?.setText(vm.temporaryUserData.location)
        editText_edit_fullname.editText?.setText(vm.temporaryUserData.fullname)
        editText_edit_nickname.editText?.setText(vm.temporaryUserData.nickname)
        editText_edit_email.editText?.setText(vm.temporaryUserData.email)
        vm.loadFromTemporaryData = false
        vm.temporaryUserData = UserProfile()
    }

    /**
     * Validate fields and eventually display error strings. This makes sure that no fields are empty
     * and validates the format of the email.
     */
    private fun validateFields() : Boolean {
        val inputEmail = editText_edit_email.editText?.text.toString().trim()
        val inputName = editText_edit_fullname.editText?.text.toString().trim()
        val inputNickname = editText_edit_nickname.editText?.text.toString().trim()
        val inputLocation = editText_edit_location.editText?.text.toString().trim()
        var retVal = true
        if(inputEmail.isEmpty()){
            editText_edit_email.error = "This field can't be empty"
            retVal = false
        } else if(!Patterns.EMAIL_ADDRESS.matcher(inputEmail).matches()){
            editText_edit_email.error = "This should be a valid email address"
            retVal = false
        } else {
            editText_edit_email.error = null
        }
        if(inputName.isEmpty()){
            editText_edit_fullname.error = "This field can't be empty"
            retVal = false
        } else {
            editText_edit_fullname.error = null
        }
        if(inputNickname.isEmpty()){
            editText_edit_nickname.error = "This field can't be empty"
            retVal = false
        } else {
            editText_edit_nickname.error = null
        }
        if(inputLocation.isEmpty()) {
            editText_edit_location.error = "This field can't be empty"
            retVal = false
        } else {
            editText_edit_location.error = null
        }

        if(vm.isRegistration){
            val inputPwd = editText_edit_password.editText?.text.toString().trim()
            if(inputPwd.isEmpty() || inputPwd.length < 6){
                editText_edit_password.error = "Password must be at least 6 characters long"
                retVal = false
            } else
                editText_edit_password.error = null
        }
        return retVal
    }

    /**
     * Inflate the menu when created
     */
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        menu.clear()
        inflater.inflate(R.menu.profile_save, menu)
    }

    /**
     * Check which menu item is pressed and start corresponding task
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.nav_user_profile -> {
                if(!validateFields())
                    false
                else {
                    myActivityMenuItem = item
                    item.isEnabled = false
                    if(!vm.isRegistration) {
                        //Log.d("THREAD MAIN ", "t ----> "+Thread.currentThread().id)
                        if(editText_edit_email.editText?.text.toString() != vm.userLiveData.value?.email) {
                            val text = layoutInflater.inflate(R.layout.dialog_pwd, null)
                            val et = text.findViewById<TextInputLayout>(R.id.et_dialog_pwd);
                            var pwd = ""

                            val alertDialog = Builder(requireContext()).create()
                            alertDialog.setTitle("Enter your password.")
                            alertDialog.setCancelable(false)
                            alertDialog.setMessage("Seems like you want to change your email. Re-enter your password to complete the operation.")


                            alertDialog.setButton(
                                BUTTON_POSITIVE,
                                "OK"
                            ) { dialogInterface: DialogInterface, i: Int ->
                                pwd = et.editText?.text.toString()
                                if(pwd.isNotEmpty() && pwd.length >= 6) {
                                    try {
                                        if(!vm.profilePicHasChanged)
                                            editImage_user_avatar.tag = ""
                                        vm.coroutineUpdateProfile(
                                            editImage_user_avatar.tag?.toString() ?: "",
                                            editText_edit_fullname.editText?.text.toString() ?: "",
                                            editText_edit_nickname.editText?.text.toString() ?: "",
                                            editText_edit_location.editText?.text.toString() ?: "",
                                            editText_edit_email.editText?.text.toString() ?: "",
                                            pwd
                                        )
                                        vm.profilePicHasChanged = false
                                        vm.profileHasChanged = false
                                        NavHostFragment.findNavController(nav_host_fragment)
                                            .navigate(R.id.action_nav_user_edit_profile_to_nav_user_profile)
                                    } catch (e : InvalidUserProfileUpdateException){
                                        Snackbar.make(
                                            nav_user_edit_profile,
                                            "There was something wrong with the password you entered.Try again!",
                                            Snackbar.LENGTH_LONG
                                        )
                                        item.isEnabled = true
                                    }
                                }else{
                                    editText_edit_email.error = "You must enter the password to change the email"
                                    item.isEnabled = true
                                }
                            }


                            alertDialog.setButton(
                                BUTTON_NEGATIVE,
                                "Cancel"
                            ) { dialogInterface: DialogInterface, i: Int ->
                                Snackbar.make(
                                    nav_user_edit_profile,
                                    "You can't change your email without entering the password",
                                    Snackbar.LENGTH_LONG
                                )
                                    .setAction("Action", null).show()
                                editText_edit_email.error = "You must enter the password to change the email"
                                item.isEnabled = true
                            }


                            alertDialog.setView(text)
                            alertDialog.show()
                        } else {
                            if(!vm.profilePicHasChanged)
                                editImage_user_avatar.tag = ""
                            vm.coroutineUpdateProfile(
                                editImage_user_avatar.tag?.toString() ?: "",
                                editText_edit_fullname.editText?.text.toString() ?: "",
                                editText_edit_nickname.editText?.text.toString() ?: "",
                                editText_edit_location.editText?.text.toString() ?: "",
                                editText_edit_email.editText?.text.toString() ?: "",
                                ""
                            )

                            vm.profilePicHasChanged = false
                            vm.profileHasChanged = false
                            NavHostFragment.findNavController(nav_host_fragment)
                                .navigate(R.id.action_nav_user_edit_profile_to_nav_user_profile)
                        }
                    } else {
                        if(!vm.profilePicHasChanged)
                            editImage_user_avatar.tag = ""
                        vm.coroutineRegisterNewProfile(
                            editImage_user_avatar.tag?.toString() ?: "",
                            editText_edit_fullname.editText?.text.toString() ?: "",
                            editText_edit_nickname.editText?.text.toString() ?: "",
                            editText_edit_location.editText?.text.toString() ?: "",
                            editText_edit_email.editText?.text.toString() ?: "",
                            editText_edit_password.editText?.text.toString() ?: ""
                        )
                        val navView : NavigationView? = activity?.findViewById(R.id.nav_view)
                        if(navView != null){
                            navView.menu.findItem(R.id.nav_login).isVisible = false
                            navView.menu.findItem(R.id.nav_user_profile).isVisible = true
                            navView.menu.findItem(R.id.nav_logout).isVisible = true
                            navView.menu.findItem(R.id.nav_advertisements).isVisible = true
                        }
                        vm.profilePicHasChanged = false
                        vm.profileHasChanged = false
                        NavHostFragment.findNavController(nav_host_fragment)
                            .navigate(R.id.action_nav_user_edit_profile_to_nav_user_profile)
                    }
                    true
                }
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Callback function to keep the state of the activity when rotating it (for example)
     */
    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("avatarImage",editImage_user_avatar?.tag?.toString() ?: "")
        outState.putString("locationText", editText_edit_location?.editText?.text?.toString() ?: "")
        outState.putString("nameText", editText_edit_fullname?.editText?.text?.toString() ?: "")
        outState.putString("emailText", editText_edit_email?.editText?.text?.toString() ?: "")
        outState.putString("nicknameText", editText_edit_nickname?.editText?.text?.toString() ?: "")
        super.onSaveInstanceState(outState)
    }

    /**
     * Callback function to restore the state of the activity after rotating it (for example)
     */
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        if (savedInstanceState != null && !vm.loadFromTemporaryData) {
            val path = savedInstanceState.getString("avatarImage")
            if(!path.isNullOrEmpty()) {
                if(vm.profilePicHasChanged) {
                    editImage_user_avatar.setImageURI(Uri.parse(path))
                    editImage_user_avatar.tag = path
                }else {
                    Picasso.get().load(path).into(editImage_user_avatar)
                    editImage_user_avatar.tag = path
                }
            } else {
                editImage_user_avatar.tag = ""
            }
            currentPhotoPath=path ?: ""
            editText_edit_location.editText?.setText(savedInstanceState.getString("locationText"))
            editText_edit_fullname.editText?.setText(savedInstanceState.getString("nameText"))
            editText_edit_nickname.editText?.setText(savedInstanceState.getString("nicknameText"))
            editText_edit_email.editText?.setText(savedInstanceState.getString("emailText"))
        } else if(vm.loadFromTemporaryData){
            fillFromTemporaryData()
        }
    }

    /**
     * Callback function that handles the arrival of an intent as result (After calling startActivityForResult)
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //super.onActivityResult(requestCode, resultCode, data)
        if(!this::cameraGallery.isInitialized){
            Log.d("DEBUG","restoring CameraGallery")
            cameraGallery = CameraGallery(edit_profile_picture,
                    requireContext(),
                this,
                false)
            val sharedPref = activity?.getSharedPreferences(
                getString(R.string.preference_file_key),
                Context.MODE_PRIVATE
            )
            cameraGallery.photoPath = sharedPref?.getString("persistentCurrentPhotoPath", "") ?: ""
            cameraGallery.imgUri = Uri.parse(sharedPref?.getString("persistentCurrentUri", "") ?: "")
            //Log.d("DEBUG", "restored cameragallery photopath" + cameraGallery.photoPath)
        }

        when(requestCode){
            cameraGallery.REQUEST_IMAGE_CAPTURE -> {
                if(resultCode == AppCompatActivity.RESULT_OK ){
                    if (cameraGallery.imgUri == null) {
                        Log.d("", "URI is null")
                        return
                    }
                    //val uri: Uri = cameraGallery.imgUri!!
                    currentPhotoPath = cameraGallery.photoPath
                    cameraGallery.handleRotation()
                    cameraGallery.launchImageCrop(16,9)
                    vm.profilePicHasChanged=true
                    vm.profileHasChanged=true
                    editImage_user_avatar.tag = currentPhotoPath
                } else {
                    Snackbar.make(nav_user_edit_profile, "Error while taking the photo!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }
            }
            cameraGallery.REQUEST_IMAGE_GALLERY -> {
                if(resultCode == AppCompatActivity.RESULT_OK && data != null) {
                    val ins: InputStream? =
                        this
                            .activity
                            ?.contentResolver
                            ?.openInputStream(data.data!!)                   /* loading the bitmap */
                    var img: Bitmap? = BitmapFactory.decodeStream(ins)
                    cameraGallery.bitmapToFile(img!!)                        /* store the image in application directory */
                    currentPhotoPath = cameraGallery.photoPath
                    editImage_user_avatar.tag = currentPhotoPath
                    //editImage_user_avatar.setImageBitmap(img)
                    vm.profilePicHasChanged=true
                    vm.profileHasChanged=true
                    cameraGallery.launchImageCrop(16,9)
                } else {
                    Snackbar.make(nav_user_edit_profile, "Error while taking the photo!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }
            }
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                if(resultCode == AppCompatActivity.RESULT_OK && data != null){
                    val result = CropImage.getActivityResult(data)
                    val resUri = result.uri

                    cameraGallery.createThumbnail()
                    currentPhotoPath = cameraGallery.thumbnailPath
                    editImage_user_avatar.tag = currentPhotoPath
                    editImage_user_avatar.setImageURI(resUri)
                    vm.profilePicHasChanged=true
                    vm.profileHasChanged=true
                    Snackbar.make(nav_user_edit_profile, "Profile photo updated!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                    Snackbar.make(nav_user_edit_profile, "Profile data not loaded. Try again please!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }
            }
            else -> false
        }
    }
}