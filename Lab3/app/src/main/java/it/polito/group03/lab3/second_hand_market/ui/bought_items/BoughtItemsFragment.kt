package it.polito.group03.lab3.second_hand_market.ui.bought_items

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import it.polito.group03.lab3.second_hand_market.R
import it.polito.group03.lab3.second_hand_market.ui.user_profile.UserViewModel
import kotlinx.android.synthetic.main.bought_items_fragment.*

class BoughtItemsFragment : Fragment() {
    private lateinit var boughtItemsAdapter: BoughtItemsAdapter
    private lateinit var boughtItemsViewModel: BoughtItemsViewModel
    private lateinit var userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        boughtItemsViewModel =
            ViewModelProviders.of(this).get(BoughtItemsViewModel::class.java)
        userViewModel = activity?.let { ViewModelProvider(it).get(UserViewModel::class.java) }!!
        val root = inflater.inflate(R.layout.bought_items_fragment, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        boughtItemsAdapter = BoughtItemsAdapter(findNavController())
        itemList.adapter = boughtItemsAdapter
        itemList.layoutManager = LinearLayoutManager(context)
        boughtItemsViewModel.myUserId = userViewModel.userProfileId.value!!
        observeData()
    }

    /**
     * This method allows you to observe the data of the view model.
     */
    private fun observeData() {
        shimmer_view_container_bought.startShimmer()
        boughtItemsViewModel.fetchAdvertisements().observe(viewLifecycleOwner, Observer {
            shimmer_view_container_bought.stopShimmer()
            shimmer_view_container_bought.visibility = View.GONE
            boughtItemsAdapter.boughtlist = it

            messageListEmpty.isVisible = it.isEmpty()
        })

        boughtItemsViewModel.repository.errorOrException.observe(viewLifecycleOwner, Observer {
            if (it) {
                Snackbar.make(
                    constraintLayout,
                    "Error during the loading of advertisements, retry!",
                    Snackbar.LENGTH_LONG
                )
                    .setAction("Action", null).show()
                boughtItemsViewModel.repository.errorOrException.value = false
            }
        })
    }
}