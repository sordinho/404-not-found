import {StyleSheet} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    engine: {
        position: 'absolute',
        right: 0,
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: Colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
    highlight: {
        fontWeight: '700',
    },
    footer: {
        color: Colors.dark,
        fontSize: 12,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'right',
    },
    item: {
        flexDirection: 'row',
        marginTop: 8,
        flex: 1,
        paddingRight: 15,
        paddingTop: 13,
        paddingBottom: 13,
        borderBottomWidth: 0.5,
        borderColor: '#c9c9c9',
        alignItems: 'center',
    },
    departure: {
        flex: 1,
        marginStart: 16,
    },
    arrival: {
        flex: 1,
        marginEnd: 16,
    },
    nameStation: {
        fontSize: 18,
        color: Colors.black,
        alignContent: 'flex-end',
        textAlign: 'center', // <-- the magic
    },
    time: {
        fontSize: 14,
        textAlign: 'center', // <-- the magic
    },
    date: {
        fontSize: 10,
        textAlign: 'center', // <-- the magic
    },
    changes: {
        flex: 1,
        marginTop: 0,
        color: Colors.black,
        textAlign: 'center', // <-- the magic
        justifyContent: 'center',
        alignItems: 'center',
    },
    label: {
        fontSize: 12,
        textAlign: 'center', // <-- the magic
    },
    logo: {
        width: 30,
        height: 30,
        marginTop: 16,
        marginBottom: 16,
    },
    departure_details: {
        marginTop: 8,
        paddingRight: 15,
    },
    change_details: {
        paddingRight: 15,
        paddingTop: 13,
    },
    arrival_details: {
        paddingRight: 15,
        paddingTop: 13,
        paddingBottom: 13,
        alignItems: 'center',
    },
    details_item: {
        flex: 1,
        borderBottomWidth: 0.5,
        backgroundColor: '#FFFFFF',
        alignItems: 'stretch',
    },
    details_logo: {
        width: 20,
        height: 20,
        marginTop: 16,
    },
    details_walk: {
        width: 25,
        height: 40,
        marginTop: 16,
    },
    details_logo_big: {
        width: 40,
        height: 40,
        marginTop: 32,
        marginBottom: 16,
        alignItems: 'center',
        justifyContent: 'center',
    },
    details_image_container: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 1,
    },
    container: {
        backgroundColor: '#FFFFFF',
        flex: 1,
        padding: 5,
    },
    autocompleteContainer: {
        backgroundColor: '#ffffff',
        borderWidth: 1,
        borderColor: '#673AB7',
        left: 0,
        position: 'absolute',
        right: 0,
        top: 0,
        zIndex: 3,
    },
    autocompleteContainer2: {
        backgroundColor: '#ffffff',
        borderColor: '#673AB7',
        borderWidth: 1,
        left: 0,
        position: 'absolute',
        right: 0,
        top: 75,
        zIndex: 2,
    },
    descriptionContainer: {
        flex: 1,
        justifyContent: 'center',
    },
    itemText: {
        fontSize: 15,
        paddingTop: 5,
        paddingBottom: 5,
        margin: 2,
    },
    infoText: {
        textAlign: 'center',
        fontSize: 16,
    },
    bordered: {
        borderColor: '#673AB7',
        fontSize: 15,
        borderWidth: 1,
        padding: 5,
    },
    nameStation_details: {
        fontSize: 18,
        color: '#673AB7',
        textAlign: 'center',
    },
    nameStation_small:{
        fontSize: 15,
        color: '#673AB7',
        textAlign: 'center',
    },
    sectionTitle_details: {
        fontSize: 24,
        color: '#8b0000',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    summaryStationText: {
        fontSize: 15,
        color: '#673AB7',
    },
    summaryStationView: {
        flex: 1,
        padding: 5,
    },
});

export default styles;
