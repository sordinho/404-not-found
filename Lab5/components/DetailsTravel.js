import React, {useContext, useState} from 'react';
import {View, Text, FlatList, Image} from 'react-native';
import styles from '../Styles';
import StationsCtx from '../context/StationsContext';

function Station({value}) {
  if (value.typeStation === 'departure') {
    return (
      <Departure
        trainName={value.trainName}
        stationName={value.stationName}
        departureTime={value.departureTime}
        date={value.date}
      />
    );
  } else if (value.typeStation === 'walk') {
    return (
      <Walk
        seconds={value.walkingSeconds}
        stationName={value.stationName}
        dstName={value.dstName}
        departureTime={value.departureTime}
        date={value.date}
        last={value.last}
      />
    );
  } else if (value.typeStation === 'intermediate') {
    return (
      <Intermediate
        stationName={value.stationName}
        departureTime={value.departureTime}
        date={value.date}
      />
    );
  } else if (value.typeStation === 'arrival') {
    return (
      <Arrival
        stationName={value.stationName}
        departureTime={value.departureTime}
        date={value.date}
        last={value.last}
      />
    );
  }
}

function Intermediate({stationName, departureTime}) {
  return (
    <View style={styles.change_details}>
      <Text style={styles.nameStation_details}>
        {departureTime} - {stationName}
      </Text>
      <View style={styles.details_image_container}>
        <Image
          style={styles.details_logo}
          source={require('../bottom_arrow.png')}
        />
      </View>
    </View>
  );
}

function Arrival({stationName, departureTime, date, last}) {
  if (last) {
    return (
      <View style={styles.arrival_details}>
        <Text style={styles.sectionTitle_details}>Arrival</Text>
        <Text style={styles.nameStation_details}>
          {departureTime} - {stationName}
        </Text>
        <Text style={styles.time}>{date}</Text>
      </View>
    );
  } else {
    return (
      <View style={styles.arrival_details}>
        <Text style={styles.sectionTitle_details}>Arrival</Text>
        <Text style={styles.nameStation_details}>
          {departureTime} - {stationName}
        </Text>
        <Text style={styles.time}>{date}</Text>
        <Image
          style={styles.details_logo_big}
          source={require('../white-down-arrow-png-8.png')}
        />
      </View>
    );
  }
}

function Departure({stationName, departureTime, date, trainName}) {
  return (
    <View style={styles.departure_details}>
      <Text style={styles.sectionTitle_details}>Departure - {trainName}</Text>
      <Text style={styles.nameStation_details}>
        {departureTime} - {stationName}
      </Text>
      <Text style={styles.time}>{date}</Text>
      <View style={styles.details_image_container}>
        <Image
          style={styles.details_logo}
          source={require('../bottom_arrow.png')}
        />
      </View>
    </View>
  );
}

function Walk({seconds, stationName, dstName, departureTime, date, last}) {
  var hours = 0;
  var minutes = Math.floor(seconds / 60);
  if (minutes >= 60) {
    hours = Math.floor(minutes / 60);
    minutes = minutes % 60;
  }
  var secs = seconds % 60;
  hours = ('0' + hours).slice(-2);
  minutes = ('0' + minutes).slice(-2);
  secs = ('0' + secs).slice(-2);
  var str = '' + hours + ':' + minutes + ':' + secs;
  return (
    <View style={styles.departure_details}>
      <Text style={styles.nameStation_details}>From : {stationName}</Text>
      <Text style={styles.nameStation_details}>To : {dstName}</Text>
      <Text style={styles.time}>
        {date} - {departureTime}
      </Text>
      <Text style={styles.time}>Walking time : {str}</Text>
      <View style={styles.details_image_container}>
        <Image style={styles.details_walk} source={require('../walking.png')} />
      </View>
    </View>
  );
}

function Summary({departureName, arrivalName, departureTime, arrivalTime}) {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginStart: 10,
        marginEnd: 10,
      }}>
      <View style={styles.summaryStationView}>
        <Text numberOfLines={1} style={styles.summaryStationText}>
          From: {departureName}
        </Text>
        <Text>Departure at: {departureTime}</Text>
      </View>
      <View style={[styles.summaryStationView, {alignItems: 'flex-end'}]}>
        <Text numberOfLines={1} style={styles.summaryStationText}>
          To: {arrivalName}
        </Text>
        <Text>Arrival at: {arrivalTime}</Text>
      </View>
    </View>
  );
}

const Stations = () => {
  const context = useContext(StationsCtx);
  const [route, setRoute] = useState(context.route);
  console.log('details');
  // console.log(route.sections[0].journey.passList);
  const data = [];
  var i = 0;
  var j = 0;
  var type = '';
  var date;
  var time;
  var last = false;
  for (j = 0; j < route.sections.length; j++) {
    if (route.sections[j].journey == null) {
      if (j == route.sections.length - 1) {
        last = true;
      }
      var item = {
        typeStation: 'walk',
        trainName: null,
        stationName: route.sections[j].fromName,
        dstName: route.sections[j].toName,
        date: route.sections[j].fromDate,
        last: last,
        departureTime: route.sections[j].fromTime,
        walkingSeconds: route.sections[j].walk,
      };
      console.log(
        'j ' +
          route.sections[j].walk +
          ' ' +
          route.sections[j].fromDate +
          ' ' +
          route.sections[j].fromTime +
          ' ' +
          route.sections[j].toDate +
          ' ' +
          route.sections[j].toTime +
          ' ' +
          route.sections[j].fromName +
          ' ' +
          route.sections[j].toName,
      );
      data.push(item);
      continue;
    }
    for (i = 0; i < route.sections[j].journey.passList.length; i++) {
      if (i === 0) {
        type = 'departure';
        last = false;
        date = route.sections[j].journey.passList[i].departureDate;
        time = route.sections[j].journey.passList[i].departureTime;
      } else if (
        (j == route.sections.length - 1 &&
          i == route.sections[j].journey.passList.length - 1) ||
        (j != route.sections.length - 1 &&
          i == route.sections[j].journey.passList.length - 1)
      ) {
        type = 'arrival';
        date = route.sections[j].journey.passList[i].arrivalDate;
        time = route.sections[j].journey.passList[i].arrivalTime;
        if (
          j == route.sections.length - 1 &&
          i == route.sections[j].journey.passList.length - 1
        ) {
          last = true;
        }
      } else {
        type = 'intermediate';
        date = '';
        if (route.sections[j].journey.passList[i].arrivalTime != null) {
          time = route.sections[j].journey.passList[i].arrivalTime;
        } else if (
          route.sections[j].journey.passList[i].departureTime != null
        ) {
          time = route.sections[j].journey.passList[i].departureTime;
        } else {
          time = '?';
        }
        last = false;
      }
      // console.log(
      //     'i = ' + i + ' --- ' + route.sections[j].journey.passList.length,
      // );
      // console.log(route.sections[j].journey.passList[i]);
      var item = {
        typeStation: type,
        trainName: route.sections[j].journey.trainName,
        stationName: route.sections[j].journey.passList[i].stationName,
        date: date,
        last: last,
        departureTime: time,
      };
      console.log(item);
      data.push(item);
    }
  }
  return (
    <View style={styles.details_item}>
      <Summary
        departureName={context.from.name}
        arrivalName={context.to.name}
        departureTime={context.route.departureTime}
        arrivalTime={context.route.arrivalTime}
      />
      <FlatList
        data={data}
        keyExtractor={(item1, index) => index}
        renderItem={({item}) => <Station value={item} />}
      />
    </View>
  );
};

export default Stations;
