import React, {useContext, useEffect, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import styles from '../Styles';
import StationsCtx from '../context/StationsContext';

function Departure({stationName, timestamp, date}) {
  return (
    <View style={styles.departure}>
      <Text style={styles.label}>From</Text>
      <Text style={styles.nameStation}>{stationName}</Text>
      <Text style={styles.time}>{timestamp}</Text>
      <Text style={styles.date}>{date}</Text>
    </View>
  );
}

function Arrival({stationName, timestamp, date}) {
  return (
    <View style={styles.arrival}>
      <Text style={styles.label}>To</Text>
      <Text style={styles.nameStation}>{stationName}</Text>
      <Text style={styles.time}>{timestamp}</Text>
      <Text style={styles.date}>{date}</Text>
    </View>
  );
}

function Item({
  departureStation,
  arrivalStation,
  departureDate,
  arrivalDate,
  departureTime,
  arrivalTime,
  num_of_changes,
  navigation,
  item,
}) {
  var context = useContext(StationsCtx);
  return (
    <View>
      <TouchableOpacity
        style={styles.item}
        onPress={() => {
          context.resetRoute();
          console.log('ITEM ROUTE');
          console.log(item);
          var sections = [];

          for (var i = 0; i < item.sections.length; i++) {
            console.log('ITEM ROUTE ' + i);
            var passlist = [];
            var walk = -1;
            var journey = null;
            if (item.sections[i].journey == null) {
              walk = item.sections[i].walk.duration;
            } else if (item.sections[i].journey.passList != null) {
              for (
                var j = 0;
                j < item.sections[i].journey.passList.length;
                j++
              ) {
                console.log('ITEM ROUTE ' + i + ' ' + j);
                var depTime = null;
                var depDate = null;
                var arrTime = null;
                var arrDate = null;
                if (item.sections[i].journey.passList[j].departure != null) {
                  depDate = item.sections[i].journey.passList[
                    j
                  ].departure.split('T')[0];
                  depTime = item.sections[i].journey.passList[j].departure
                    .split('T')[1]
                    .split(':00+')[0];
                }
                if (item.sections[i].journey.passList[j].arrival != null) {
                  arrDate = item.sections[i].journey.passList[j].arrival.split(
                    'T',
                  )[0];
                  arrTime = item.sections[i].journey.passList[j].arrival
                    .split('T')[1]
                    .split(':00+')[0];
                }
                passlist[j] = {
                  stationName:
                    item.sections[i].journey.passList[j].station.name,
                  stationId: item.sections[i].journey.passList[j].station.id,
                  platform: item.sections[i].journey.passList[j].platform,
                  departureDate: depDate,
                  departureTime: depTime,
                  arrivalDate: arrDate,
                  arrivalTime: arrTime,
                };
              }
              journey = {
                  trainName: item.sections[i].journey.name,
                  trainCategory: item.sections[i].journey.category,
                  passList: passlist,
              }
            }
            sections[i] = {
              fromName: item.sections[i].departure.station.name,
              fromId: item.sections[i].departure.station.id,
              fromTime: item.sections[i].departure.departure
                .split('T')[1]
                .split(':00+')[0],
              fromDate: item.sections[i].departure.departure.split('T')[0],
              toName: item.sections[i].arrival.station.name,
              toId: item.sections[i].arrival.station.id,
              toTime: item.sections[i].arrival.arrival
                .split('T')[1]
                .split(':00+')[0],
              toDate: item.sections[i].arrival.arrival.split('T')[0],
              journey: journey,
              walk: walk,
            };
            console.log(sections[i]);
          }
          context.changeRoute({
            departureDate: departureDate,
            departureTime: departureTime,
            departurePlatform: item.from.platform,
            arrivalDate: arrivalDate,
            arrivalTime: arrivalTime,
            arrivalPlatform: item.to.platform,
            duration: item.duration,
            transfers: item.transfers,
            products: item.products,
            sections: sections,
          });
          console.log(context.route);
          console.log(context.route.sections[0].journey);
          navigation.navigate('details');
        }}>
        <Departure
          stationName={departureStation}
          timestamp={departureTime}
          date={departureDate}
        />
        <View style={styles.changes}>
          <Image style={styles.logo} source={require('../arrow.png')} />
          <Text style={{fontSize: 12}}>Num. of Changes: {num_of_changes}</Text>
        </View>
        <Arrival
          stationName={arrivalStation}
          timestamp={arrivalTime}
          date={arrivalDate}
        />
      </TouchableOpacity>
    </View>
  );
}

const API = 'http://transport.opendata.ch/v1/connections?';
const Routes = (props) => {
  const context = useContext(StationsCtx);
  const [dataSource, setDataSource] = useState([]);
  const [departureStation, setDepartureStation] = useState(context.from.name);
  const [arrivalStation, setArrivalStation] = useState(context.to.name);
  const [dateTime, setDateTime] = useState(context.date + ' ' + context.time);
  const [isLoading, setIsLoading] = useState(true);
  const navigation = props.navigation;
  const queryTravel = () => {
    console.log(
      API +
        'from=%3C' +
        departureStation +
        '%3E&to=%3C' +
        arrivalStation +
        '%3E&datetime=' +
        dateTime,
    );
    fetch(
      API +
        'from=%3C' +
        departureStation +
        '%3E&to=%3C' +
        arrivalStation +
        '%3E&datetime=' +
        dateTime,
    )
      .then((res) => res.json())
      .then((json) => {
        if (json.connections.length == 0) {
          alert('No solution found');
          setIsLoading(false);
        } else {
          setDataSource(json.connections);
          setIsLoading(false);
        }
      })
      .catch((reason) => alert(reason.toString()));
  };
  useEffect(() => {
    queryTravel();
  }, []);

  return isLoading ? (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <ActivityIndicator size={'large'} />
    </View>
  ) : (
    <View>
      <FlatList
        data={dataSource}
        keyExtractor={(item, index) =>
          item.from.departure + '_' + item.to.arrival
        }
        renderItem={({item}) => (
          <Item
            departureStation={item.from.station.name}
            arrivalStation={item.to.station.name}
            departureDate={item.from.departure.split('T')[0]}
            arrivalDate={item.to.arrival.split('T')[0]}
            departureTime={item.from.departure.split('T')[1].split(':00+')[0]}
            arrivalTime={item.to.arrival.split('T')[1].split(':00+')[0]}
            num_of_changes={item.transfers}
            navigation={navigation}
            item={item}
          />
        )}
      />
    </View>
  );
};

export default Routes;
