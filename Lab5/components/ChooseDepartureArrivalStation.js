import React, {useContext, useState} from 'react';
import {
  View,
  Text,
  Button,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import styles from '../Styles';
import Autocomplete from 'react-native-autocomplete-input';
import StationsCtx from '../context/StationsContext';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Moment from 'moment';

const API = 'http://transport.opendata.ch/v1/locations?type=station&query=';

const AutocompleteStations = (props) => {
  const context = useContext(StationsCtx);
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isTimePickerVisible, setTimePickerVisibility] = useState(false);
  const [stations, setStations] = useState([]);
  const [arrivalStations, setArrivalStations] = useState([]);
  const [query, setQuery] = useState('');
  const [arrivalQuery, setArrivalQuery] = useState('');
  const [hideDropdown, setHide] = useState(true);
  const [hideArrivalDropdown, setArrivalHide] = useState(false);
  const navigation = props.navigation;

  const findStations = (text, arrival = false) => {
    if (arrival) {
      if (context.to.id !== -1) {
        context.resetStationTo();
      }
      if (text === '') {
        setArrivalHide(true);
        return [];
      }
    } else {
      if (context.from.id !== -1) {
        context.resetFrom();
      }
      if (text === '') {
        setHide(true);
        return [];
      }
    }

    fetch(API + text)
      .then((res) => res.json())
      .then((json) => {
        console.log(API + query + '----' + json.stations.length);

        console.log(json.stations.length);
        var filtered = json.stations.filter((item) => {
          return item.id !== null;
        });
        console.log(filtered.length);
        if (arrival) {
          setArrivalStations(json.stations);
        } else {
          setStations(json.stations);
        }
      })
      .catch((reason) => alert(reason.toString()));
    console.log('findstations3');
  };

  const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();

  console.log(context.from.name + ' ---- ' + context.to.name);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };
  const showTimePicker = () => {
    setTimePickerVisibility(true);
  };
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };
  const hideTimePicker = () => {
    setTimePickerVisibility(false);
  };
  const handleDateConfirm = (date: Date) => {
    hideDatePicker();
    context.changeDate(Moment(date).format('YYYY-MM-DD'));
    console.log('Date: ', context.date, ' _Time: ', context.time);
  };
  const handleTimeConfirm = (date: Date) => {
    hideTimePicker();
    context.changeTime(Moment(date).format('HH:mm'));
    console.log('Date: ', context.date, ' _Time: ', context.time);
  };

  function invalidDateTime(): boolean {
    return Moment(context.date + ' ' + context.time).isBefore(
      Moment(new Date()).subtract(1, 'minutes'),
    );
  }

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        setArrivalHide(true);
        setHide(true);
      }}>
      <View style={styles.container}>
        <View style={{flex: 2, justifyContent: 'space-between'}}>
          <View style={styles.autocompleteContainer}>
            <Autocomplete
              autoCapitalize="none"
              autoCorrect={false}
              // containerStyle={styles.autocompleteContainer}
              //data to show in suggestion
              data={
                stations.length === 1 && comp(query, stations[0].name)
                  ? []
                  : stations
              }
              keyExtractor={(item, index) => item.name + item.id}
              //default value if you want to set something in input
              defaultValue={query}
              hideResults={hideDropdown}
              /*onchange of the text changing the state of the query which will trigger
                                            the findFilm method to show the suggestions*/
              onChangeText={(text) => {
                setQuery(text);
                setHide(false);
                //this.setState({query: text});
                findStations(text);
              }}
              placeholder="Enter the departure station name"
              renderItem={({item}) => (
                //you can change the view you want to show in suggestion from here
                <TouchableOpacity
                  key={item.id}
                  onPress={() => {
                    //this.setState({query: item.name});
                    setQuery(item.name);
                    context.changeFrom(item);
                    setHide(true);
                  }}>
                  <Text style={styles.itemText}>{item.name}</Text>
                </TouchableOpacity>
              )}
            />
          </View>
          <View style={styles.autocompleteContainer2}>
            <Autocomplete
              autoCapitalize="none"
              autoCorrect={false}
              // containerStyle={styles.autocompleteContainer2}
              //data to show in suggestion
              data={
                arrivalStations.length === 1 &&
                comp(arrivalQuery, arrivalStations[0].name)
                  ? []
                  : arrivalStations
              }
              keyExtractor={(item, index) => item.name + item.id}
              //default value if you want to set something in input
              defaultValue={arrivalQuery}
              hideResults={hideArrivalDropdown}
              /*onchange of the text changing the state of the query which will trigger
                                            the findFilm method to show the suggestions*/
              onChangeText={(text) => {
                setArrivalQuery(text);
                setArrivalHide(false);
                //this.setState({query: text});
                findStations(text, true);
              }}
              placeholder="Enter the arrival station name"
              renderItem={({item}) => (
                //you can change the view you want to show in suggestion from here
                <TouchableOpacity
                  key={item.id}
                  onPress={() => {
                    //this.setState({query: item.name});
                    setArrivalQuery(item.name);
                    context.changeTo(item);
                    setArrivalHide(true);
                  }}>
                  <Text style={styles.itemText}>{item.name}</Text>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>
        <View style={{flex: 3, justifyContent: 'space-around', zIndex: 1}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <TouchableOpacity onPress={showDatePicker}>
              <Text style={styles.bordered}>Date: {context.date}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={showTimePicker}>
              <Text style={styles.bordered}>Time: {context.time}</Text>
            </TouchableOpacity>
          </View>
          <DateTimePickerModal
            isVisible={isDatePickerVisible}
            mode="date"
            onConfirm={handleDateConfirm}
            onCancel={hideDatePicker}
            date={new Date()}
            minimumDate={new Date()}
          />
          <DateTimePickerModal
            isVisible={isTimePickerVisible}
            mode="time"
            onConfirm={handleTimeConfirm}
            onCancel={hideTimePicker}
            date={new Date()}
            is24Hour={true}
          />
          <Button
            color={'#673AB7'}
            onPress={() => {
              if (context.from.id === -1 || context.to.id === -1) {
                alert(
                  'Please select a departure and an arrival station before navigating to the next page.',
                );
              } else if (invalidDateTime()) {
                alert(
                  'Your date/time is in the past. Please select a date/time in the future.',
                );
              } else {
                navigation.navigate('routes');
              }
            }}
            title={'Show Routes'}
          />
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default AutocompleteStations;
