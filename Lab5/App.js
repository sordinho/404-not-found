/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

import AutocompleteStations from './components/ChooseDepartureArrivalStation';
import Routes from './components/Routes';
import DetailsTravel from './components/DetailsTravel';
import ContextProvider from './context/ContextProvider';
const StationsCtx = React.createContext({from: {}, to: {}});
const App: () => React$Node = () => {
  return (
    <NavigationContainer>
      <ContextProvider>
        <Stack.Navigator initialRouteName={'choose'}>
          <Stack.Screen name={'choose'} component={AutocompleteStations} options={{title: 'Choose your journey'}}/>
          <Stack.Screen name={'routes'} component={Routes}  options={{title: 'Select your route'}}/>
          <Stack.Screen name={'details'} component={DetailsTravel}  options={{title: 'Route details'}}/>
        </Stack.Navigator>
      </ContextProvider>
    </NavigationContainer>
  );
};

export default App;
