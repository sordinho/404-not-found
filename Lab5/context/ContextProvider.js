import StationsCtx from './StationsContext';
import React, {useState} from 'react';
import Moment from 'moment';

const ContextProvider = ({children}) => {
  const [from, setFrom] = useState({
    id: -1,
    name: 'name',
    score: null,
    coordinate: {type: 'WGS84', x: 0.0, y: 0.0},
    distance: null,
    icon: 'train',
  });
  const [to, setTo] = useState({
    id: -1,
    name: 'name',
    score: null,
    coordinate: {type: 'WGS84', x: 0.0, y: 0.0},
    distance: null,
    icon: 'train',
  });
  const defaultRoute = {
    departureDate: '00/00/0000',
    departureTime: '00:00:00',
    departurePlatform: '0',
    arrivalDate: '00/00/0000',
    arrivalTime: '00:00:00',
    arrivalPlatform: '0',
    duration: '00d00:00:00',
    transfers: 0,
    products: ['IC 21', 'IC 5'],
    sections: [
      {
        fromName: 'name',
        fromId: -1,
        fromTime: '00:00:00',
        fromDate: '00/00/0000',
        toName: 'name',
        toId: -1,
        toTime: '00:00:00',
        toDate: '00/00/0000',
        journey: {
          trainName: 'IC 00',
          trainCategory: 'IC',
          passList: [
            {
              stationName: 'name',
              stationId: -1,
              platform: -1,
              departureDate: '00/00/0000',
              departureTime: '00:00:00',
              arrivalDate: '00/00/0000',
              arrivalTime: '00:00:00',
            },
            {
              stationName: 'name',
              stationId: -1,
              platform: -1,
              departureDate: '00/00/0000',
              departureTime: '00:00:00',
              arrivalDate: '00/00/0000',
              arrivalTime: '00:00:00',
            },
            {
              stationName: 'name',
              stationId: -1,
              platform: -1,
              departureDate: '00/00/0000',
              departureTime: '00:00:00',
              arrivalDate: '00/00/0000',
              arrivalTime: '00:00:00',
            },
          ],
        },
        walk: 0,
      },
      {
        fromName: 'name',
        fromId: -1,
        fromTime: '00:00:00',
        fromDate: '00/00/0000',
        toName: 'name',
        toId: -1,
        toTime: '00:00:00',
        toDate: '00/00/0000',
        journey: {
          trainName: 'IC 00',
          trainCategory: 'IC',
          passList: [
            {
              stationName: 'name',
              stationId: -1,
              platform: -1,
              departureDate: '00/00/0000',
              departureTime: '00:00:00',
              arrivalDate: '00/00/0000',
              arrivalTime: '00:00:00',
            },
            {
              stationName: 'name',
              stationId: -1,
              platform: -1,
              departureDate: '00/00/0000',
              departureTime: '00:00:00',
              arrivalDate: '00/00/0000',
              arrivalTime: '00:00:00',
            },
            {
              stationName: 'name',
              stationId: -1,
              platform: -1,
              departureDate: '00/00/0000',
              departureTime: '00:00:00',
              arrivalDate: '00/00/0000',
              arrivalTime: '00:00:00',
            },
          ],
        },
        walk: 0,
      },
    ],
  };
  const [date, setDate] = useState(Moment(Date.now()).format('YYYY-MM-DD'));
  const [time, setTime] = useState(Moment(Date.now()).format('HH:mm'));
  const [route, setRoute] = useState(defaultRoute);
  const resetRoute = () => {
    setRoute(defaultRoute);
  }
  const changeFrom = (obj) => {
    setFrom(obj);
  };
  const changeTo = (obj) => {
    setTo(obj);
  };
  const changeDate = (obj) => {
    setDate(obj);
  };
  const changeTime = (obj) => {
    setTime(obj);
  };
  const changeRoute = (obj) => {
    setRoute(obj);
  };
  const resetFrom = () => {
    setFrom({
      id: -1,
      name: 'name',
      score: null,
      coordinate: {type: 'WGS84', x: 0.0, y: 0.0},
      distance: null,
      icon: 'train',
    });
  };
  const resetStationTo = () => {
    setTo({
      id: -1,
      name: 'name',
      score: null,
      coordinate: {type: 'WGS84', x: 0.0, y: 0.0},
      distance: null,
      icon: 'train',
    });
  };
  return (
    <StationsCtx.Provider
      value={{
        from,
        to,
        date,
        time,
        route,
        changeDate,
        changeTime,
        changeFrom,
        resetFrom,
        changeTo,
        resetStationTo,
        changeRoute,
        resetRoute,
      }}>
      {children}
    </StationsCtx.Provider>
  );
};



export default ContextProvider;
