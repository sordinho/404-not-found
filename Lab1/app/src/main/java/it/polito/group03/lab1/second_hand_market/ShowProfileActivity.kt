package it.polito.group03.lab1.second_hand_market


import android.app.Activity
import android.content.Intent
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import it.polito.group03.lab1.second_hand_market.exceptions.IDException
import kotlinx.android.synthetic.main.activity_show_profile.*
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.net.URI

class ShowProfileActivity : AppCompatActivity() {

    private val LAUNCH_EDIT_PROFILE_CODE = 3
    private val INTENT_EXTRA_FULLNAME = "group03.lab1.second_hand_market.fullname"
    private val INTENT_EXTRA_NICKNAME = "group03.lab1.second_hand_market.nickname"
    private val INTENT_EXTRA_EMAIL = "group03.lab1.second_hand_market.email"
    private val INTENT_EXTRA_LOCATION = "group03.lab1.second_hand_market.location"
    private val INTENT_EXTRA_USER_AVATAR = "group03.lab1.second_hand_market.userAvatar"
    private val NO_CONTENT_STR = "Empty Field"
    private lateinit var myActivityMenuItem: MenuItem

    private var imgUri: Uri? = null
    var currentPhotoPath: String = ""  // The one to be used when accessing the userAvatar image from URI

    /**
     * To be invoked when the pencil button is pressed. Inside this method an explicit intent
     * targeting EditProfileActivity is created.
     * Items naming convention : group03.lab1.second_hand_market.ITEM_NAME
     *
     * EditProfileActivity will be launched.
     */
    private fun editProfile() {
        val intent = Intent(this, EditProfileActivity::class.java)
            .apply {
                //action = Intent.ACTION_SEND
                if (imgUri != null) {
                    putExtra(INTENT_EXTRA_USER_AVATAR, imgUri!!.path)
                }
                putExtra(INTENT_EXTRA_FULLNAME, fullname.text.toString())
                putExtra(INTENT_EXTRA_NICKNAME, nickname.text.toString())
                putExtra(INTENT_EXTRA_EMAIL, email.text.toString())
                putExtra(INTENT_EXTRA_LOCATION, location.text.toString())
            }

        startActivityForResult(intent, LAUNCH_EDIT_PROFILE_CODE);
    }

    /**
     * This function fill the given field with the content of an intent.
     */
    @Throws(IDException::class)
    private fun fillFields(name: String, fieldID: Int, data: Intent) {
        val field: TextView = findViewById(fieldID) ?: throw IDException("Non existing id")
        if (data.hasExtra(name))
            field.text = data.getStringExtra(name)
        else {
            field.text = ""
            field.hint = NO_CONTENT_STR
        }
    }

    /**
     * This function returns the URI of the user profile custom image (if present).
     */
    @Throws(IOException::class)
    private fun getImageUri(): Uri? {
        /* Create an image file name */
        val filename = "userImage.jpg"
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val file = File(
            storageDir, filename
        ).apply {
            /* Save a file: path for use with ACTION_VIEW intents */
            currentPhotoPath = absolutePath
        }
        if (file.exists())
            return Uri.fromFile(file)
        else
            return null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_profile)

        // Get shared preferences
        val sharedPref =
            this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        val defaultProfileString: String =
            getString(R.string.defaultUserInfo) // Get default string in JSON form
        val profileString: String = sharedPref.getString("profile", defaultProfileString).toString()
        val profileJSON: JSONObject = JSONObject(profileString)

        // Set values in activity when applicaiton starts
        fullname.text = profileJSON.getString("fullName")
        fullname.hint = NO_CONTENT_STR
        nickname.text = profileJSON.getString("nickName")
        nickname.hint = NO_CONTENT_STR
        location.text = profileJSON.getString("location")
        location.hint = NO_CONTENT_STR
        email.text = profileJSON.getString("email")
        email.hint = NO_CONTENT_STR
        val tmpImgUri: Uri? = getImageUri()
        if (tmpImgUri != null) {
            imgUri = tmpImgUri
            userAvatar.setImageURI(imgUri)
            userAvatar.tag = imgUri!!.path
        }
    }

    /**
     * Inflate the menu when created
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    /**
     * Check which menu item is pressed and start corresponding task
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.edit_account -> {
                myActivityMenuItem = item
                item.isEnabled = false
                editProfile()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Callback function that handles the arrival of an intent as result (After calling startActivityForResult)
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        myActivityMenuItem.isEnabled = true

        if (requestCode == LAUNCH_EDIT_PROFILE_CODE && resultCode == Activity.RESULT_OK && data != null) {
            try {
                fillFields(INTENT_EXTRA_FULLNAME, R.id.fullname, data)
                fillFields(INTENT_EXTRA_NICKNAME, R.id.nickname, data)
                fillFields(INTENT_EXTRA_EMAIL, R.id.email, data)
                fillFields(INTENT_EXTRA_LOCATION, R.id.location, data)

                val userAvatar: ImageView = findViewById(R.id.userAvatar)
                if (data.hasExtra(INTENT_EXTRA_USER_AVATAR)) {
                    val newURI = Uri.parse(data.getStringExtra(INTENT_EXTRA_USER_AVATAR))
                    userAvatar.setImageURI(Uri.parse(""))
                    userAvatar.setImageURI(newURI)
                    imgUri = newURI
                    currentPhotoPath = imgUri!!.path.toString()
                }
                // Save data to sharedPreferences for persistency
                val dataToSaveJSON: JSONObject = JSONObject()
                dataToSaveJSON.put("fullName", fullname.text)
                dataToSaveJSON.put("nickName", nickname.text)
                dataToSaveJSON.put("location", location.text)
                dataToSaveJSON.put("email", email.text)
                val sharedPref = this.getSharedPreferences(
                    getString(R.string.preference_file_key),
                    Context.MODE_PRIVATE
                )
                sharedPref.edit().putString("profile", dataToSaveJSON.toString()).apply()
            } catch ( e: IDException){
                val alertDialog = AlertDialog.Builder(this)
                alertDialog.setMessage("Errore nell'aggiornamento del profilo utente.\nTutte le modifiche andranno perse.")
                alertDialog.setPositiveButton("Ok"){
                        dialog,which -> recreate()
                }
                alertDialog.setCancelable(false)
                alertDialog.show()
            }

        }
    }
}
