package it.polito.group03.lab1.second_hand_market

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.PersistableBundle
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import java.io.*
import kotlinx.android.synthetic.main.activity_edit_profile.*;

class EditProfileActivity : AppCompatActivity() {
    val REQUEST_IMAGE_CAPTURE = 1
    val REQUEST_IMAGE_GALLERY = 2
    val INTENT_EXTRA_FULLNAME = "group03.lab1.second_hand_market.fullname"
    val INTENT_EXTRA_NICKNAME = "group03.lab1.second_hand_market.nickname"
    val INTENT_EXTRA_EMAIL = "group03.lab1.second_hand_market.email"
    val INTENT_EXTRA_LOCATION = "group03.lab1.second_hand_market.location"
    val INTENT_EXTRA_USER_AVATAR = "group03.lab1.second_hand_market.userAvatar"
    private val NO_CONTENT_STR = "Empty Field"

    private var imgUri: Uri? = null
    var currentPhotoPath: String = ""// to be used when setting the userAvatar URI

    /**
     * Private function to update the content of the EditText fields with the content of an intent.
     */
    private fun fillFields(name: String, field: EditText) {
        if (intent.hasExtra(name))
            field.setText(intent.getStringExtra(name))
        else {
            field.setHint(NO_CONTENT_STR)
            field.setText("")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        /* creates the popup menu that asks the user whether pick a picture from gallery or take a photo*/
        edit_profile_picture.setOnClickListener {

            val popupMenu: PopupMenu = PopupMenu(this, edit_profile_picture)
            popupMenu.menuInflater.inflate(R.menu.popup_menu_edit_profile_picture, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.action_edit_picture_photo -> {
                        takePictureFromCamera()
                    }
                    R.id.action_edit_picture_gallery -> {
                        takePictureFromGallery()
                    }
                }
                true
            }
            popupMenu.show()

        }

        if ( intent != null) {
            fillFields(INTENT_EXTRA_FULLNAME, editText_edit_fullname)
            fillFields(INTENT_EXTRA_NICKNAME, editText_edit_nickname)
            fillFields(INTENT_EXTRA_EMAIL, editText_edit_email)
            fillFields(INTENT_EXTRA_LOCATION, editText_edit_location)
            if (intent.hasExtra(INTENT_EXTRA_USER_AVATAR)) {
                editImage_user_avatar.setImageURI(
                    Uri.parse(
                        intent.getStringExtra(INTENT_EXTRA_USER_AVATAR)
                    )
                )
                currentPhotoPath = intent.getStringExtra(INTENT_EXTRA_USER_AVATAR)!!
                imgUri = Uri.parse(currentPhotoPath)
            }
        }
    }

    /**
     * Select a picture from the phone's gallery
     */
    private fun takePictureFromGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(
            Intent.createChooser(intent, ""),
            REQUEST_IMAGE_GALLERY
        )
    }

    /**
     * Take a picture using camera Intent
     */
    private fun takePictureFromCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            /* Ensure that there's a camera activity to handle the intent */
            takePictureIntent.resolveActivity(packageManager)?.also {
                /* Create the File where the photo should go */
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    /* Error occurred while creating the File */
                    Log.d("Error", "Error occurred while creating the File")
                    null
                }
                /* Continue only if the File was successfully created */
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        applicationContext.packageName
                            .toString() + ".fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(
                        MediaStore.EXTRA_OUTPUT,
                        photoURI
                    )
                    imgUri = photoURI
                    startActivityForResult(
                        takePictureIntent,
                        REQUEST_IMAGE_CAPTURE
                    )
                }
            }
        }
    }

    /**
     * This method creates a file with a constant name (userImage.jpg) stored in the application directory
     */
    @Throws(IOException::class)
    private fun createImageFile(): File {
        /* Create an image file name */
        val filename = "userImage.jpg"
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File(storageDir, filename).apply {
            /* Save a file: path for use with ACTION_VIEW intents */
            currentPhotoPath = absolutePath
        }
    }

    /**
     * Inflate the menu when created
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.save, menu)
        return true
    }

    /**
     * Check which menu item is pressed and start corresponding task
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.edit_account -> {
                true
            }
            R.id.save_account -> {
                val intent = Intent(this, ShowProfileActivity::class.java)
                    .apply {
                        if (currentPhotoPath.isNotEmpty()) {
                            putExtra(
                                INTENT_EXTRA_USER_AVATAR,
                                currentPhotoPath
                            )
                        }
                        if (editText_edit_fullname.text.toString().isNotEmpty())
                            putExtra(
                                INTENT_EXTRA_FULLNAME,
                                editText_edit_fullname.text.toString()
                            )
                        if (editText_edit_nickname.text.toString().isNotEmpty())
                            putExtra(
                                INTENT_EXTRA_NICKNAME,
                                editText_edit_nickname.text.toString()
                            )
                        if (editText_edit_email.text.toString().isNotEmpty())
                            putExtra(
                                INTENT_EXTRA_EMAIL,
                                editText_edit_email.text.toString()
                            )
                        if (editText_edit_location.text.toString().isNotEmpty())
                            putExtra(
                                INTENT_EXTRA_LOCATION,
                                editText_edit_location.text.toString()
                            )
                    }
                setResult(Activity.RESULT_OK, intent)
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Callback function to keep the state of the activity when rotating it (for example)
     */
    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        outState.putString("locationText", editText_edit_location.toString())
        outState.putString("nameText", editText_edit_fullname.toString())
        outState.putString("emailText", editText_edit_email.toString())
        outState.putString("nicknameText", editText_edit_nickname.toString())
        super.onSaveInstanceState(outState, outPersistentState)
    }

    /**
     * Callback function to restore the state of the activity after rotating it (for example)
     */
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        editText_edit_location.setText(savedInstanceState.getString("locationText"))
        editText_edit_fullname.setText(savedInstanceState.getString("nameText"))
        editText_edit_nickname.setText(savedInstanceState.getString("nicknameText"))
        editText_edit_email.setText(savedInstanceState.getString("emailText"))
        super.onRestoreInstanceState(savedInstanceState)
    }

    /**
     * Callback function that handles the arrival of an intent as result (After calling startActivityForResult)
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            if (imgUri == null) {
                Log.d("", "URI is null")
                return
            }
            val uri: Uri = imgUri!!

            val ins: InputStream? =
                contentResolver?.openInputStream(uri)/* loading the bitmap */
            var img: Bitmap? = BitmapFactory.decodeStream(ins)
            ins?.close()
            if (img != null) {
                img = handleRotation(currentPhotoPath, img)/* manages the rotation */
                findViewById<ImageView>(R.id.editImage_user_avatar).setImageBitmap(img)
            } else {
                Toast.makeText(this, "Error while managing the photo!", Toast.LENGTH_SHORT)
                    .show();
            }
        } else if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == Activity.RESULT_OK && data != null) {
            val ins: InputStream? =
                contentResolver?.openInputStream(data.getData()!!)             /* loading the bitmap */
            var img: Bitmap? = BitmapFactory.decodeStream(ins)
            bitmapToFile(img!!)                        /* store the image in application directory */
            findViewById<ImageView>(R.id.editImage_user_avatar).setImageBitmap(img)
        } else {
            currentPhotoPath = ""
        }
    }


    /**
     * This method checks the angle of image's rotation. If the image has a angle != 0, the method calls rotateImage method.
     */
    private fun handleRotation(path: String, bitmap: Bitmap): Bitmap? {
        val ei = ExifInterface(path)

        val orientation: Int = ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )

        var rotatedBitmap: Bitmap?
        rotatedBitmap = when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap, 90 as Float)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap, 180 as Float)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap, 270 as Float)
            ExifInterface.ORIENTATION_NORMAL -> bitmap
            else -> bitmap
        }
        return rotatedBitmap
    }

    /**
     * This method rotates the image by a preset amount of degrees.
     */
    private fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }

    /**
     * This method allows you to store an image on disk given a bitmap.
     * The name of the image is in accordance with the name given by the createImageFile method.
     */
    private fun bitmapToFile(bitmap: Bitmap) {
        try {
            // Initialize a new file instance to save bitmap object
            val file: File = createImageFile()
            // Compress the bitmap and save in jpg format
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}

