package it.polito.group03.lab2.second_hand_market.ui.user_advertisements

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import it.polito.group03.lab2.second_hand_market.R
import it.polito.group03.lab2.second_hand_market.model.Advertisement

class AdvertisementAdapter(
    var advertisements: ArrayList<Advertisement>,
    var navController: NavController
): RecyclerView.Adapter<AdvertisementAdapter.ViewHolder>() {

    class ViewHolder(v: View, var navController: NavController): RecyclerView.ViewHolder(v) {
        val cardView: CardView = v as CardView
        val title: TextView = v.findViewById(R.id.title)
        val price: TextView = v.findViewById(R.id.price)
        val description: TextView = v.findViewById(R.id.description)
        val subCategory: TextView = v.findViewById(R.id.subCategory)
        val item_image: ImageView = v.findViewById(R.id.item_image)
        val edit_button: Button = v.findViewById(R.id.edit_button)

        fun bind(advertisement: Advertisement, position: Int) {
            title.text = advertisement.title
            price.text = "" + advertisement.price + "€"
            description.text = advertisement.description
            subCategory.text = advertisement.subCategory
            item_image.setImageURI(Uri.parse(advertisement.thumbnailPath))

            cardView.setOnClickListener{
                val bundle = Bundle()
                bundle.putParcelable("item",advertisement)
                bundle.putInt("position", position)
                navController.navigate(R.id.action_nav_advertisements_to_itemDetailFragment,bundle)
            }
            edit_button.setOnClickListener{
                val bundle = Bundle()
                bundle.putParcelable("item", advertisement)
                bundle.putInt("position", position)
                this.navController.navigate(R.id.action_nav_slideshow_to_itemEditFragment, bundle)
            }
        }

        fun unbind() {
            cardView.setOnClickListener(null)
            edit_button.setOnClickListener(null)
        }
    }

    /**
     * it inflates the layout and returnes the ViewHolder object.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.advertisement_item, parent, false)
        return ViewHolder(v, this.navController)
    }

    /**
     * Retriving the size of list.
     */
    override fun getItemCount(): Int = advertisements.size

    /**
     * It permittes to bind the ViewHolder object with an specific object in the advertisement list.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(advertisements[position], position)
    }

    /**
     * This method is called when an item is recycled, we can perform an unbind method of ViewHolder.
     */
    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        holder.unbind()
    }

    /**
     * This method perform an update of entire list.
     */
    fun updateData(advertisementsUpdated: ArrayList<Advertisement>) {
        this.advertisements = advertisementsUpdated
        notifyDataSetChanged()
    }
}