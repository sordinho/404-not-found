package it.polito.group03.lab2.second_hand_market

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.widget.ImageView
import android.widget.TextView
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
            R.id.nav_advertisements, R.id.nav_user_profile), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        val sharedP = getSharedPreferences(
                getString(R.string.preference_file_key),
                Context.MODE_PRIVATE
            )
        if(sharedP != null) {
            val defaultProfileString: String = getString(R.string.default_user_info)
            val profileString = sharedP.getString("profile", defaultProfileString)?.toString() ?: ""
            val profileJSON: JSONObject = JSONObject(profileString)
            var img : Uri? =  null
            if(profileJSON.has("avatar") && profileJSON.getString("avatar").isNotEmpty()){
                img = Uri.parse( profileJSON.getString("avatar"))
            }

            val navAvatar: ImageView? = navView
                .getHeaderView(0)
                .findViewById(R.id.header_avatar)
            if (navAvatar != null && img != null) {
                navAvatar.setImageURI(img)
                navAvatar.tag = profileJSON.getString("avatar") ?: ""
            }

            val navNick: TextView? = navView
                .getHeaderView(0)
                .findViewById(R.id.header_nickname)
            if (navNick != null && profileJSON.has("nickname")) {
                navNick.text = profileJSON.getString("nickname") ?: ""
            }

            val navEmail: TextView? = navView
                .getHeaderView(0)
                .findViewById(R.id.header_email)
            if (navEmail != null && profileJSON.has("email")) {
                navEmail.text = profileJSON.getString("email") ?: ""
            }
        }
    }

    fun setActionBarTitle(title: String?) {
        getSupportActionBar()?.setTitle(title)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}
