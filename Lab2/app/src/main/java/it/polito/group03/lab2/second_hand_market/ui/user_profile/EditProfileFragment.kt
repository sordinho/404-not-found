package it.polito.group03.lab2.second_hand_market.ui.user_profile

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.snackbar.Snackbar
import com.theartofdev.edmodo.cropper.CropImage
import it.polito.group03.lab2.second_hand_market.R
import it.polito.group03.lab2.second_hand_market.model.UserProfile
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import java.io.*
import it.polito.group03.lab2.second_hand_market.utils.*
import kotlinx.android.synthetic.main.content_main.*
import org.json.JSONObject

class EditProfileFragment : Fragment() {
    private lateinit var myActivityMenuItem: MenuItem
    private val PROFILE_BUNDLE_KEY = "it.polito.group03.lab2.second_hand_market.profileInfo"
    private var currentPhotoPath: String = ""
    private lateinit var cameraGallery : CameraGallery

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_edit_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        edit_profile_picture.setOnClickListener {
            cameraGallery = CameraGallery(
                edit_profile_picture,
                requireContext(),
                this
            )
        }
        if(savedInstanceState != null)
            fillFieldsFromBundle(savedInstanceState)
        else if(arguments != null)
            fillFieldsFromBundle(arguments)
    }

    /**
     * Validate fields and eventually display error strings. This makes sure that no fields are empty
     * and validates the format of the email.
     */
    private fun validateFields() : Boolean {
        val inputEmail = editText_edit_email.editText?.text.toString().trim()
        val inputName = editText_edit_fullname.editText?.text.toString().trim()
        val inputNickname = editText_edit_nickname.editText?.text.toString().trim()
        val inputLocation = editText_edit_location.editText?.text.toString().trim()
        var retVal = true
        if(inputEmail.isEmpty()){
            editText_edit_email.error = "This field can't be empty"
            retVal = false
        } else if(!Patterns.EMAIL_ADDRESS.matcher(inputEmail).matches()){
            editText_edit_email.error = "This should be a valid email address"
            retVal = false
        } else {
            editText_edit_email.error = null
        }
        if(inputName.isEmpty()){
            editText_edit_fullname.error = "This field can't be empty"
            retVal = false
        } else {
            editText_edit_fullname.error = null
        }
        if(inputNickname.isEmpty()){
            editText_edit_nickname.error = "This field can't be empty"
            retVal = false
        } else {
            editText_edit_nickname.error = null
        }
        if(inputLocation.isEmpty()) {
            editText_edit_location.error = "This field can't be empty"
            retVal = false
        } else {
            editText_edit_location.error = null
        }
        return retVal
    }

    /**
     * Fill fields from received bundle
     */
    private fun fillFieldsFromBundle(arguments: Bundle?) {
        val profile: UserProfile? = arguments?.getParcelable(PROFILE_BUNDLE_KEY)
        if (profile != null) {
            if (profile.path.isNotEmpty()) {
                val img = Uri.parse(profile.path)
                if(img != null) {
                    editImage_user_avatar.setImageURI(img)
                    editImage_user_avatar.tag = profile.path
                    currentPhotoPath = profile.path
                }
            }
            editText_edit_fullname.editText?.setText(profile.fullname)
            editText_edit_nickname.editText?.setText(profile.nickname)
            editText_edit_email.editText?.setText(profile.email)
            editText_edit_location.editText?.setText(profile.location)
        }
    }

    /**
     * Inflate the menu when created
     */
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        //hide settings menu item

        val item = menu.findItem(R.id.action_settings)
        if (item.isEnabled && item.isVisible) {
            item.isEnabled = false
            item.isVisible = false
        }
        inflater.inflate(R.menu.profile_save, menu)
    }

    /**
     * Check which menu item is pressed and start corresponding task
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.nav_user_profile -> {
                if(!validateFields())
                    false
                else {
                    myActivityMenuItem = item
                    item.isEnabled = false
                    val dataToSaveJSON = JSONObject()
                    dataToSaveJSON.put("avatar", editImage_user_avatar.tag.toString() ?: "")
                    dataToSaveJSON.put("fullname", editText_edit_fullname.editText?.text ?: "")
                    dataToSaveJSON.put("nickname", editText_edit_nickname.editText?.text ?: "")
                    dataToSaveJSON.put("location", editText_edit_location.editText?.text ?: "")
                    dataToSaveJSON.put("email", editText_edit_email.editText?.text ?: "")
                    val sharedPref = activity?.getSharedPreferences(
                        getString(R.string.preference_file_key),
                        Context.MODE_PRIVATE
                    )
                    if (sharedPref != null) {
                        sharedPref
                            .edit()
                            .putString("profile", dataToSaveJSON.toString())
                            .apply()
                    } else
                        Log.d("DEBUG", "SHARED PREFERENCES == NULL")
                    NavHostFragment.findNavController(nav_host_fragment)
                        .navigate(R.id.action_nav_user_edit_profile_to_nav_user_profile)
                    true
                }
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Callback function to keep the state of the activity when rotating it (for example)
     */
    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("avatarImage",editImage_user_avatar.tag?.toString() ?: "")
        outState.putString("locationText", editText_edit_location.editText?.text?.toString() ?: "")
        outState.putString("nameText", editText_edit_fullname.editText?.text?.toString() ?: "")
        outState.putString("emailText", editText_edit_email.editText?.text?.toString() ?: "")
        outState.putString("nicknameText", editText_edit_nickname.editText?.text?.toString() ?: "")
        super.onSaveInstanceState(outState)
    }

    /**
     * Callback function to restore the state of the activity after rotating it (for example)
     */
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        if (savedInstanceState != null) {
            val path = savedInstanceState.getString("avatarImage")
            if(!path.isNullOrEmpty()) {
                editImage_user_avatar.setImageURI(Uri.parse(path))
                editImage_user_avatar.tag = path
            } else {
                editImage_user_avatar.tag = ""
            }
            currentPhotoPath=path ?: ""
            editText_edit_location.editText?.setText(savedInstanceState.getString("locationText"))
            editText_edit_fullname.editText?.setText(savedInstanceState.getString("nameText"))
            editText_edit_nickname.editText?.setText(savedInstanceState.getString("nicknameText"))
            editText_edit_email.editText?.setText(savedInstanceState.getString("emailText"))
        }
    }

    /**
     * Callback function that handles the arrival of an intent as result (After calling startActivityForResult)
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //super.onActivityResult(requestCode, resultCode, data)
        if(!this::cameraGallery.isInitialized){
            Log.d("DEBUG","restoring CameraGallery")
            cameraGallery = CameraGallery(edit_profile_picture,
                    requireContext(),
                this,
                false)
            val sharedPref = activity?.getSharedPreferences(
                getString(R.string.preference_file_key),
                Context.MODE_PRIVATE
            )
            cameraGallery.photoPath = sharedPref?.getString("persistentCurrentPhotoPath", "") ?: ""
            cameraGallery.imgUri = Uri.parse(sharedPref?.getString("persistentCurrentUri", "") ?: "")
            Log.d("DEBUG", "restored cameragallery photopath" + cameraGallery.photoPath)
        }
        when(requestCode){
            cameraGallery.REQUEST_IMAGE_CAPTURE -> {
                if(resultCode == AppCompatActivity.RESULT_OK ){
                    if (cameraGallery.imgUri == null) {
                        Log.d("", "URI is null")
                        return
                    }
                    //val uri: Uri = cameraGallery.imgUri!!
                    currentPhotoPath = cameraGallery.photoPath
                    cameraGallery.handleRotation()
                    cameraGallery.launchImageCrop(14,9)
                    editImage_user_avatar.tag = currentPhotoPath
                } else {
                    Snackbar.make(editProfileLayout, "Error while taking the photo!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }
            }
            cameraGallery.REQUEST_IMAGE_GALLERY -> {
                if(resultCode == AppCompatActivity.RESULT_OK && data != null) {
                    val ins: InputStream? =
                        this
                            .activity
                            ?.contentResolver
                            ?.openInputStream(data.data!!)                   /* loading the bitmap */
                    var img: Bitmap? = BitmapFactory.decodeStream(ins)
                    cameraGallery.bitmapToFile(img!!)                        /* store the image in application directory */

                    currentPhotoPath = cameraGallery.photoPath
                    cameraGallery.launchImageCrop(14,9)
                    editImage_user_avatar.tag = currentPhotoPath
                } else {
                    Snackbar.make(editProfileLayout, "Error while taking the photo!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }
            }
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                if(resultCode == AppCompatActivity.RESULT_OK && data != null){
                    val result = CropImage.getActivityResult(data)
                    val resUri = result.uri
                    editImage_user_avatar.setImageURI(resUri)
                    editImage_user_avatar.tag = currentPhotoPath
                    Snackbar.make(editProfileLayout, "Profile photo updated!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                    Snackbar.make(editProfileLayout, "Profile data not loaded. Try again please!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }
            }
            else -> false
        }
    }
}