package it.polito.group03.lab2.second_hand_market.ui.user_advertisements

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import it.polito.group03.lab2.second_hand_market.model.Advertisement
import java.lang.reflect.Type


class UserAdvertisementsViewModel : ViewModel() {
    var sharedPreferences: SharedPreferences? = null
        set(value){
            field = value
            loadList()
        }

    lateinit var advertisements: LiveData<ArrayList<Advertisement>>

    /**
     * This method allows to add a new item in the list
     */
    fun addAdvertisement(advertisement: Advertisement): Boolean? {
        var result = false
        advertisements.apply {
            result= value!!.add(advertisement)
        }
        saveList()
        return result
    }

    /**
     * It updates an item in a specific position. After, it saves all list, using the saveList method.
     */
    fun updateAdvertisement(advertisement: Advertisement, position: Int){
        advertisements.apply {
            value?.set(position, advertisement)
        }
        saveList()
    }

    /**
     * It saves the list in json format using the Gson library.
     */
    fun saveList() {
        val editor = sharedPreferences!!.edit()
        val gson = Gson()
        val json = gson.toJson(advertisements.value)
        editor.putString("advertisements", json)
        editor.apply()
    }

    /**
     * It loads the list from json format in the local device.
     */
    fun loadList() {
        val gson = Gson()
        val json = sharedPreferences!!.getString("advertisements", null)

        if (json != null) {
            val type: Type? = object : TypeToken<ArrayList<Advertisement>>() {}.type
            val list = gson.fromJson<ArrayList<Advertisement>>(json, type)

            advertisements = MutableLiveData<ArrayList<Advertisement>>().apply {
                value = list
            }
        } else {
            advertisements = MutableLiveData<ArrayList<Advertisement>>().apply {
                value = arrayListOf()
            }
        }
    }
}