package it.polito.group03.lab2.second_hand_market.ui.user_profile

import android.net.Uri
import it.polito.group03.lab2.second_hand_market.R
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.content.Context
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import it.polito.group03.lab2.second_hand_market.model.UserProfile
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_show_profile.*
import org.json.JSONObject

class ShowProfileFragment : Fragment() {
    private lateinit var myActivityMenuItem: MenuItem
    private val PROFILE_BUNDLE_KEY = "it.polito.group03.lab2.second_hand_market.profileInfo"


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?{
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_show_profile,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("DEBUG", "arguments == NULL")
        val sharedP =
            this.activity?.getSharedPreferences(
                getString(R.string.preference_file_key),
                Context.MODE_PRIVATE
            )
        if(sharedP == null){
            Snackbar.make(showProfileLayout, "Profile informations not loaded! Error in the data.", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        } else {
            val defaultProfileString: String = getString(R.string.default_user_info)
            val profileString = sharedP?.getString("profile", defaultProfileString).toString() ?: ""

            val profileJSON = JSONObject(profileString)
            val img = Uri.parse(profileJSON.getString("avatar"))

            val navHeader: View? = activity
                ?.findViewById<NavigationView?>(R.id.nav_view)
                ?.getHeaderView(0)

            var avatarUri = ""
            var nick = ""
            var userEmail = ""
            if (profileJSON.has("avatar") &&
                profileJSON.getString("avatar").isNotEmpty() &&
                img != null
            ) {
                userAvatar.setImageURI(img)
                avatarUri = profileJSON.getString("avatar")
                userAvatar.tag = avatarUri
            }
            if (profileJSON.has("fullname"))
                fullname.text = profileJSON.getString("fullname")
            if (profileJSON.has("nickname")) {
                nick = profileJSON.getString("nickname")
                nickname.text = nick
            }
            if (profileJSON.has("location"))
                location.text = profileJSON.getString("location")
            if (profileJSON.has("email")) {
                userEmail = profileJSON.getString("email")
                email.text = userEmail
            }

            if (navHeader != null) {
                val navAvatar: ImageView? = navHeader
                    .findViewById(R.id.header_avatar)
                if (navAvatar != null && avatarUri.isNotEmpty()) {
                    navAvatar.setImageURI(img)
                    navAvatar.tag = avatarUri
                }

                val navNick: TextView? = navHeader
                    .findViewById(R.id.header_nickname)
                if (navNick != null && nick.isNotEmpty()) {
                    navNick.text = nick
                }

                val navEmail: TextView? = navHeader
                    .findViewById(R.id.header_email)
                if (navEmail != null && userEmail.isNotEmpty()) {
                    navEmail.text = userEmail
                }
            }
        }
    }

    /**
     * Inflate the menu when created
     */
    override fun onCreateOptionsMenu(menu: Menu,inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu,inflater)

        //hide settings menu item

        val item = menu.findItem(R.id.action_settings)
        item.isEnabled = false
        item.isVisible = false
        inflater.inflate(R.menu.edit_profile, menu)
    }

    /**
     * Check which menu item is pressed and start corresponding task
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.nav_user_edit_profile -> {
                myActivityMenuItem = item
                item.isEnabled = false
                val profile = UserProfile(
                    userAvatar.tag.toString(),
                    fullname.text.toString(),
                    nickname.text.toString(),
                    email.text.toString(),
                    location.text.toString()
                )
                val bundle = Bundle()
                bundle.putParcelable(PROFILE_BUNDLE_KEY,profile)
                NavHostFragment.findNavController(nav_host_fragment)
                    .navigate(R.id.action_nav_user_profile_to_nav_user_edit_profile,bundle)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}