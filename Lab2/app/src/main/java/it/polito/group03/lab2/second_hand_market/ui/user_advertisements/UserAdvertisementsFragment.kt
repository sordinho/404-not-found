package it.polito.group03.lab2.second_hand_market.ui.user_advertisements

import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import it.polito.group03.lab2.second_hand_market.R
import it.polito.group03.lab2.second_hand_market.model.Advertisement
import kotlinx.android.synthetic.main.fragment_user_advertisements.*

class UserAdvertisementsFragment : Fragment() {

    private lateinit var userAdvertisementsViewModel: UserAdvertisementsViewModel
    private var sharedPreferences : SharedPreferences? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        userAdvertisementsViewModel =
                ViewModelProviders.of(this).get(UserAdvertisementsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_user_advertisements, container, false)
        sharedPreferences = activity?.getSharedPreferences("ListOfAdvertisements", MODE_PRIVATE)
        userAdvertisementsViewModel.sharedPreferences = sharedPreferences
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val advertisementAdapter = AdvertisementAdapter(userAdvertisementsViewModel.advertisements.value!!, findNavController())
        itemList.adapter = advertisementAdapter
        itemList.layoutManager = LinearLayoutManager(context)

        userAdvertisementsViewModel.advertisements.observe(viewLifecycleOwner, Observer {
            advertisementAdapter.updateData(it)
        })

        // Fill form with received bundle
        fillFieldsFromBundle(arguments)

        fab.setOnClickListener{
            findNavController().navigate(R.id.action_nav_slideshow_to_itemEditFragment)
        }

        if (userAdvertisementsViewModel.advertisements.value.isNullOrEmpty()) {
            messageListEmpty.isVisible = true
            itemList.isVisible = false
        }else if (messageListEmpty.isVisible) {
            messageListEmpty.isVisible = false
        }
    }

    /**
     * Fill fields from received bundle
     */
    private fun fillFieldsFromBundle(arguments: Bundle?) {
        val adv: Advertisement? = arguments?.getParcelable("item")
        val position: Int? = arguments?.getInt("position")
        if (adv != null) {
            if (position == -1) {
                val result = userAdvertisementsViewModel.addAdvertisement(
                    adv
                )
                if (result == false) {
                    Snackbar.make(constraintLayout, "Error during the insertion, retry!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
                }else{
                    Snackbar.make(constraintLayout, "A new advertisement is added!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }
            }else {
                try {
                    userAdvertisementsViewModel.updateAdvertisement(
                        adv,
                        position!!
                    )
                    Snackbar.make(constraintLayout, "The advertisement is updated!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }catch (e: IndexOutOfBoundsException){
                    Snackbar.make(constraintLayout, "Error during the update, retry!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }
            }
        }
        this.getArguments()?.clear()
    }
}
