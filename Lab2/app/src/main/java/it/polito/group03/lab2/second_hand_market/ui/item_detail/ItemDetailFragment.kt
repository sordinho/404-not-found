package it.polito.group03.lab2.second_hand_market.ui.item_detail

import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import it.polito.group03.lab2.second_hand_market.MainActivity
import it.polito.group03.lab2.second_hand_market.R
import it.polito.group03.lab2.second_hand_market.model.Advertisement
import kotlinx.android.synthetic.main.fragment_item_detail.*

class ItemDetailFragment : Fragment() {

    var position: Int = 0;
    lateinit var adv: Advertisement;

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true);
        (activity as MainActivity).supportActionBar?.title="Item Detail"
        return inflater.inflate(R.layout.fragment_item_detail,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fillFieldsFromBundle(arguments)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear();
        inflater.inflate(R.menu.item_detail, menu);
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_edit -> {
                val bundle = Bundle()
                bundle.putParcelable("item", adv)
                bundle.putInt("position", this.position)
                findNavController().navigate(
                    R.id.action_itemDetailFragment3_to_itemEditFragment,
                    bundle
                )
                return true
            }
            else -> {
                return false
            }
        }
    }

    /**
     * Fill fields from received bundle
     */
    private fun fillFieldsFromBundle(arguments: Bundle?) {
        this.adv = arguments?.getParcelable("item")!!
        if (adv != null) {
            imageView_item_detail_picture.setImageURI(Uri.parse(adv.photoPath))
            textView_item_detail_title.text = adv.title
            textView_item_detail_description.text = adv.description
            if (adv.price.isNaN())
                textView_item_detail_price.text = ""
            else
                textView_item_detail_price.text=adv.price.toString()+"€"
            textView_item_detail_category.text=adv.category+"/"+adv.subCategory
            textView_item_detail_location.text=adv.location
            textView_item_detail_expiry_date.text="Expire date: "+adv.expiryDate
        }
        val position: Int? = arguments?.getInt("position")
        if (position != null)
            this.position = position

    }

}