package it.polito.group03.lab2.second_hand_market.model

import android.media.ThumbnailUtils
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Advertisement(
    val photoPath: String,
    val thumbnailPath: String,
    val title: String,
    val description: String,
    val price: Double,
    val category: String,
    val subCategory: String,
    val location: String,
    val expiryDate: String
) : Parcelable