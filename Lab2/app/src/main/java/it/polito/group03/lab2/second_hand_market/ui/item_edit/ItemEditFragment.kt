package it.polito.group03.lab2.second_hand_market.ui.item_edit

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_GET_CONTENT
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.snackbar.Snackbar
import com.theartofdev.edmodo.cropper.CropImage
import it.polito.group03.lab2.second_hand_market.MainActivity
import it.polito.group03.lab2.second_hand_market.R
import it.polito.group03.lab2.second_hand_market.model.Advertisement
import it.polito.group03.lab2.second_hand_market.utils.CameraGallery
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_item_edit.*
import kotlinx.android.synthetic.main.nav_header_main.*
import java.io.InputStream
import java.util.*


class ItemEditFragment : Fragment() {
    private var position: Int = -1
    private var currentPhotoPath: String = ""
    private var currentThumbnailPath: String = ""
    private lateinit var cameraGallery: CameraGallery

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        (activity as MainActivity).supportActionBar?.title =
            getString(R.string.toolbar_edit_item_fragment)
        return inflater.inflate(R.layout.fragment_item_edit, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Setup popup menu for camera / gallery
        edit_item_image.setOnClickListener {
            cameraGallery = CameraGallery(edit_item_image, requireContext(), this)
        }

        // Fill form with received bundle
        fillFieldsFromBundle(arguments)

        // Setup dropdown menus for categories
        setupDropdownMenusCategories()

        // Hide soft keyboard on spinner click
        hideSoftKeyboardOnViewClick(textField_item_edit_category.editText!!)
        hideSoftKeyboardOnViewClick(textField_item_edit_subCategory.editText!!)

        // Setup Date Picker
        textField_item_edit_expiry_date.editText!!.setOnClickListener {
            setupDatePicker()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.item_edit_fragment, menu);
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_save -> {
                if (!checkFieldBeforeSave()) {
                    return false
                }
                val advertisement = Advertisement(
                    currentPhotoPath
                    , currentThumbnailPath
                    , textField_item_edit_title.editText?.text.toString()
                    , textField_item_edit_description.editText?.text.toString()
                    , textField_item_edit_price.editText?.text.toString().toDouble()
                    , textField_item_edit_category.editText?.text.toString()
                    , textField_item_edit_subCategory.editText?.text.toString()
                    , textField_item_edit_location.editText?.text.toString()
                    , textField_item_edit_expiry_date.editText?.text.toString()
                )
                val bundle = Bundle()
                bundle.putParcelable("item", advertisement)
                bundle.putInt("position", this.position)
                NavHostFragment.findNavController(nav_host_fragment).navigate(
                    R.id.action_itemEditFragment_to_nav_advertisements,
                    bundle
                )
                true
            }
            else -> {
                false
            }
        }
    }

    private fun checkFieldBeforeSave(): Boolean {
        var result = true
        if (currentPhotoPath == "") {
            Snackbar.make(
                itemEditLayout,
                resources.getString(R.string.error_empty_field) + " " + item_image.contentDescription,
                Snackbar.LENGTH_LONG
            ).show()
            result = false
        } else if (currentThumbnailPath == "") {
            Snackbar.make(
                itemEditLayout,
                resources.getString(R.string.error_image_thumbnail),
                Snackbar.LENGTH_LONG
            ).show()
            result = false
        }
        if (textField_item_edit_title.editText?.text.isNullOrEmpty()) {
            textField_item_edit_title.error =
                resources.getString(R.string.error_empty_field) + " " + textField_item_edit_title.hint
            result = false
        }
        if (textField_item_edit_description.editText?.text.isNullOrEmpty()) {
            textField_item_edit_description.error =
                resources.getString(R.string.error_empty_field) + " " + textField_item_edit_description.hint
            result = false
        }
        if (textField_item_edit_price.editText?.text.isNullOrEmpty()) {
            textField_item_edit_price.error =
                resources.getString(R.string.error_empty_field) + " " + textField_item_edit_price.hint
            result = false
        }
        if (textField_item_edit_category.editText?.text.isNullOrEmpty()) {
            textField_item_edit_category.error =
                resources.getString(R.string.error_empty_field) + " " + textField_item_edit_category.hint
            result = false
        }
        if (textField_item_edit_subCategory.editText?.text.isNullOrEmpty()) {
            textField_item_edit_subCategory.error =
                resources.getString(R.string.error_empty_field) + " " + textField_item_edit_subCategory.hint
            result = false
        }
        if (textField_item_edit_location.editText?.text.isNullOrEmpty()) {
            textField_item_edit_location.error =
                resources.getString(R.string.error_empty_field) + " " + textField_item_edit_location.hint
            result = false
        }
        if (textField_item_edit_expiry_date.editText?.text.isNullOrEmpty()) {
            textField_item_edit_expiry_date.error =
                resources.getString(R.string.error_empty_field) + " " + textField_item_edit_expiry_date.hint
            result = false
        }
        return result
    }

    /**
     * Fill fields from received bundle
     */
    private fun fillFieldsFromBundle(arguments: Bundle?) {
        val adv: Advertisement? = arguments?.getParcelable("item")
        if (adv != null) {
            item_image.setImageURI(Uri.parse(adv.photoPath))
            item_image.tag = adv.photoPath
            currentPhotoPath = adv.photoPath
            currentThumbnailPath = adv.thumbnailPath
            textField_item_edit_title.editText?.setText(adv.title)
            textField_item_edit_description.editText?.setText(adv.description)
            if (adv.price.isNaN())
                textField_item_edit_price.editText?.setText("")
            else
                textField_item_edit_price.editText?.setText(adv.price.toString())
            textField_item_edit_category.editText?.setText(adv.category)
            textField_item_edit_subCategory.editText?.setText(adv.subCategory)
            setSubcategoryDropdown(adv.category)
            textField_item_edit_location.editText?.setText(adv.location)
            textField_item_edit_expiry_date.editText?.setText(adv.expiryDate)
            position = arguments.getInt("position")
        }
    }

    /**
     * Setup the date picker for expiry date
     */
    private fun setupDatePicker() {
        val calendar = Calendar.getInstance()
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)

        // date picker dialog
        val dialog = DatePickerDialog(
            requireContext(),
            DatePickerDialog.OnDateSetListener { view, dYear, dMonth, dayOfMonth ->
                textField_item_edit_expiry_date.editText?.setText("" + dayOfMonth + "/" + (dMonth + 1) + "/" + dYear)
            }, year, month, day
        )
        dialog.datePicker.minDate = System.currentTimeMillis() - 1000;
        dialog.show()
    }

    /**
     * This function setup the dropdown menus for categories and sub cateogries
     * also setup onclick listener for categories
     */
    private fun setupDropdownMenusCategories() {
        val categories = resources.getStringArray(R.array.categories)
        val adapter = context?.let { ArrayAdapter(it, R.layout.list_item, categories) }
        (textField_item_edit_category.editText as AutoCompleteTextView).setAdapter(adapter)
        (textField_item_edit_category.editText as AutoCompleteTextView).addTextChangedListener(
            object :
                TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    textField_item_edit_subCategory.isEnabled = true
                    (textField_item_edit_subCategory.editText as AutoCompleteTextView).setText("")
                    setSubcategoryDropdown(s.toString())
                }
            })
    }

    /**
     * Setup the dropdown menu for sub categories given a category
     */
    private fun setSubcategoryDropdown(selectedCategory: String?) {
        var subCatToUpdate: Array<String> = emptyArray()

        Log.d("kkk", selectedCategory)

        when (selectedCategory) {
            resources.getString(R.string.cat1) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat1)
            resources.getString(R.string.cat2) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat2)
            resources.getString(R.string.cat3) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat3)
            resources.getString(R.string.cat4) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat4)
            resources.getString(R.string.cat5) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat5)
            resources.getString(R.string.cat6) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat6)
            resources.getString(R.string.cat7) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat7)
            resources.getString(R.string.cat8) -> subCatToUpdate =
                resources.getStringArray(R.array.subcat8)
            else -> textField_item_edit_subCategory.isEnabled = false
        }
        val subCatAdapter =
            context?.let { ArrayAdapter(it, R.layout.list_item, subCatToUpdate) }
        (textField_item_edit_subCategory.editText as AutoCompleteTextView).setAdapter(
            subCatAdapter
        )
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("image", item_image.tag?.toString() ?: "")
        outState.putString("title", textField_item_edit_title.editText?.text.toString())
        outState.putString("description", textField_item_edit_description.editText?.text.toString())
        outState.putString("price", textField_item_edit_price.editText?.text.toString())
        outState.putString("category", textField_item_edit_category.editText?.text.toString())
        outState.putString("subCategory", textField_item_edit_subCategory.editText?.text.toString())
        outState.putString("location", textField_item_edit_location.editText?.text.toString())
        outState.putString("expiryDate", textField_item_edit_expiry_date.editText?.text.toString())
        outState.putInt("position", position)
        outState.putString("currentThumbnailPath", currentThumbnailPath)
        super.onSaveInstanceState(outState)
    }


    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null) {
            val path = savedInstanceState.getString("image")
            if (path == "")
                item_image.setImageResource(R.drawable.ic_gallery)
            else
                item_image.setImageURI(Uri.parse(path))
            item_image.tag = path
            currentPhotoPath = path ?: ""
            position = savedInstanceState.getInt("position")
            currentThumbnailPath = savedInstanceState.getString("currentThumbnailPath") ?: ""
            textField_item_edit_title.editText?.setText(savedInstanceState.getString("title"))
            textField_item_edit_description.editText?.setText(savedInstanceState.getString("description"))
            textField_item_edit_price.editText?.setText(savedInstanceState.getString("price"))
            textField_item_edit_category.editText?.setText(savedInstanceState.getString("category"))
            textField_item_edit_subCategory.editText?.setText(savedInstanceState.getString("subCategory"))
            setupDropdownMenusCategories()
            setSubcategoryDropdown(savedInstanceState.getString("category"))
            textField_item_edit_location.editText?.setText(savedInstanceState.getString("location"))
            textField_item_edit_expiry_date.editText?.setText(savedInstanceState.getString("expiryDate"))
        }
    }


    /**
     * Callback function after camera/gallery/crop intents
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //super.onActivityResult(requestCode, resultCode, data)
        if (!this::cameraGallery.isInitialized) {
            Log.d("kkk", "restore cameragallery")
            cameraGallery = CameraGallery(edit_item_image, requireContext(), this, false)
            val sharedP = this.activity?.getSharedPreferences(
                getString(R.string.preference_file_key),
                Context.MODE_PRIVATE
            )
            cameraGallery.photoPath = sharedP?.getString("persistentCurrentPhotoPath", "") ?: ""
            cameraGallery.imgUri = Uri.parse(sharedP?.getString("persistentCurrentUri", "") ?: "")
            Log.d("kkk", "restored cameragallery photopath" + cameraGallery.photoPath)
        }
        when (requestCode) {
            cameraGallery.REQUEST_IMAGE_CAPTURE -> {
                if (resultCode == AppCompatActivity.RESULT_OK) {
                    if (cameraGallery.imgUri == null) {
                        Log.d("", "URI is null")
                        return
                    }
                    currentPhotoPath = cameraGallery.photoPath
                    item_image.tag = currentPhotoPath
                    cameraGallery.handleRotation()
                    cameraGallery.launchImageCrop()
                } else {
                    Toast.makeText(context, "Error while taking the photo!", Toast.LENGTH_SHORT)
                        .show();
                }
            }
            cameraGallery.REQUEST_IMAGE_GALLERY -> {
                if (resultCode == AppCompatActivity.RESULT_OK && data != null) {
                    val ins: InputStream? =
                        this
                            .activity
                            ?.contentResolver
                            ?.openInputStream(data.data!!)             /* loading the bitmap */
                    var img: Bitmap? = BitmapFactory.decodeStream(ins)
                    cameraGallery.bitmapToFile(img!!)                        /* store the image in application directory */
                    currentPhotoPath = cameraGallery.photoPath
                    item_image.tag = currentPhotoPath
                    cameraGallery.launchImageCrop()
                } else {
                    Toast.makeText(context, "Error while taking the photo!", Toast.LENGTH_SHORT)
                        .show();
                }
            }
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                if (resultCode == AppCompatActivity.RESULT_OK && data != null) {
                    val result = CropImage.getActivityResult(data)
                    val resUri = result.uri
                    item_image.tag = currentPhotoPath
                    item_image.setImageURI(resUri)
                    cameraGallery.createThumbnail()
                    currentThumbnailPath = cameraGallery.thumbnailPath
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Toast.makeText(context, "Error while cropping the photo!", Toast.LENGTH_SHORT)
                        .show();
                }
            }
            else -> Toast.makeText(context, "ERROR! YOU SHOULDN'T SEE THIS!!!", Toast.LENGTH_SHORT)
                .show();
        }
    }

    /**
     * Hide soft keyboard when a view is clicked (spinner)
     */
    private fun hideSoftKeyboardOnViewClick(view: View) {
        view.setOnClickListener() {
            val imm: InputMethodManager =
                requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            val viewToken = this.view?.rootView?.windowToken;
            imm.hideSoftInputFromWindow(viewToken, 0)
        }
    }
}