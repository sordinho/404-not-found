package it.polito.group03.lab2.second_hand_market.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserProfile (
    val path: String,
    val fullname: String,
    val nickname: String,
    val email: String,
    val location: String
    ) : Parcelable